//Maya ASCII 2017ff05 scene
//Name: MountainSection2_model_002.ma
//Last modified: Mon, Sep 18, 2017 02:56:50 PM
//Codeset: 1252
requires maya "2017ff05";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Enterprise Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "education";
createNode transform -s -n "persp";
	rename -uid "D83B3D24-4D1E-1289-9687-FD8CA8008D4E";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 13.730165186787294 5.5009560228047913 32.302183774850967 ;
	setAttr ".r" -type "double3" -24.338352729620034 -2.1999999999995214 9.9465648292798417e-017 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "CD3BD249-465E-13E9-9A1B-6D95E4F28800";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 67.959795054855363;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "4A67872E-4914-FCFE-72A4-019DBAE4CF60";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "6653BF6B-4686-8017-6E5E-C28088840089";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 50.494445959443851;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "4032E8BE-4E65-8597-A9BD-9B864592ED4E";
	setAttr ".t" -type "double3" 1.2628588359146846 -8.769656613276215 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "8A7E5565-43ED-6DD2-ABD7-75B3BBA82C7D";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 128.21721696016078;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "E68E16A9-4F8C-3670-1D22-E9A08876976B";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 -3.3131639590665447 -8.7163085866948258 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "63BE7A0D-468A-2470-F637-D693C6C0CA45";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 39.871208917882306;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "imagePlane1";
	rename -uid "161520DB-4A0F-B98E-89C8-9EAE3E9DADBF";
	setAttr ".t" -type "double3" 0 0 -6.0456273764258572 ;
createNode imagePlane -n "imagePlaneShape1" -p "imagePlane1";
	rename -uid "1248AA26-4E98-C7CE-4469-3598032FEDE8";
	setAttr -k off ".v";
	setAttr ".fc" 101;
	setAttr ".imn" -type "string" "C:/Users/10795516/Git Repos/game-fall2017/Design/Level Design/IMG_20170918_141830.jpg";
	setAttr ".cov" -type "short2" 4032 3024 ;
	setAttr ".dic" yes;
	setAttr ".dlc" no;
	setAttr ".w" 40.32;
	setAttr ".h" 30.240000000000002;
	setAttr ".cs" -type "string" "sRGB";
createNode transform -n "pCube1";
	rename -uid "E7096A5A-46CC-31CE-FB01-C384AA951751";
	setAttr ".t" -type "double3" -19.672549769757108 -8.9906222929659236 0 ;
	setAttr ".s" -type "double3" 1 12.614814079169303 1 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	rename -uid "703D986E-4643-C99A-3F8F-C68C93B4153F";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.72237786650657654 0.33971070498228073 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "CB6F7537-4FC5-D307-49A2-338E05DFEF4A";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "EB3F1DE0-4CEF-AABB-61CB-B1BD90270243";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "3CF88FCE-4A23-5433-05DA-AB9C63E7A4E4";
createNode displayLayerManager -n "layerManager";
	rename -uid "D79A2082-46AB-A925-E229-669E601E06F6";
createNode displayLayer -n "defaultLayer";
	rename -uid "39ABC536-4410-E3C6-1AE3-69A612F902D8";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "45D59ADF-40D4-8C08-4DEB-FFA63F537202";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "BD824DCB-465E-7C48-1912-0A989BB45C7D";
	setAttr ".g" yes;
createNode polyCube -n "polyCube1";
	rename -uid "1BCDACA3-454F-63EC-1E47-749B6746591B";
	setAttr ".cuv" 4;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "52C00660-4803-A324-5D5C-CEB9115A2C36";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 12.614814079169303 0 0 0 0 1 0 -19.672549769757108 -8.9906222929659236 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -19.17255 -8.6395016 0 ;
	setAttr ".rs" 52933;
	setAttr ".lt" -type "double3" -1.9057979616469388e-016 1.0511083457039984e-016 0.85829509899166112 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -19.172549769757108 -15.298029332550575 -0.5 ;
	setAttr ".cbx" -type "double3" -19.172549769757108 -1.9809738285527141 0.5 ;
createNode polyTweak -n "polyTweak1";
	rename -uid "23DF1D1E-48FC-1AE2-2AF1-0EB7FAD4378F";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[2:5]" -type "float3"  0 0.034019336 0 0 0.055667993
		 0 0 0.034019336 0 0 0.055667993 0;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "DE17BFF4-493C-7731-BBBF-E4AB77561AE6";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 12.614814079169303 0 0 0 0 1 0 -19.672549769757108 -8.9906222929659236 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -18.314255 -8.6395016 0 ;
	setAttr ".rs" 37220;
	setAttr ".lt" -type "double3" -2.1656795018715213e-016 1.8958009695938857e-015 0.97533533976324449 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -18.31425432908328 -15.298028580649063 -0.5 ;
	setAttr ".cbx" -type "double3" -18.31425432908328 -1.9809738285527141 0.5 ;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "F2E44427-4953-3735-EB9D-EE92F285B84A";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 12.614814079169303 0 0 0 0 1 0 -19.672549769757108 -8.9906222929659236 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -17.338921 -8.6395016 0 ;
	setAttr ".rs" 63945;
	setAttr ".lt" -type "double3" -8.2990919422574558e-015 -2.528213519711689e-015 37.375787378664178 ;
	setAttr ".d" 25;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -17.338920161602811 -15.298028580649063 -0.5 ;
	setAttr ".cbx" -type "double3" -17.338920161602811 -1.9809738285527141 0.5 ;
createNode polySplitRing -n "polySplitRing1";
	rename -uid "897B0172-47DB-7AB0-DA49-559CFE5C2EBE";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[52]" "e[77]" "e[127]" "e[177]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 12.614814079169303 0 0 0 0 1 0 -19.672549769757108 -8.9906222929659236 0 1;
	setAttr ".wt" 0.56861191987991333;
	setAttr ".dr" no;
	setAttr ".re" 177;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak2";
	rename -uid "8A04B934-42E9-2BE1-0D2C-D39DE36641AA";
	setAttr ".uopa" yes;
	setAttr -s 56 ".tk";
	setAttr ".tk[10]" -type "float3" 0 0.036972847 0 ;
	setAttr ".tk[11]" -type "float3" 0 0.036972847 0 ;
	setAttr ".tk[14]" -type "float3" 0 0.017064391 0 ;
	setAttr ".tk[15]" -type "float3" 0 0.017064391 0 ;
	setAttr ".tk[40]" -type "float3" 1.4305115e-006 0 0 ;
	setAttr ".tk[65]" -type "float3" 1.4305115e-006 0 0 ;
	setAttr ".tk[66]" -type "float3" 0 -0.08247789 0 ;
	setAttr ".tk[67]" -type "float3" 0 -0.19339643 0 ;
	setAttr ".tk[68]" -type "float3" 0 -0.30147085 0 ;
	setAttr ".tk[69]" -type "float3" 0 -0.43229765 0 ;
	setAttr ".tk[70]" -type "float3" 0 -0.50197709 0 ;
	setAttr ".tk[71]" -type "float3" 0 -0.55642092 0 ;
	setAttr ".tk[72]" -type "float3" 0 -0.61140984 0 ;
	setAttr ".tk[73]" -type "float3" 0 -0.64009947 0 ;
	setAttr ".tk[74]" -type "float3" 0 -0.65205359 0 ;
	setAttr ".tk[75]" -type "float3" 0 -0.66161656 0 ;
	setAttr ".tk[76]" -type "float3" 0 -0.68313456 0 ;
	setAttr ".tk[77]" -type "float3" 0 -0.71182412 0 ;
	setAttr ".tk[78]" -type "float3" 0 -0.72377843 0 ;
	setAttr ".tk[79]" -type "float3" 0 -0.74768704 0 ;
	setAttr ".tk[80]" -type "float3" 0 -0.75964111 0 ;
	setAttr ".tk[81]" -type "float3" 0 -0.7644226 0 ;
	setAttr ".tk[82]" -type "float3" 0 -0.76442289 0 ;
	setAttr ".tk[83]" -type "float3" 0 -0.76203191 0 ;
	setAttr ".tk[84]" -type "float3" 0 -0.73637545 0 ;
	setAttr ".tk[85]" -type "float3" 0 -0.65747797 0 ;
	setAttr ".tk[86]" -type "float3" -2.9802322e-008 -0.54749984 0 ;
	setAttr ".tk[87]" -type "float3" 0 -0.42556798 0 ;
	setAttr ".tk[88]" -type "float3" 0 -0.17453066 0 ;
	setAttr ".tk[89]" -type "float3" 0 -0.033471636 0 ;
	setAttr ".tk[90]" -type "float3" 1.4305115e-006 -0.0765066 0 ;
	setAttr ".tk[91]" -type "float3" 0 -0.08247789 0 ;
	setAttr ".tk[92]" -type "float3" 0 -0.19339643 0 ;
	setAttr ".tk[93]" -type "float3" 0 -0.30147085 0 ;
	setAttr ".tk[94]" -type "float3" 0 -0.43229765 0 ;
	setAttr ".tk[95]" -type "float3" 0 -0.50197709 0 ;
	setAttr ".tk[96]" -type "float3" 0 -0.55642092 0 ;
	setAttr ".tk[97]" -type "float3" 0 -0.61140984 0 ;
	setAttr ".tk[98]" -type "float3" 0 -0.64009947 0 ;
	setAttr ".tk[99]" -type "float3" 0 -0.65205359 0 ;
	setAttr ".tk[100]" -type "float3" 0 -0.66161656 0 ;
	setAttr ".tk[101]" -type "float3" 0 -0.68313456 0 ;
	setAttr ".tk[102]" -type "float3" 0 -0.71182412 0 ;
	setAttr ".tk[103]" -type "float3" 0 -0.72377843 0 ;
	setAttr ".tk[104]" -type "float3" 0 -0.74768704 0 ;
	setAttr ".tk[105]" -type "float3" 0 -0.75964111 0 ;
	setAttr ".tk[106]" -type "float3" 0 -0.7644226 0 ;
	setAttr ".tk[107]" -type "float3" 0 -0.76442289 0 ;
	setAttr ".tk[108]" -type "float3" 0 -0.76203191 0 ;
	setAttr ".tk[109]" -type "float3" 0 -0.73637545 0 ;
	setAttr ".tk[110]" -type "float3" 0 -0.65747797 0 ;
	setAttr ".tk[111]" -type "float3" -2.9802322e-008 -0.54749984 0 ;
	setAttr ".tk[112]" -type "float3" 0 -0.42556798 0 ;
	setAttr ".tk[113]" -type "float3" 0 -0.17453066 0 ;
	setAttr ".tk[114]" -type "float3" 0 -0.033471636 0 ;
	setAttr ".tk[115]" -type "float3" 1.4305115e-006 -0.0765066 0 ;
createNode polySplitRing -n "polySplitRing2";
	rename -uid "23229289-45BB-CEA2-02EA-178F0276339B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[51]" "e[76]" "e[126]" "e[176]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 12.614814079169303 0 0 0 0 1 0 -19.672549769757108 -8.9906222929659236 0 1;
	setAttr ".wt" 0.66858404874801636;
	setAttr ".dr" no;
	setAttr ".re" 176;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak3";
	rename -uid "A6D6E9FC-44A0-4638-45FB-2482C8865648";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[116:117]" -type "float3"  0 0.011954154 0 0 0.011954154
		 0;
createNode polySplitRing -n "polySplitRing3";
	rename -uid "F1090932-4296-A135-8AD5-93ADD192C45A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[20:21]" "e[23]" "e[25]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 12.614814079169303 0 0 0 0 1 0 -19.672549769757108 -8.9906222929659236 0 1;
	setAttr ".wt" 0.51203161478042603;
	setAttr ".dr" no;
	setAttr ".re" 25;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak4";
	rename -uid "6AF9D1D9-465D-E190-25F6-108ACC12FF71";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[120:121]" -type "float3"  0 0.021828623 0 0 0.021828623
		 0;
createNode polySoftEdge -n "polySoftEdge1";
	rename -uid "035780C5-4BAD-3DE5-03E2-26B471E61E2A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 14 "e[0:4]" "e[6:8]" "e[10:15]" "e[17:18]" "e[20:23]" "e[25:26]" "e[28:127]" "e[152:202]" "e[227:231]" "e[233:234]" "e[236:239]" "e[241:242]" "e[244:247]" "e[249:250]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 12.614814079169303 0 0 0 0 1 0 -19.672549769757108 -8.9906222929659236 0 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak5";
	rename -uid "44ECED0F-44D9-1C7D-A69B-B8805E57E562";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[124:125]" -type "float3"  0 0.018462023 0 0 0.018462023
		 0;
createNode polySoftEdge -n "polySoftEdge2";
	rename -uid "6DB7BF0D-43AA-CAA3-CC7F-BC8ABFB10DF6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 15 "e[0:6]" "e[8:10]" "e[12:13]" "e[15:17]" "e[19:21]" "e[23:25]" "e[27:77]" "e[102:177]" "e[202:229]" "e[231:233]" "e[235:237]" "e[239:241]" "e[243:245]" "e[247:249]" "e[251]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 12.614814079169303 0 0 0 0 1 0 -19.672549769757108 -8.9906222929659236 0 1;
	setAttr ".a" 0;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "5DBE5697-4A81-5CD1-551B-31822500CA05";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n"
		+ "            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 397\n            -height 384\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 396\n            -height 383\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n"
		+ "            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n"
		+ "            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 397\n            -height 383\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n"
		+ "        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 396\n            -height 384\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n"
		+ "            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"0\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n"
		+ "            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n"
		+ "            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n"
		+ "            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n"
		+ "                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n"
		+ "                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n"
		+ "                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n"
		+ "                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n"
		+ "                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n"
		+ "                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n"
		+ "                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n"
		+ "                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n"
		+ "                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n"
		+ "                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"quad\\\" -ps 1 50 50 -ps 2 50 50 -ps 3 50 50 -ps 4 50 50 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 397\\n    -height 384\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 397\\n    -height 384\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 396\\n    -height 384\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 396\\n    -height 384\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Side View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 396\\n    -height 383\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 396\\n    -height 383\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Front View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"wireframe\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 397\\n    -height 383\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"wireframe\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 397\\n    -height 383\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "B4CDBFFA-40BB-54CF-2813-AFBDFC2AC07C";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyTweak -n "polyTweak6";
	rename -uid "A8FA9610-4059-F52C-EEB9-85B26859DA06";
	setAttr ".uopa" yes;
	setAttr -s 128 ".tk[0:127]" -type "float3"  0 0 0.14649998 0 0 0.14649998
		 0 0 0.14649998 0 0 0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0
		 -0.14649998 0 0 -0.14649998 0 0 0.14649998 0 0 -0.14649998 0 0 0.14649998 0 0 -0.14649998
		 0 0 0.14649998 0 0 -0.14649998 0 0 0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0
		 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998
		 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0
		 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0
		 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998
		 0 0 -0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998
		 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998
		 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998
		 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998
		 0 0 0.14649998 0 0 0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0
		 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998
		 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0
		 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0
		 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 -0.14649998
		 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998
		 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998
		 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998
		 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 0.14649998
		 0 0 0.14649998 0 0 0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 0.14649998 0 0
		 0.14649998 0 0 -0.14649998 0 0 -0.14649998 0 0 0.14649998 0 0 0.14649998 0 0 -0.14649998
		 0 0 -0.14649998 0 0 0.14649998;
createNode deleteComponent -n "deleteComponent1";
	rename -uid "FFF3DBA1-4AC4-C45C-AAAE-008CFFB17B13";
	setAttr ".dc" -type "componentList" 7 "f[2]" "f[7]" "f[11]" "f[39:63]" "f[115]" "f[119]" "f[123]";
createNode objectSet -n "set1";
	rename -uid "1D14A59A-4A4F-AC84-867C-3A88BC8EC685";
	setAttr ".ihi" 0;
createNode groupId -n "groupId1";
	rename -uid "F7F7F02C-4288-0ADE-6D69-609530FB19ED";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "2439DB9D-43AF-DA6A-E420-ADB2EE68E001";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 8 "e[0]" "e[3]" "e[9:13]" "e[18:20]" "e[25:99]" "e[204:206]" "e[211:213]" "e[218:220]";
createNode deleteComponent -n "deleteComponent2";
	rename -uid "89346B06-4C44-DB72-8AA7-9799B243E62B";
	setAttr ".dc" -type "componentList" 7 "f[2]" "f[5]" "f[8]" "f[11:35]" "f[87]" "f[90]" "f[93]";
createNode polyExtrudeEdge -n "polyExtrudeEdge1";
	rename -uid "823EC213-41B5-85B7-1BCD-EE9064CF89D3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[2]" "e[10]" "e[15]" "e[45:69]" "e[147]" "e[152]" "e[157]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 12.614814079169303 0 0 0 0 1 0 -19.672549769757108 -8.9906222929659236 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.067840144 -6.5171466 -0.64649999 ;
	setAttr ".rs" 36913;
	setAttr ".lt" -type "double3" 3.8857805861880479e-015 1.9087970370651082 0.5769594265356095 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -20.172549769757108 -11.62402650822075 -0.64649999141693115 ;
	setAttr ".cbx" -type "double3" 20.036869480731173 -1.4102662947427538 -0.64649999141693115 ;
createNode polyExtrudeEdge -n "polyExtrudeEdge2";
	rename -uid "8EF7F02C-4C78-24B4-4C99-32AC7164762B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 29 "e[163]" "e[165]" "e[167]" "e[170]" "e[172]" "e[174]" "e[176]" "e[178]" "e[180]" "e[182]" "e[184]" "e[186]" "e[188]" "e[190]" "e[192]" "e[194]" "e[196]" "e[198]" "e[200]" "e[202]" "e[204]" "e[206]" "e[208]" "e[210]" "e[212]" "e[214]" "e[216]" "e[219]" "e[221:223]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 12.614814079169303 0 0 0 0 1 0 -19.672549769757108 -8.9906222929659236 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.004025891 -5.9720216 -2.5552969 ;
	setAttr ".rs" 45725;
	setAttr ".lt" -type "double3" 0.49185119739035993 6.0449896765314559 0.97920496776535204 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -20.324548289654569 -11.04729774710192 -2.5552971363067627 ;
	setAttr ".cbx" -type "double3" 20.332600071551486 -0.89674538233680323 -2.5552968978881836 ;
createNode polyExtrudeEdge -n "polyExtrudeEdge3";
	rename -uid "F58E51B8-4ED4-DBB2-DC0A-D1A9ACF41E3C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 29 "e[226]" "e[228]" "e[230]" "e[233]" "e[235]" "e[237]" "e[239]" "e[241]" "e[243]" "e[245]" "e[247]" "e[249]" "e[251]" "e[253]" "e[255]" "e[257]" "e[259]" "e[261]" "e[263]" "e[265]" "e[267]" "e[269]" "e[271]" "e[273]" "e[275]" "e[277]" "e[279]" "e[282]" "e[284:286]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 12.614814079169303 0 0 0 0 1 0 -19.672549769757108 -8.9906222929659236 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.77178329 -3.3851137 -8.0887947 ;
	setAttr ".rs" 64065;
	setAttr ".lt" -type "double3" 1.2449214054296227 14.383333011950526 2.2868417073352596 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -20.533574626324491 -8.3725622576286636 -8.1188392639160156 ;
	setAttr ".cbx" -type "double3" 22.077141239764376 1.6023346353880346 -8.058751106262207 ;
createNode polyMapDel -n "polyMapDel1";
	rename -uid "E78CFC12-4341-FCCA-4F03-D5901AB37A4C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyTweak -n "polyTweak7";
	rename -uid "92DE2C3D-4E05-E2C6-4430-EA87EAC30EB5";
	setAttr ".uopa" yes;
	setAttr -s 95 ".tk";
	setAttr ".tk[0]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[1]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[2]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[3]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[7]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[9]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[10]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[12]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[14]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[15]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[16]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[17]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[18]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[19]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[20]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[21]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[22]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[23]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[24]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[25]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[26]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[27]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[28]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[29]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[30]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[31]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[32]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[33]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[34]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[35]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[36]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[37]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[38]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[64]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[65]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[66]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[67]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[68]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[69]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[70]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[71]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[72]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[73]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[74]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[75]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[76]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[77]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[78]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[79]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[80]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[81]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[82]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[83]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[84]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[85]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[86]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[87]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[88]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[89]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[91]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[92]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[94]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[95]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[97]" -type "float3" 0 0 1.7393197 ;
	setAttr ".tk[134]" -type "float3" -0.14576614 0 0 ;
	setAttr ".tk[135]" -type "float3" -0.65094131 0 0 ;
	setAttr ".tk[136]" -type "float3" -0.65094131 0 0 ;
	setAttr ".tk[137]" -type "float3" -0.65094131 0 0 ;
	setAttr ".tk[151]" -type "float3" -0.37243322 -0.0093085021 0.039576553 ;
	setAttr ".tk[152]" -type "float3" -0.38200989 0 0.13452694 ;
	setAttr ".tk[165]" -type "float3" -3.4132268 0.037275266 0 ;
	setAttr ".tk[166]" -type "float3" -4.9481668 0.10571869 -0.40153795 ;
	setAttr ".tk[167]" -type "float3" -4.3948655 0.098280728 -0.60043669 ;
	setAttr ".tk[168]" -type "float3" -4.0193715 0.087058485 -0.034973115 ;
	setAttr ".tk[169]" -type "float3" -4.4473391 0.13413335 0.3184908 ;
	setAttr ".tk[170]" -type "float3" -2.9419646 0.15077794 0.3753289 ;
	setAttr ".tk[171]" -type "float3" -0.89062887 0.023655135 -0.22679229 ;
	setAttr ".tk[172]" -type "float3" -1.3461821 0.043063693 -0.11762538 ;
	setAttr ".tk[173]" -type "float3" -0.71888888 0.034189962 -0.19448034 ;
	setAttr ".tk[174]" -type "float3" 0.33073759 -0.0060327337 -0.28782004 ;
	setAttr ".tk[175]" -type "float3" -0.25142935 -0.023597058 -0.22679229 ;
	setAttr ".tk[177]" -type "float3" -1.4873513 0.012235013 0.19658443 ;
	setAttr ".tk[178]" -type "float3" -1.7157352 0.019114261 0.28801215 ;
	setAttr ".tk[179]" -type "float3" -2.2350061 -0.0016213325 0.37871966 ;
	setAttr ".tk[180]" -type "float3" -2.840275 0.012833411 0.15539911 ;
	setAttr ".tk[181]" -type "float3" -2.6220114 0.0091627575 0.068520181 ;
	setAttr ".tk[182]" -type "float3" -3.0098958 0.012021446 0.1563821 ;
	setAttr ".tk[183]" -type "float3" -3.3763313 0.014408082 0.26626375 ;
	setAttr ".tk[184]" -type "float3" -2.6546257 0.030917944 0.77429062 ;
	setAttr ".tk[185]" -type "float3" 0.38901207 0.098265953 1.2044833 ;
	setAttr ".tk[186]" -type "float3" 2.0805721 0.143768 1.0818553 ;
	setAttr ".tk[187]" -type "float3" 1.505928 0.10083569 0.81856823 ;
	setAttr ".tk[188]" -type "float3" 2.1195989 0.15899736 1.3221819 ;
	setAttr ".tk[189]" -type "float3" 2.5019517 0.076146327 0.81563151 ;
createNode polySoftEdge -n "polySoftEdge3";
	rename -uid "7079B0D6-40DF-0D27-B379-C1A5D5070930";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 31 "e[1:2]" "e[5:6]" "e[10:12]" "e[15:17]" "e[45:46]" "e[68:69]" "e[71]" "e[96]" "e[147]" "e[152]" "e[156:158]" "e[161:172]" "e[174]" "e[176]" "e[178]" "e[180]" "e[212:243]" "e[245]" "e[247]" "e[249]" "e[251]" "e[253]" "e[255]" "e[257]" "e[259]" "e[261]" "e[263]" "e[265]" "e[267]" "e[269]" "e[271:349]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 12.614814079169303 0 0 0 0 1 0 -19.672549769757108 -8.9906222929659236 0 1;
	setAttr ".a" 180;
createNode polySoftEdge -n "polySoftEdge4";
	rename -uid "D26F09CC-4899-D194-5E17-D48161E5495D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[1]" "e[11]" "e[16]" "e[71:95]" "e[146]" "e[151]" "e[156]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 12.614814079169303 0 0 0 0 1 0 -19.672549769757108 -8.9906222929659236 0 1;
	setAttr ".a" 0;
createNode polySoftEdge -n "polySoftEdge5";
	rename -uid "3F71CE40-4842-BFE6-BAF1-25AB2095C1B7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[5]" "e[161]" "e[224]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 12.614814079169303 0 0 0 0 1 0 -19.672549769757108 -8.9906222929659236 0 1;
	setAttr ".a" 0;
createNode polySoftEdge -n "polySoftEdge6";
	rename -uid "22D35FD9-465F-94E5-FAFA-66BC2EBBB6DB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[120]" "e[220]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 12.614814079169303 0 0 0 0 1 0 -19.672549769757108 -8.9906222929659236 0 1;
	setAttr ".a" 0;
createNode polyTweak -n "polyTweak8";
	rename -uid "94D74FE5-48E9-42E9-7C8A-DB98DC12CE21";
	setAttr ".uopa" yes;
	setAttr -s 3 ".tk[175:176]" -type "float3"  0.59499967 0 0 -0.68906152
		 0 0;
createNode deleteComponent -n "deleteComponent3";
	rename -uid "F65B244E-441D-F583-08A7-24891ADF254B";
	setAttr ".dc" -type "componentList" 1 "f[2]";
createNode deleteComponent -n "deleteComponent4";
	rename -uid "CF0287C0-4AE6-5AD5-5C81-678E471CA062";
	setAttr ".dc" -type "componentList" 1 "f[2]";
createNode polyPlanarProj -n "polyPlanarProj1";
	rename -uid "AFD1C46E-43F2-A3AD-63F3-BF8F4F03DF0E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "f[0]" "f[3]" "f[5]" "f[31:55]" "f[57]" "f[59]" "f[61]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 12.614814079169303 0 0 0 0 1 0 -19.672549769757108 -8.9906222929659236 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.067840576171875 -8.3541481494903564 2.385819673538208 ;
	setAttr ".ps" -type "double2" 40.209419250488281 42.793030364378652 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj2";
	rename -uid "E12C9B34-4723-E4B7-8A11-46BCB5A02AEE";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "f[1:2]" "f[4]" "f[6:30]" "f[56]" "f[58]" "f[60]" "f[62:154]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 12.614814079169303 0 0 0 0 1 0 -19.672549769757108 -8.9906222929659236 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 2.7347183227539063 -1.0962996482849121 -9.1027978658676147 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 48.865180969238281 22.977235078811646 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV1";
	rename -uid "AE364518-46D9-2C4B-E672-D6A33451EFC7";
	setAttr ".uopa" yes;
	setAttr -s 224 ".uvtk[0:223]" -type "float2" 0 0.13301799 -0.0078080185
		 0.13301799 -0.0078080185 0.035315931 0 0.037319541 -0.014509596 0.13301799 -0.014509596
		 0.031894088 -0.018408928 0.13301799 -0.018408928 0.031128883 -0.022125017 0.033736646
		 -0.022125017 0.13301799 -0.033798255 0.13301799 -0.033798255 0.042949259 -0.045471489
		 0.13301799 -0.045471489 0.053214788 -0.057144724 0.13301799 -0.057144724 0.063217044
		 -0.068817943 0.13301799 -0.068817943 0.075325072 -0.080491185 0.13301799 -0.080491185
		 0.081773877 -0.092164397 0.13301799 -0.092164397 0.086812675 -0.10383764 0.13301799
		 -0.10383764 0.091901898 -0.11551085 0.13301799 -0.11551085 0.094557106 -0.12718409
		 0.13301799 -0.12718409 0.095663488 -0.13885733 0.13301799 -0.13885733 0.096548557
		 -0.15053055 0.13301799 -0.15053055 0.098540008 -0.16220379 0.13301799 -0.16220379
		 0.10119522 -0.17387703 0.13301799 -0.17387703 0.1023016 -0.18555027 0.13301799 -0.18555027
		 0.10451436 -0.19722348 0.13301799 -0.19722348 0.10562068 -0.2088967 0.13301799 -0.2088967
		 0.10606325 -0.22056991 0.13301799 -0.22056991 0.10606325 -0.23224318 0.13301799 -0.23224318
		 0.10584199 -0.24391639 0.13301799 -0.24391639 0.10346746 -0.2555896 0.13301799 -0.2555896
		 0.096165538 -0.26726288 0.13301799 -0.26726288 0.085987031 -0.27893609 0.13301799
		 -0.27893609 0.074702263 -0.2906093 0.13301799 -0.2906093 0.05146873 -0.29841387 0.13301799
		 -0.29841387 0.040720105 -0.30228257 0.038413763 -0.30228257 0.13301799 -0.30892009
		 0.13301799 -0.30892009 0.03957212 -0.31395578 0.13301799 -0.31395578 0.042396665
		 -0.00035227463 0.00080153742 -0.0003640838 0.00077089953 -0.00026310235 -0.069195829
		 -0.00025197864 -0.069173001 -0.00036600232 0.00074262824 -0.00025751442 -0.06921114
		 -0.0003759712 0.00072443153 -0.00026158243 -0.069224432 -0.00024934858 -0.069246024
		 -0.00036957115 0.00070549373 -0.00033302605 0.00063151092 -0.00019864738 -0.069324106
		 -0.000275895 0.0005462165 -0.00013354421 -0.06941364 -0.00021781027 0.00045634739
		 -7.4207783e-005 -0.069505453 -0.00011497736 0.00036870173 1.8298626e-005 -0.06960398
		 -9.2193484e-005 0.00029315994 2.7343631e-005 -0.069678128 -8.0496073e-005 0.00022862307
		 2.43783e-005 -0.06974034 -6.6399574e-005 0.00017433459 2.2053719e-005 -0.069795802
		 -6.6667795e-005 0.00013111747 6.0200691e-006 -0.069837436 -7.1942806e-005 9.6870914e-005
		 -1.2755394e-005 -0.069868289 -7.7873468e-005 6.8349145e-005 -3.015995e-005 -0.069893546
		 -8.0496073e-005 4.342071e-005 -4.4316053e-005 -0.069918007 -7.8737736e-005 2.2852326e-005
		 -5.6028366e-005 -0.069940276 -8.0019236e-005 8.2898696e-006 -7.1465969e-005 -0.069953322
		 -7.5697899e-005 2.9802322e-008 -8.4340572e-005 -0.06996353 -7.1883202e-005 8.0162789e-007
		 -0.00010031462 -0.069963455 -6.6459179e-005 1.2430126e-005 -0.00011754036 -0.069952741
		 -5.8531761e-005 3.7302332e-005 -0.00013589859 -0.069930509 -4.7564507e-005 7.9308047e-005
		 -0.00015509129 -0.069893673 -3.0636787e-005 0.00014403916 -0.00017255545 -0.069835491
		 2.2411346e-005 0.00023551327 -0.00015377998 -0.069745779 0.0001103282 0.00035224293
		 -9.572506e-005 -0.069629818 0.00021088123 0.00049184624 -1.9133091e-005 -0.06949503
		 0.00061649084 0.00064348697 0.00039124489 -0.069299005 0.0007327795 0.00072975253
		 0.00052195787 -0.069199599 0.00053143501 -0.069164321 0.00073498487 0.0007677509
		 0.00072038174 0.00082571962 0.00052344799 -0.069120161 0.00072461367 0.00086714263
		 0.00052857399 -0.069092169 -0.00021412224 -0.11322515 -0.00020388328 -0.1132179 -0.00021088123
		 -0.11322001 -0.00022160262 -0.11323325 -0.00016757846 -0.11335677 -0.00021620095
		 -0.1132666 -0.0001039505 -0.11345486 -4.5508146e-005 -0.11355072 4.8056245e-005 -0.11366306
		 5.4895878e-005 -0.11372656 4.9740076e-005 -0.11378314 4.5746565e-005 -0.11383869
		 2.7626753e-005 -0.11387451 4.9769878e-006 -0.11389921 -1.8417835e-005 -0.11392047
		 -3.9130449e-005 -0.11394556 -5.6177378e-005 -0.11397038 -7.8260899e-005 -0.11398075
		 -9.7930431e-005 -0.11399432 -0.00012028217 -0.11399508 -0.00014448166 -0.11398588
		 -0.00016975403 -0.11396681 -0.00019460917 -0.11394045 -0.00021451712 -0.11389703
		 -0.00019872189 -0.11381526 -0.00014400482 -0.11370303 -7.3611736e-005 -0.11358443
		 0.00033509731 -0.11331763 0.00046527386 -0.11319874 0.00045353174 -0.1131243 0.00046902895
		 -0.1131557 0.00045919418 -0.1131205 -0.0001338385 -0.24029522 -0.00012729876 -0.24021508
		 -0.00014239177 -0.24096815 -0.00017528236 -0.24148691 -8.9198351e-005 -0.24031082
		 -0.00017276406 -0.24081722 -2.7298927e-005 -0.24043083 3.6433339e-005 -0.24065793
		 7.0750713e-005 -0.24087054 6.1869621e-005 -0.24080345 5.2928925e-005 -0.24078418
		 4.6253204e-005 -0.24088091 2.8192997e-005 -0.24087177 3.7252903e-006 -0.24082857
		 -2.7805567e-005 -0.2408361 -5.8859587e-005 -0.24087784 -8.2403421e-005 -0.24092029
		 -0.00011241436 -0.24092595 -0.00014126301 -0.24093102 -0.00016832352 -0.24092022
		 -0.00019836426 -0.24090777 -0.00022149086 -0.24003552 -0.00024646521 -0.2380165 -0.00026857853
		 -0.24124904 -0.00023049116 -0.2410202 -0.00017690659 -0.24084815 -0.00012761354 -0.24086149
		 0.0002835989 -0.24059881 0.0003965497 -0.24098241 0.0003361702 -0.24075492 0.00037181377
		 -0.24137546 0.00033885241 -0.24021898 -5.4528937e-007 -0.51755881 0 -0.5153085 -4.2911619e-005
		 -0.52187026 -7.198751e-005 -0.52264345 -2.7224422e-005 -0.52924824 -7.0050359e-005
		 -0.5277878 -4.2021275e-006 -0.51631945 1.8358231e-005 -0.50854206 -3.0994415e-006
		 -0.50863874 1.9550323e-005 -0.52285099 4.9769878e-006 -0.51868993 -8.3446503e-007
		 -0.52092171 -7.3313713e-006 -0.52359247 -2.348423e-005 -0.52156687 -4.0322542e-005
		 -0.51577246 -6.2018633e-005 -0.5109058 -7.1555376e-005 -0.50952989 -7.7545643e-005
		 -0.50741947 -8.4400177e-005 -0.51217282 -9.4592571e-005 -0.51449519 -0.00010192394
		 -0.51229286 -0.0001116991 -0.50983125 -9.727478e-005 -0.49928686 -9.8466873e-005
		 -0.49150214 -7.3850155e-005 -0.4936769 -3.6597252e-005 -0.49782518 5.1558018e-005
		 -0.48768875 9.7513199e-005 -0.50097871 0.00018614531 -0.52208352 1.0311604e-005 -0.51971865
		 9.6917152e-005 -0.52383721 0 -0.51630974;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr ":defaultColorMgtGlobals.cme" "imagePlaneShape1.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "imagePlaneShape1.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "imagePlaneShape1.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "imagePlaneShape1.ws";
connectAttr ":frontShape.msg" "imagePlaneShape1.ltc";
connectAttr "groupId1.id" "pCubeShape1.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape1.iog.og[0].gco";
connectAttr "polyTweakUV1.out" "pCubeShape1.i";
connectAttr "polyTweakUV1.uvtk[0]" "pCubeShape1.uvst[0].uvtw";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyTweak1.out" "polyExtrudeFace1.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polyCube1.out" "polyTweak1.ip";
connectAttr "polyExtrudeFace1.out" "polyExtrudeFace2.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyExtrudeFace2.out" "polyExtrudeFace3.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace3.mp";
connectAttr "polyTweak2.out" "polySplitRing1.ip";
connectAttr "pCubeShape1.wm" "polySplitRing1.mp";
connectAttr "polyExtrudeFace3.out" "polyTweak2.ip";
connectAttr "polyTweak3.out" "polySplitRing2.ip";
connectAttr "pCubeShape1.wm" "polySplitRing2.mp";
connectAttr "polySplitRing1.out" "polyTweak3.ip";
connectAttr "polyTweak4.out" "polySplitRing3.ip";
connectAttr "pCubeShape1.wm" "polySplitRing3.mp";
connectAttr "polySplitRing2.out" "polyTweak4.ip";
connectAttr "polyTweak5.out" "polySoftEdge1.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge1.mp";
connectAttr "polySplitRing3.out" "polyTweak5.ip";
connectAttr "polySoftEdge1.out" "polySoftEdge2.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge2.mp";
connectAttr "polySoftEdge2.out" "polyTweak6.ip";
connectAttr "polyTweak6.out" "deleteComponent1.ig";
connectAttr "groupId1.msg" "set1.gn" -na;
connectAttr "pCubeShape1.iog.og[0]" "set1.dsm" -na;
connectAttr "deleteComponent1.og" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "groupParts1.og" "deleteComponent2.ig";
connectAttr "deleteComponent2.og" "polyExtrudeEdge1.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeEdge1.mp";
connectAttr "polyExtrudeEdge1.out" "polyExtrudeEdge2.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeEdge2.mp";
connectAttr "polyExtrudeEdge2.out" "polyExtrudeEdge3.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeEdge3.mp";
connectAttr "polyTweak7.out" "polyMapDel1.ip";
connectAttr "polyExtrudeEdge3.out" "polyTweak7.ip";
connectAttr "polyMapDel1.out" "polySoftEdge3.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge3.mp";
connectAttr "polySoftEdge3.out" "polySoftEdge4.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge4.mp";
connectAttr "polySoftEdge4.out" "polySoftEdge5.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge5.mp";
connectAttr "polySoftEdge5.out" "polySoftEdge6.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge6.mp";
connectAttr "polySoftEdge6.out" "polyTweak8.ip";
connectAttr "polyTweak8.out" "deleteComponent3.ig";
connectAttr "deleteComponent3.og" "deleteComponent4.ig";
connectAttr "deleteComponent4.og" "polyPlanarProj1.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj1.mp";
connectAttr "polyPlanarProj1.out" "polyPlanarProj2.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj2.mp";
connectAttr "polyPlanarProj2.out" "polyTweakUV1.ip";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCubeShape1.iog" ":initialShadingGroup.dsm" -na;
// End of MountainSection2_model_002.ma
