//Maya ASCII 2017ff05 scene
//Name: MountainSectionOne_model_003.ma
//Last modified: Mon, Sep 18, 2017 07:17:09 PM
//Codeset: 1252
requires maya "2017ff05";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Home Premium Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "318AFCAB-42D3-9913-669F-7AB9BE343052";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 7.5976488734981347 33.45504058507067 82.99263371187503 ;
	setAttr ".r" -type "double3" -22.538352729589544 5.4000000000033603 -5.990124411916164e-016 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "FEB172EA-495A-86D2-5428-6E8CC51FCF8D";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 86.915798972037663;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "6E622639-443D-7EB0-5622-B9B955917FF9";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "C87E6A96-436A-6E05-419B-B293F755ADAD";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "0F75CB09-44E1-9C4C-4488-33BF27F53337";
	setAttr ".t" -type "double3" 6.2005202440481675 10.204039518362952 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "701C2857-4033-474C-3AD8-398ED23C94DE";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 110.62162136763259;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "7F46EF79-4945-EC0E-7708-4A9F638A5E00";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "0EF4DB6A-4666-6C11-8AAA-249105BFA22D";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 94.907164424979172;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pCube1";
	rename -uid "B13D8308-47C9-BA9C-229C-CEBFE61A51DE";
	setAttr ".t" -type "double3" -18.84567503947536 -9.358297536725777 0 ;
	setAttr ".s" -type "double3" 0.77777777958737127 3.0666666498374471 1 ;
createNode transform -n "transform1" -p "pCube1";
	rename -uid "952738AB-4F33-43E2-EA6C-0A9C7B528868";
	setAttr ".v" no;
createNode mesh -n "pCubeShape1" -p "transform1";
	rename -uid "02F62683-491C-E6B9-D44E-86824DF72BB4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[156:159]" -type "float3"  -0.023265362 -0.028133169 
		0 0.023265362 0.028133169 0 0.023265362 0.028133169 0 -0.023265362 -0.028133169 0;
createNode transform -n "imagePlane1";
	rename -uid "A2634C01-4CEC-8A84-3323-EDA9AC20361B";
	setAttr ".t" -type "double3" 0 0 -16.555563100055583 ;
	setAttr ".r" -type "double3" 0 0 89.834491075586413 ;
createNode imagePlane -n "imagePlaneShape1" -p "imagePlane1";
	rename -uid "753220F0-4416-F31D-2951-0AA710F2B6DA";
	setAttr -k off ".v";
	setAttr ".fc" 204;
	setAttr ".imn" -type "string" "C:/Users/jonat/Git Repos/game-fall2017/Design/Level Design/IMG_20170918_141818.jpg";
	setAttr ".cov" -type "short2" 3024 4032 ;
	setAttr ".dlc" no;
	setAttr ".w" 30.24;
	setAttr ".h" 40.319999999999993;
	setAttr ".cs" -type "string" "sRGB";
createNode transform -n "pCube2";
	rename -uid "5776AC70-410C-3B97-FA08-208C739A2C16";
	setAttr ".t" -type "double3" 5.820970736298074 -3.6101179249830184 0 ;
createNode transform -n "transform4" -p "pCube2";
	rename -uid "5A65E55C-46F7-2217-8E3C-22BA64CCB56E";
	setAttr ".v" no;
createNode mesh -n "pCubeShape2" -p "transform4";
	rename -uid "990EF537-4ABB-733F-5A1C-86A1097009B0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt";
	setAttr ".pt[22]" -type "float3" 0 -0.034281313 0 ;
	setAttr ".pt[29]" -type "float3" 0 -0.034281313 0 ;
	setAttr ".pt[48]" -type "float3" 4.4408921e-016 0.055009801 0 ;
	setAttr ".pt[49]" -type "float3" 4.4408921e-016 0.055009801 0 ;
	setAttr ".pt[50]" -type "float3" 4.4408921e-016 -0.055009801 0 ;
	setAttr ".pt[51]" -type "float3" 4.4408921e-016 -0.055009801 0 ;
createNode transform -n "pCube3";
	rename -uid "94EF719F-4D06-6176-0B20-32BE1386122B";
	setAttr ".t" -type "double3" 1.5095241642941275 -0.57424726185597841 0 ;
createNode mesh -n "polySurfaceShape1" -p "pCube3";
	rename -uid "1BDF655A-40E5-1FDA-D979-4694D5070AF0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.66636461019515991 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 59 ".uvst[0].uvsp[0:58]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0 0.625 0 0.625 0 0.625 0 0.625 0 0.625 0 0.625
		 0 0.875 0 0.875 0 0.875 0 0.875 0 0.875 0 0.875 0 0.875 0 0.875 0.25 0.875 0.25 0.875
		 0.25 0.875 0.25 0.875 0.25 0.875 0.25 0.875 0.25 0.625 0.25 0.625 0.25 0.625 0.25
		 0.625 0.25 0.625 0.25 0.625 0.25 0.625 0.25 0.625 0.25 0.875 0.25 0.875 0 0.625 0
		 0.625 0.25 0.875 0.25 0.875 0 0.625 0 0.625 0.25 0.875 0.25 0.875 0 0.625 0 0.45772922
		 0.25 0.45772922 0.5 0.45772922 0.75000006 0.45772922 0 0.45772922 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 52 ".pt[0:51]" -type "float3"  -1.5553874 -1.6260866 0 -1.5553874 
		-1.6260866 0 -1.5553874 -1.6260866 0 -1.5553874 -1.6260866 0 -1.5553874 -1.6260866 
		0 -1.5553874 -1.6260866 0 -1.5553874 -1.6260866 0 -1.5553874 -1.6260866 0 0 -1.0039319 
		0 0 -0.56057137 0 0 -0.32599261 0 0 -0.1169412 0 -1.7763568e-015 -0.02187201 0 0.31107739 
		-0.035714798 0 0.65043455 -0.14139865 0 0 -1.0039319 0 0 -0.56057137 0 0 -0.32599261 
		0 0 -0.1169412 0 -1.7763568e-015 -0.02187201 0 0.31107739 -0.035714798 0 0.65043461 
		-0.14139865 0 0 -1.0382133 0 0 -0.65545863 0 0 -0.1264835 0 0 0.1169412 0 -1.7763568e-015 
		0.33294946 0 0.31107739 0.063994542 0 0.65043455 -0.14139865 0 0 -1.0382133 0 0 -0.65545863 
		0 0 -0.1264835 0 0 0.1169412 0 -1.7763568e-015 0.33294946 0 0.31107739 0.063994542 
		0 0.65043461 -0.14139865 0 0.57973504 -0.012636535 0 0.57973504 -0.012636535 0 0.57973504 
		-0.072202735 0 0.57973504 -0.072202735 0 0.65043485 -0.056559518 0 0.65043485 -0.056559518 
		0 0.65043485 -0.056559518 0 0.65043485 -0.056559518 0 0.65043455 -0.048848882 0 0.65043455 
		-0.048848882 0 0.65043455 -0.064270601 0 0.65043455 -0.064270601 0 -1.5553874 -1.571077 
		0 -1.5553874 -1.571077 0 -1.5553874 -1.6810963 0 -1.5553874 -1.6810963 0;
	setAttr -s 52 ".vt[0:51]"  -0.31111097 -0.10082442 0.5 0.19769049 -0.46965891 0.5
		 -0.31111097 0.10082442 0.5 0.19769049 0.28632686 0.5 -0.31111097 0.10082442 -0.5
		 0.19769049 0.28632686 -0.5 -0.31111097 -0.10082442 -0.5 0.19769049 -0.46965891 -0.5
		 1.26494551 -0.85158652 -0.5 2.33220005 -1.15685666 -0.5 3.39945412 -1.3089081 -0.5
		 4.46670914 -1.37817121 -0.5 5.60271358 -1.40684462 -0.5 6.60121727 -1.35190809 -0.5
		 7.66847229 -0.87297583 -0.5 1.26494551 -0.85158652 0.5 2.33220005 -1.15685666 0.5
		 3.39945412 -1.3089081 0.5 4.46670914 -1.37817121 0.5 5.60271358 -1.40684462 0.5 6.60121727 -1.35190809 0.5
		 7.66847229 -0.87297583 0.5 1.26494551 0.35040468 -0.5 2.33220005 0.17163396 -0.5
		 3.39945412 0.051294744 -0.5 4.46670914 -0.068362817 -0.5 5.60271358 -0.19731081 -0.5
		 6.60121727 -0.23510647 -0.5 7.66847229 -0.6965239 -0.5 1.26494551 0.35040468 0.5
		 2.33220005 0.17163396 0.5 3.39945412 0.051294744 0.5 4.46670914 -0.068362817 0.5
		 5.60271358 -0.19731081 0.5 6.60121727 -0.23510647 0.5 7.66847229 -0.6965239 0.5 7.3414259 -0.49701709 0.5
		 7.3414259 -0.49701709 -0.5 7.3414259 -1.16419256 -0.5 7.3414259 -1.16419256 0.5 6.94688988 -0.32470554 0.5
		 6.94688988 -0.32470554 -0.5 6.94688988 -1.28038275 -0.5 6.94688988 -1.28038275 0.5
		 7.52444839 -0.58294547 0.5 7.52444839 -0.58294547 -0.5 7.52444839 -1.045597076 -0.5
		 7.52444839 -1.045597076 0.5 -0.14274004 0.16221029 0.5 -0.14274004 0.16221029 -0.5
		 -0.14274004 -0.22287795 -0.5 -0.14274004 -0.22287795 0.5;
	setAttr -s 100 ".ed[0:99]"  0 51 0 2 48 0 4 49 0 6 50 0 0 2 0 1 3 1 2 4 0
		 3 5 1 4 6 0 5 7 1 6 0 0 7 1 1 7 8 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 42 0
		 1 15 0 15 16 0 16 17 0 17 18 0 18 19 0 19 20 0 20 43 0 8 15 1 9 16 1 10 17 1 11 18 1
		 12 19 1 13 20 1 14 21 0 5 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 41 0 22 8 1
		 23 9 1 24 10 1 25 11 1 26 12 1 27 13 1 28 14 0 3 29 0 29 30 0 30 31 0 31 32 0 32 33 0
		 33 34 0 34 40 0 29 22 1 30 23 1 31 24 1 32 25 1 33 26 1 34 27 1 35 28 0 15 29 1 16 30 1
		 17 31 1 18 32 1 19 33 1 20 34 1 21 35 0 36 44 0 37 45 0 36 37 1 38 46 0 37 38 1 39 47 0
		 38 39 1 39 36 1 40 36 0 41 37 0 40 41 1 42 38 0 41 42 1 43 39 0 42 43 1 43 40 1 44 35 0
		 45 28 0 44 45 1 46 14 0 45 46 1 47 21 0 46 47 1 47 44 1 48 3 0 49 5 0 48 49 1 50 7 0
		 49 50 1 51 1 0 50 51 1 51 48 1;
	setAttr -s 50 -ch 200 ".fc[0:49]" -type "polyFaces" 
		f 4 0 99 -2 -5
		mu 0 4 0 57 54 2
		f 4 1 94 -3 -7
		mu 0 4 2 54 55 4
		f 4 2 96 -4 -9
		mu 0 4 4 55 56 6
		f 4 3 98 -1 -11
		mu 0 4 6 56 58 8
		f 4 -33 -47 -61 -68
		mu 0 4 20 27 34 41
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -12 12 26 -20
		mu 0 4 1 10 21 14
		f 4 -27 13 27 -21
		mu 0 4 14 21 22 15
		f 4 -28 14 28 -22
		mu 0 4 15 22 23 16
		f 4 -29 15 29 -23
		mu 0 4 16 23 24 17
		f 4 -30 16 30 -24
		mu 0 4 17 24 25 18
		f 4 -31 17 31 -25
		mu 0 4 18 25 26 19
		f 4 -32 18 82 -26
		mu 0 4 19 26 48 49
		f 4 -10 33 40 -13
		mu 0 4 10 11 28 21
		f 4 -41 34 41 -14
		mu 0 4 21 28 29 22
		f 4 -42 35 42 -15
		mu 0 4 22 29 30 23
		f 4 -43 36 43 -16
		mu 0 4 23 30 31 24
		f 4 -44 37 44 -17
		mu 0 4 24 31 32 25
		f 4 -45 38 45 -18
		mu 0 4 25 32 33 26
		f 4 -46 39 80 -19
		mu 0 4 26 33 47 48
		f 4 -8 47 54 -34
		mu 0 4 11 3 35 28
		f 4 -55 48 55 -35
		mu 0 4 28 35 36 29
		f 4 -56 49 56 -36
		mu 0 4 29 36 37 30
		f 4 -57 50 57 -37
		mu 0 4 30 37 38 31
		f 4 -58 51 58 -38
		mu 0 4 31 38 39 32
		f 4 -59 52 59 -39
		mu 0 4 32 39 40 33
		f 4 -60 53 78 -40
		mu 0 4 33 40 46 47
		f 4 -6 19 61 -48
		mu 0 4 3 1 14 35
		f 4 -62 20 62 -49
		mu 0 4 35 14 15 36
		f 4 -63 21 63 -50
		mu 0 4 36 15 16 37
		f 4 -64 22 64 -51
		mu 0 4 37 16 17 38
		f 4 -65 23 65 -52
		mu 0 4 38 17 18 39
		f 4 -66 24 66 -53
		mu 0 4 39 18 19 40
		f 4 -67 25 83 -54
		mu 0 4 40 19 49 46
		f 4 -71 68 86 -70
		mu 0 4 43 42 50 51
		f 4 -73 69 88 -72
		mu 0 4 44 43 51 52
		f 4 -75 71 90 -74
		mu 0 4 45 44 52 53
		f 4 -76 73 91 -69
		mu 0 4 42 45 53 50
		f 4 -79 76 70 -78
		mu 0 4 47 46 42 43
		f 4 -81 77 72 -80
		mu 0 4 48 47 43 44
		f 4 -83 79 74 -82
		mu 0 4 49 48 44 45
		f 4 -84 81 75 -77
		mu 0 4 46 49 45 42
		f 4 -87 84 60 -86
		mu 0 4 51 50 41 34
		f 4 -89 85 46 -88
		mu 0 4 52 51 34 27
		f 4 -91 87 32 -90
		mu 0 4 53 52 27 20
		f 4 -92 89 67 -85
		mu 0 4 50 53 20 41
		f 4 92 7 -94 -95
		mu 0 4 54 3 5 55
		f 4 -97 93 9 -96
		mu 0 4 56 55 5 7
		f 4 -99 95 11 -98
		mu 0 4 58 56 7 9
		f 4 -100 97 5 -93
		mu 0 4 54 57 1 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform3" -p "pCube3";
	rename -uid "34942212-439C-3D3D-DF93-A8B9E54F0308";
	setAttr ".v" no;
createNode mesh -n "pCubeShape3" -p "transform3";
	rename -uid "7E5DDAE6-41D4-B93C-67F4-12A4DE6C81FD";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".pt";
	setAttr ".pt[1]" -type "float3" 0 0.042419638 0 ;
	setAttr ".pt[3]" -type "float3" 0 0.042419638 0 ;
	setAttr ".pt[5]" -type "float3" 0 0.042419638 0 ;
	setAttr ".pt[7]" -type "float3" 0 0.042419638 0 ;
	setAttr ".pt[52]" -type "float3" 6.6613381e-016 0.068223104 0 ;
	setAttr ".pt[53]" -type "float3" 6.6613381e-016 0.068223104 0 ;
	setAttr ".pt[54]" -type "float3" 6.6613381e-016 -0.039943371 0 ;
	setAttr ".pt[55]" -type "float3" 6.6613381e-016 -0.039943356 0 ;
	setAttr ".pt[56]" -type "float3" 3.3306691e-016 0.070310652 0 ;
	setAttr ".pt[57]" -type "float3" 3.3306691e-016 0.070310652 0 ;
	setAttr ".pt[58]" -type "float3" 3.3306691e-016 -0.070310652 0 ;
	setAttr ".pt[59]" -type "float3" 3.3306691e-016 -0.070310652 0 ;
createNode transform -n "pCube4";
	rename -uid "1AC64BFB-47FD-5279-9AA9-4DAAE57041CA";
	setAttr ".t" -type "double3" 3.039627869647008 0.57253612049626612 0 ;
createNode transform -n "transform5" -p "pCube4";
	rename -uid "92E95168-4015-D491-E94E-82987BAFCD8D";
	setAttr ".v" no;
createNode mesh -n "pCubeShape4" -p "transform5";
	rename -uid "AB46DB1E-4475-0C19-F274-6B957DC510D6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt";
	setAttr ".pt[96]" -type "float3" 0.10302465 0.038634244 0 ;
	setAttr ".pt[97]" -type "float3" 0.10302465 0.038634244 0 ;
	setAttr ".pt[98]" -type "float3" 0.051512327 -0.090146571 0 ;
	setAttr ".pt[99]" -type "float3" 0.051512327 -0.090146571 0 ;
	setAttr ".pt[112]" -type "float3" -0.014480437 -0.14189099 0 ;
	setAttr ".pt[113]" -type "float3" -0.014480437 -0.14189099 0 ;
	setAttr ".pt[114]" -type "float3" 0.014480437 0.14189099 0 ;
	setAttr ".pt[115]" -type "float3" 0.014480437 0.14189099 0 ;
createNode transform -n "pCube5";
	rename -uid "415BB093-40D4-DB28-9559-D79FFE2B65E6";
	setAttr ".t" -type "double3" -4.7137335680730006 -1.8679299996434993 0 ;
	setAttr ".r" -type "double3" 0 0 48.011647555934978 ;
createNode transform -n "transform2" -p "pCube5";
	rename -uid "488F37E0-4159-3D97-8A3C-F993FFD1E704";
	setAttr ".v" no;
createNode mesh -n "pCubeShape5" -p "transform2";
	rename -uid "8C7ADEE2-4935-6FC5-1AC6-4EBDAFC706AB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[164:167]" -type "float3"  0.72567266 -0.56710452 0 
		0.72567266 -0.56710452 0 -0.60038465 0.38036203 0 -0.60038465 0.38036203 0;
createNode transform -n "pCube6";
	rename -uid "A86EDD6B-4944-4A42-10F5-5E9A8DB8B4AB";
	setAttr ".t" -type "double3" 6.9817939883132452 -3.7818050770030065 -0.98794977195228117 ;
createNode mesh -n "pCubeShape6" -p "pCube6";
	rename -uid "F7B8F901-4D6E-A4A7-DA57-DEA3489BE58E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt";
	setAttr ".pt[17]" -type "float3" -0.02146939 0 0 ;
	setAttr ".pt[18]" -type "float3" -0.02146939 0 0 ;
	setAttr ".pt[32]" -type "float3" 8.4921246 0.36499205 0 ;
	setAttr ".pt[33]" -type "float3" -3.2171147 -0.36499205 0 ;
	setAttr ".pt[34]" -type "float3" -3.2171147 -0.36499205 0 ;
	setAttr ".pt[35]" -type "float3" 8.4921246 0.36499205 0 ;
createNode transform -n "pCube7";
	rename -uid "015884FF-41F1-E136-E731-5A8B148E2644";
	setAttr ".rp" -type "double3" 2.3593366928011807 4.9473949785668649 0 ;
	setAttr ".sp" -type "double3" 2.3593366928011807 4.9473949785668649 0 ;
createNode mesh -n "pCube7Shape" -p "pCube7";
	rename -uid "B12E10AF-4C00-243F-5480-10B909AD6EBC";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "B3C6C1B5-4073-7FA7-2170-529ADB69E9BB";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "527709CB-4772-0A9A-949A-C098756C3F0E";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "8EF196F0-4922-913E-756F-C2AFD00D27D0";
createNode displayLayerManager -n "layerManager";
	rename -uid "7193DA8B-46AD-2ECB-FC35-3294707F4FDD";
createNode displayLayer -n "defaultLayer";
	rename -uid "10285AAE-449C-3FC5-4BBC-F3BEA14B69E7";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "9A449396-4E26-7C6A-97A7-8C81A4E58FEF";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "EA2026E5-4C9F-F9A3-A006-8BB2C9E459E4";
	setAttr ".g" yes;
createNode polyCube -n "polyCube1";
	rename -uid "59EB9921-4B2F-3089-6A28-C085B59A67E0";
	setAttr ".cuv" 4;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "62C55522-48CA-0205-BE3B-A7868445987D";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.77777777958737127 0 0 0 0 3.0666666498374471 0 0 0 0 1 0
		 -18.84567503947536 -9.358297536725777 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -18.456785 -9.3582973 0 ;
	setAttr ".rs" 49181;
	setAttr ".lt" -type "double3" -8.5124684876691151e-015 -6.3417271161526855e-016 
		38.336749909069731 ;
	setAttr ".d" 25;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -18.456786149681673 -10.891630861644501 -0.5 ;
	setAttr ".cbx" -type "double3" -18.456786149681673 -7.8249642118070533 0.5 ;
createNode polySplitRing -n "polySplitRing1";
	rename -uid "BD432C11-4AF9-7CF2-A05A-74B61E226895";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[36]" "e[61]" "e[111]" "e[161]";
	setAttr ".ix" -type "matrix" 0.77777777958737127 0 0 0 0 3.0666666498374471 0 0 0 0 1 0
		 -18.84567503947536 -9.358297536725777 0 1;
	setAttr ".wt" 0.36550670862197876;
	setAttr ".re" 161;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak1";
	rename -uid "C442D215-4392-EBDA-4C89-D083016139D7";
	setAttr ".uopa" yes;
	setAttr -s 54 ".tk";
	setAttr ".tk[3]" -type "float3" 0 0.015803853 0 ;
	setAttr ".tk[5]" -type "float3" 0 0.015803853 0 ;
	setAttr ".tk[32]" -type "float3" 0.28343338 0 0 ;
	setAttr ".tk[57]" -type "float3" 0.28343338 0 0 ;
	setAttr ".tk[58]" -type "float3" 0 0.27639797 0 ;
	setAttr ".tk[59]" -type "float3" 0 0.84762061 0 ;
	setAttr ".tk[60]" -type "float3" 0 1.0134593 0 ;
	setAttr ".tk[61]" -type "float3" 0 1.4188426 0 ;
	setAttr ".tk[62]" -type "float3" 0 0.91211337 0 ;
	setAttr ".tk[63]" -type "float3" 0 0.5251562 0 ;
	setAttr ".tk[64]" -type "float3" 0 0.58964896 0 ;
	setAttr ".tk[65]" -type "float3" 0 0.90290022 0 ;
	setAttr ".tk[66]" -type "float3" 0 1.0410987 0 ;
	setAttr ".tk[67]" -type "float3" 3.5527137e-015 1.0963781 0 ;
	setAttr ".tk[68]" -type "float3" 0 1.0503122 0 ;
	setAttr ".tk[69]" -type "float3" 0 0.89368689 0 ;
	setAttr ".tk[70]" -type "float3" 0 0.81998062 0 ;
	setAttr ".tk[71]" -type "float3" 0 0.71863467 0 ;
	setAttr ".tk[72]" -type "float3" 0 0.66335511 0 ;
	setAttr ".tk[73]" -type "float3" 0 0.59886235 0 ;
	setAttr ".tk[74]" -type "float3" 0 0.58964902 0 ;
	setAttr ".tk[75]" -type "float3" 0 0.58964902 0 ;
	setAttr ".tk[76]" -type "float3" 0 0.55279595 0 ;
	setAttr ".tk[77]" -type "float3" 0 0.46066329 0 ;
	setAttr ".tk[78]" -type "float3" 0 0.50672966 0 ;
	setAttr ".tk[79]" -type "float3" 0 0.54358268 0 ;
	setAttr ".tk[80]" -type "float3" 0 0.66335517 0 ;
	setAttr ".tk[81]" -type "float3" 0 0.86604697 0 ;
	setAttr ".tk[82]" -type "float3" 0.28343338 0.93053997 0 ;
	setAttr ".tk[83]" -type "float3" 0 0.27639797 0 ;
	setAttr ".tk[84]" -type "float3" 0 0.84762061 0 ;
	setAttr ".tk[85]" -type "float3" 0 1.0134593 0 ;
	setAttr ".tk[86]" -type "float3" 0 1.4188426 0 ;
	setAttr ".tk[87]" -type "float3" 0 0.91211337 0 ;
	setAttr ".tk[88]" -type "float3" 0 0.5251562 0 ;
	setAttr ".tk[89]" -type "float3" 0 0.58964896 0 ;
	setAttr ".tk[90]" -type "float3" 0 0.90290022 0 ;
	setAttr ".tk[91]" -type "float3" 0 1.0410987 0 ;
	setAttr ".tk[92]" -type "float3" 3.5527137e-015 1.0963781 0 ;
	setAttr ".tk[93]" -type "float3" 0 1.0503122 0 ;
	setAttr ".tk[94]" -type "float3" 0 0.89368689 0 ;
	setAttr ".tk[95]" -type "float3" 0 0.81998062 0 ;
	setAttr ".tk[96]" -type "float3" 0 0.71863467 0 ;
	setAttr ".tk[97]" -type "float3" 0 0.66335511 0 ;
	setAttr ".tk[98]" -type "float3" 0 0.59886235 0 ;
	setAttr ".tk[99]" -type "float3" 0 0.58964902 0 ;
	setAttr ".tk[100]" -type "float3" 0 0.58964902 0 ;
	setAttr ".tk[101]" -type "float3" 0 0.55279595 0 ;
	setAttr ".tk[102]" -type "float3" 0 0.46066329 0 ;
	setAttr ".tk[103]" -type "float3" 0 0.50672966 0 ;
	setAttr ".tk[104]" -type "float3" 0 0.54358268 0 ;
	setAttr ".tk[105]" -type "float3" 0 0.66335517 0 ;
	setAttr ".tk[106]" -type "float3" 0 0.86604697 0 ;
	setAttr ".tk[107]" -type "float3" 0.28343338 0.93053997 0 ;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "DF37AAF2-4F7B-C15C-E5B0-B5AA20E5402F";
	setAttr ".ics" -type "componentList" 1 "f[106]";
	setAttr ".ix" -type "matrix" 0.77777777958737127 0 0 0 0 3.0666666498374471 0 0 0 0 1 0
		 -18.84567503947536 -9.358297536725777 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 19.543989 -5.0340528 0 ;
	setAttr ".rs" 45027;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 18.987565981436674 -5.0967974876327418 -0.5 ;
	setAttr ".cbx" -type "double3" 20.100411510827058 -4.9713083328699437 0.5 ;
createNode polySplitRing -n "polySplitRing2";
	rename -uid "C1CC0D4A-4273-C6A6-AA67-F197323873A4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[220:221]" "e[223]" "e[225]";
	setAttr ".ix" -type "matrix" 0.77777777958737127 0 0 0 0 3.0666666498374471 0 0 0 0 1 0
		 -18.84567503947536 -9.358297536725777 0 1;
	setAttr ".wt" 0.073386348783969879;
	setAttr ".re" 220;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak2";
	rename -uid "658F0860-4C63-60C2-8218-A8A4D4CFEAF0";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[112:115]" -type "float3"  0 2.22039437 0 0 2.22039437
		 0 0 2.22039437 0 0 2.22039437 0;
createNode polySplitRing -n "polySplitRing3";
	rename -uid "A6481678-46F8-200A-0B6B-AC8971506DFE";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[228:229]" "e[231]" "e[233]";
	setAttr ".ix" -type "matrix" 0.77777777958737127 0 0 0 0 3.0666666498374471 0 0 0 0 1 0
		 -18.84567503947536 -9.358297536725777 0 1;
	setAttr ".wt" 0.089560143649578094;
	setAttr ".re" 228;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing4";
	rename -uid "F3C40C5D-433D-9D7E-C5C0-66B9391B69D6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[236:237]" "e[239]" "e[241]";
	setAttr ".ix" -type "matrix" 0.77777777958737127 0 0 0 0 3.0666666498374471 0 0 0 0 1 0
		 -18.84567503947536 -9.358297536725777 0 1;
	setAttr ".wt" 0.10820721834897995;
	setAttr ".re" 236;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing5";
	rename -uid "F5F5A0E3-4ED1-D708-0E66-BDB080FCDBA2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[244:245]" "e[247]" "e[249]";
	setAttr ".ix" -type "matrix" 0.77777777958737127 0 0 0 0 3.0666666498374471 0 0 0 0 1 0
		 -18.84567503947536 -9.358297536725777 0 1;
	setAttr ".wt" 0.12685203552246094;
	setAttr ".re" 244;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "C5F27617-45B4-E85D-D8AB-7FB67C6B19CA";
	setAttr ".ics" -type "componentList" 1 "f[126]";
	setAttr ".ix" -type "matrix" 0.77777777958737127 0 0 0 0 3.0666666498374471 0 0 0 0 1 0
		 -18.84567503947536 -9.358297536725777 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 19.126179 -0.36358631 0 ;
	setAttr ".rs" 60595;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 18.272614146684063 -2.7605836256364968 -0.5 ;
	setAttr ".cbx" -type "double3" 19.979744158929563 2.0334109973063743 0.5 ;
createNode polyTweak -n "polyTweak3";
	rename -uid "8619577D-4376-FEC8-A78A-BAB36F3C2B9F";
	setAttr ".uopa" yes;
	setAttr -s 18 ".tk";
	setAttr ".tk[80]" -type "float3" 0.018759685 0.014273673 0 ;
	setAttr ".tk[81]" -type "float3" 0.037519369 -0.019031564 0 ;
	setAttr ".tk[105]" -type "float3" 0.018759685 0.014273673 0 ;
	setAttr ".tk[106]" -type "float3" 0.037519369 -0.019031564 0 ;
	setAttr ".tk[108]" -type "float3" -0.018759685 0.019031564 0 ;
	setAttr ".tk[109]" -type "float3" -0.018759685 0.019031564 0 ;
	setAttr ".tk[112]" -type "float3" 1.275659 0.10467359 0 ;
	setAttr ".tk[113]" -type "float3" 1.275659 0.10467359 0 ;
	setAttr ".tk[114]" -type "float3" 0.075038739 0.014273673 0 ;
	setAttr ".tk[115]" -type "float3" 0.075038739 0.014273673 0 ;
	setAttr ".tk[116]" -type "float3" 0.33767438 0 0 ;
	setAttr ".tk[117]" -type "float3" 0.33767438 0 0 ;
	setAttr ".tk[120]" -type "float3" 0.54403096 0.038063128 0 ;
	setAttr ".tk[121]" -type "float3" 0.54403096 0.038063128 0 ;
	setAttr ".tk[124]" -type "float3" 0.20635653 0.028547345 0 ;
	setAttr ".tk[125]" -type "float3" 0.20635653 0.028547345 0 ;
	setAttr ".tk[128]" -type "float3" -0.91922444 0 0 ;
	setAttr ".tk[129]" -type "float3" -0.91922444 0 0 ;
createNode polyExtrudeFace -n "polyExtrudeFace4";
	rename -uid "D1C44530-4FCC-F9F8-B44C-D49727E5F1CF";
	setAttr ".ics" -type "componentList" 1 "f[126]";
	setAttr ".ix" -type "matrix" 0.77777777958737127 0 0 0 0 3.0666666498374471 0 0 0 0 1 0
		 -18.84567503947536 -9.358297536725777 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 18.127689 -0.52789515 0 ;
	setAttr ".rs" 56957;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 17.691215287366958 -2.6089138063190891 -0.5 ;
	setAttr ".cbx" -type "double3" 18.564162134558565 1.5531234798513527 0.5 ;
createNode polyTweak -n "polyTweak4";
	rename -uid "7EEAF0BF-4C90-5F61-055A-4883E37B9080";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[132:135]" -type "float3"  -0.74751478 0.04945752 0 -0.74751478
		 0.04945752 0 -1.82003558 -0.15661547 0 -1.82003558 -0.15661547 0;
createNode polySplitRing -n "polySplitRing6";
	rename -uid "B51E8DA5-4120-DD04-271D-20A35FA5F685";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[268:269]" "e[271]" "e[273]";
	setAttr ".ix" -type "matrix" 0.77777777958737127 0 0 0 0 3.0666666498374471 0 0 0 0 1 0
		 -18.84567503947536 -9.358297536725777 0 1;
	setAttr ".wt" 0.9814293384552002;
	setAttr ".dr" no;
	setAttr ".re" 271;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak5";
	rename -uid "4A9F6A8B-4D6D-AAFB-2C59-6E9559527BB5";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[136:139]" -type "float3"  -8.088223457 0.2054337 0 -8.088223457
		 0.2054337 0 -9.1407671 -1.067331314 0 -9.1407671 -1.067331314 0;
createNode polySplitRing -n "polySplitRing7";
	rename -uid "E451B42F-4BE9-5AAB-9484-96AB8073C311";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[268:269]" "e[271]" "e[273]";
	setAttr ".ix" -type "matrix" 0.77777777958737127 0 0 0 0 3.0666666498374471 0 0 0 0 1 0
		 -18.84567503947536 -9.358297536725777 0 1;
	setAttr ".wt" 0.8253781795501709;
	setAttr ".dr" no;
	setAttr ".re" 271;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak6";
	rename -uid "9850E5E5-4462-9754-55F6-0D97949DD5E2";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[140:143]" -type "float3"  0.034307308 0.041484248 0
		 -0.034307308 -0.041484248 0 -0.034307308 -0.041484248 0 0.034307308 0.041484248 0;
createNode polySplitRing -n "polySplitRing8";
	rename -uid "9AE105F2-400E-0348-B9CC-F1A5D341E9AA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[268:269]" "e[271]" "e[273]";
	setAttr ".ix" -type "matrix" 0.77777777958737127 0 0 0 0 3.0666666498374471 0 0 0 0 1 0
		 -18.84567503947536 -9.358297536725777 0 1;
	setAttr ".wt" 0.76067745685577393;
	setAttr ".dr" no;
	setAttr ".re" 271;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak7";
	rename -uid "99091CD1-4F69-50AA-E391-4ABB7610F594";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[144:147]" -type "float3"  -0.0087418184 -0.010570792
		 0 0.0087418184 0.010570792 0 0.0087418184 0.010570792 0 -0.0087418184 -0.010570792
		 0;
createNode polySplitRing -n "polySplitRing9";
	rename -uid "CA1D43E0-4458-2F1A-1B45-FC84E65767ED";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[268:269]" "e[271]" "e[273]";
	setAttr ".ix" -type "matrix" 0.77777777958737127 0 0 0 0 3.0666666498374471 0 0 0 0 1 0
		 -18.84567503947536 -9.358297536725777 0 1;
	setAttr ".wt" 0.70428258180618286;
	setAttr ".dr" no;
	setAttr ".re" 271;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak8";
	rename -uid "AB13C9A6-4DC3-3774-337C-57865AEB0917";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[148:151]" -type "float3"  -0.02697574 -0.041829482 0
		 0.02697574 0.023410171 0 0.02697574 0.023410171 0 -0.02697574 -0.041829482 0;
createNode polySplitRing -n "polySplitRing10";
	rename -uid "E211E7BF-4D6C-C211-5AEB-7F811DFD1B64";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[268:269]" "e[271]" "e[273]";
	setAttr ".ix" -type "matrix" 0.77777777958737127 0 0 0 0 3.0666666498374471 0 0 0 0 1 0
		 -18.84567503947536 -9.358297536725777 0 1;
	setAttr ".wt" 0.48784619569778442;
	setAttr ".dr" no;
	setAttr ".re" 271;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak9";
	rename -uid "793533CF-4A7A-E033-9CBB-9D9905153968";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[152:155]" -type "float3"  -0.023161769 -0.035814971
		 0 0.023161769 0.020200629 0 0.023161769 0.020200629 0 -0.023161769 -0.035814971 0;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "0E4C2945-4A6C-E219-0F27-A2B713035C36";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n"
		+ "            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 672\n            -height 332\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n"
		+ "            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 671\n            -height 332\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 672\n            -height 332\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 671\n            -height 332\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n"
		+ "            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n"
		+ "            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n"
		+ "            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n"
		+ "            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n"
		+ "                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n"
		+ "                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n"
		+ "                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n"
		+ "                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n"
		+ "                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n"
		+ "                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"quad\\\" -ps 1 50 50 -ps 2 50 50 -ps 3 50 50 -ps 4 50 50 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 672\\n    -height 332\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 672\\n    -height 332\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 671\\n    -height 332\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 671\\n    -height 332\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Side View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 671\\n    -height 332\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 671\\n    -height 332\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Front View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"wireframe\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 672\\n    -height 332\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"wireframe\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 672\\n    -height 332\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "C6502A6C-48F3-BF9C-613B-43B104124061";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyCube -n "polyCube2";
	rename -uid "4AE10486-4C61-753D-0C18-FDA41A531C4E";
	setAttr ".cuv" 4;
createNode polyExtrudeFace -n "polyExtrudeFace5";
	rename -uid "AC56F3BF-4D6D-FABB-C4D4-078706625E17";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 5.820970736298074 -3.6101179249830184 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 6.018661 -3.6101179 0 ;
	setAttr ".rs" 35615;
	setAttr ".lt" -type "double3" -1.6588467002373398e-015 2.6728430843575537e-017 7.4707813810536603 ;
	setAttr ".d" 7;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 6.018661223206033 -4.3156734831876697 -0.5 ;
	setAttr ".cbx" -type "double3" 6.018661223206033 -2.9045623667783675 0.5 ;
createNode polyTweak -n "polyTweak10";
	rename -uid "5556541F-4B7E-DD3F-3620-3F9341A964C2";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  0.18888889 -0.20555554 0 -0.30230951
		 -0.20555554 0 0.18888889 0.20555554 0 -0.30230951 0.20555554 0 0.18888889 0.20555554
		 0 -0.30230951 0.20555554 0 0.18888889 -0.20555554 0 -0.30230951 -0.20555554 0;
createNode polySplitRing -n "polySplitRing11";
	rename -uid "320B075F-4F84-0692-51C5-60AFBBCBB420";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[18]" "e[25]" "e[39]" "e[53]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 5.820970736298074 -3.6101179249830184 0 1;
	setAttr ".wt" 0.69356334209442139;
	setAttr ".dr" no;
	setAttr ".re" 53;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak11";
	rename -uid "0CE5E80E-4BB4-DC1A-53E0-059A45285E4C";
	setAttr ".uopa" yes;
	setAttr -s 36 ".tk[0:35]" -type "float3"  -4.052314e-015 0.60473102
		 0 3.6082248e-015 0.23589654 0 -4.052314e-015 -0.60473102 0 3.6082248e-015 -0.41922858
		 0 -4.052314e-015 -0.60473102 0 3.6082248e-015 -0.41922858 0 -4.052314e-015 0.60473102
		 0 3.6082248e-015 0.23589654 0 0 -0.10317947 0 0 -0.38273865 0 -4.4408921e-016 -0.62906355
		 0 8.8817842e-016 -0.67261583 0 0.068749517 -0.70128918 0 -8.8817842e-016 -0.66349328
		 0 -1.7763568e-015 -0.11599843 0 0 -0.10317947 0 0 -0.38273865 0 -4.4408921e-016 -0.62906355
		 0 8.8817842e-016 -0.67261583 0 0.068749517 -0.70128918 0 -8.8817842e-016 -0.66349328
		 0 -1.7763568e-015 -0.11599843 0 0 -0.35515076 0 0 -0.53392148 0 -4.4408921e-016 -0.65426069
		 0 8.8817842e-016 -0.74820727 0 0.068749517 -0.90286624 0 -8.8817842e-016 -0.94066191
		 0 -1.7763568e-015 -1.35065734 0 0 -0.35515076 0 0 -0.53392148 0 -4.4408921e-016 -0.65426069
		 0 8.8817842e-016 -0.74820727 0 0.068749517 -0.90286624 0 -8.8817842e-016 -0.94066191
		 0 -1.7763568e-015 -1.35065734 0;
createNode polySplitRing -n "polySplitRing12";
	rename -uid "A1285194-487C-9F06-A8E1-21B82221106B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[18]" "e[25]" "e[39]" "e[53]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 5.820970736298074 -3.6101179249830184 0 1;
	setAttr ".wt" 0.46699309349060059;
	setAttr ".re" 53;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak12";
	rename -uid "75BB6F67-4FFD-AD1D-5623-7088863CCD3A";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[36:39]" -type "float3"  5.3290705e-015 0.022447184
		 0 5.3290705e-015 0.022447184 0 5.3290705e-015 -0.1748662 0 5.3290705e-015 -0.1748662
		 0;
createNode polySplitRing -n "polySplitRing13";
	rename -uid "5A84B605-45B1-BE88-0A9E-F88BD9045345";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[68:69]" "e[71]" "e[73]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 5.820970736298074 -3.6101179249830184 0 1;
	setAttr ".wt" 0.55962181091308594;
	setAttr ".re" 68;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak13";
	rename -uid "8A578C1A-406A-21FC-0404-CE8DAC13AAC9";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[40:43]" -type "float3"  0 0.032711372 0 0 0.032711372
		 0 0 -0.032711383 0 0 -0.032711383 0;
createNode polySplitRing -n "polySplitRing14";
	rename -uid "10CDD953-4757-7DD6-5857-A08A9BC54B44";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[0:3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 5.820970736298074 -3.6101179249830184 0 1;
	setAttr ".wt" 0.33091679215431213;
	setAttr ".re" 1;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak14";
	rename -uid "B94917F3-49A0-E7FC-3599-82A5B87F6A80";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk";
	setAttr ".tk[8]" -type "float3" 0 -0.042851642 0 ;
	setAttr ".tk[9]" -type "float3" 0 -0.068562627 0 ;
	setAttr ".tk[10]" -type "float3" 0 0.025710985 0 ;
	setAttr ".tk[13]" -type "float3" 0 0.017140657 0 ;
	setAttr ".tk[14]" -type "float3" 0 -0.05142197 0 ;
	setAttr ".tk[15]" -type "float3" 0 -0.042851642 0 ;
	setAttr ".tk[16]" -type "float3" 0 -0.068562627 0 ;
	setAttr ".tk[17]" -type "float3" 0 0.025710985 0 ;
	setAttr ".tk[20]" -type "float3" 0 0.017140657 0 ;
	setAttr ".tk[21]" -type "float3" 0 -0.05142197 0 ;
	setAttr ".tk[25]" -type "float3" 0 -0.025710985 0 ;
	setAttr ".tk[28]" -type "float3" 0 -0.05142197 0 ;
	setAttr ".tk[32]" -type "float3" 0 -0.025710985 0 ;
	setAttr ".tk[35]" -type "float3" 0 -0.05142197 0 ;
	setAttr ".tk[42]" -type "float3" 0 0.025710985 0 ;
	setAttr ".tk[43]" -type "float3" 0 0.025710985 0 ;
	setAttr ".tk[44]" -type "float3" 8.8817842e-016 -0.0030569043 0 ;
	setAttr ".tk[45]" -type "float3" 8.8817842e-016 -0.0030569043 0 ;
	setAttr ".tk[46]" -type "float3" 8.8817842e-016 -0.073152594 0 ;
	setAttr ".tk[47]" -type "float3" 8.8817842e-016 -0.073152594 0 ;
createNode polySplitRing -n "polySplitRing15";
	rename -uid "F8A4179A-4E55-6FE7-EEF5-18820131B179";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[12]" "e[19]" "e[33]" "e[47]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 1.5095241642941275 -0.57424726185597841 0 1;
	setAttr ".wt" 0.45252552628517151;
	setAttr ".re" 47;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing16";
	rename -uid "DC96E01A-43EC-DD41-A547-E8AE1BAE679F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[12]" "e[19]" "e[33]" "e[47]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 1.5095241642941275 -0.57424726185597841 0 1;
	setAttr ".wt" 0.46934860944747925;
	setAttr ".dr" no;
	setAttr ".re" 47;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak15";
	rename -uid "392EE15A-4492-34BC-CCB4-A9872600CBF6";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[52:55]" -type "float3"  8.3266727e-017 -0.051187359
		 0 8.3266727e-017 -0.051187359 0 8.3266727e-017 -0.11849117 0 8.3266727e-017 -0.11849117
		 0;
createNode polyCube -n "polyCube3";
	rename -uid "579CBBB7-418C-8A10-3012-12A7FD83BBDA";
	setAttr ".cuv" 4;
createNode polySplitRing -n "polySplitRing17";
	rename -uid "FEC55599-45B8-FDAE-0F5B-45AC671A69A5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[0:3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".wt" 0.32011321187019348;
	setAttr ".re" 1;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak16";
	rename -uid "4ADC4232-4F57-E227-EEA8-FEBADD3940E6";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  0.047796167 0.30800736 0 -0.31545475
		 0.43227738 0 -0.0573554 -0.43227738 0 -0.61179096 0.017006613 0 -0.0573554 -0.43227738
		 0 -0.61179096 0.017006613 0 0.047796167 0.30800736 0 -0.31545475 0.43227738 0;
createNode polyExtrudeFace -n "polyExtrudeFace6";
	rename -uid "2AEE4184-418E-1FBF-B624-ACB2A36FC068";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 3.076005 0.79717815 0 ;
	setAttr ".rs" 33030;
	setAttr ".lt" -type "double3" -7.7715611723760958e-016 -2.6711770288707735e-018 
		0.81946629665466597 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 2.9278369145803271 0.50481350191640162 -0.5 ;
	setAttr ".cbx" -type "double3" 3.22417311839388 1.0895427561621598 0.5 ;
createNode polyTweak -n "polyTweak17";
	rename -uid "E26BABFC-442A-82D1-2442-5283AEC1F7BA";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk";
	setAttr ".tk[0]" -type "float3" -0.0093880529 0.02318768 0 ;
	setAttr ".tk[2]" -type "float3" 0.0093880501 -0.02318768 0 ;
	setAttr ".tk[4]" -type "float3" 0.0093880501 -0.02318768 0 ;
	setAttr ".tk[6]" -type "float3" -0.0093880529 0.02318768 0 ;
	setAttr ".tk[8]" -type "float3" -0.0089112734 0.019485965 0 ;
	setAttr ".tk[9]" -type "float3" -0.0089112734 0.019485965 0 ;
	setAttr ".tk[10]" -type "float3" 0.0089112753 -0.019485965 0 ;
	setAttr ".tk[11]" -type "float3" 0.0089112753 -0.019485965 0 ;
createNode polyExtrudeFace -n "polyExtrudeFace7";
	rename -uid "33126CCE-478A-A16F-6999-19816F28CFD8";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 3.8069615 1.167621 0 ;
	setAttr ".rs" 58528;
	setAttr ".lt" -type "double3" 3.8857805861880479e-016 -6.7314866764527021e-017 0.91586957624013232 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 3.5423817234449206 0.64555306204365992 -0.5 ;
	setAttr ".cbx" -type "double3" 4.0715415077619372 1.6896890498022965 0.5 ;
createNode polyTweak -n "polyTweak18";
	rename -uid "19478674-4D67-481A-369F-B0B5CCE8A709";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[12:15]" -type "float3"  0.11641183 -0.22970337 0 0.11641183
		 -0.22970337 0 -0.11641183 0.2297034 0 -0.11641183 0.2297034 0;
createNode polyExtrudeFace -n "polyExtrudeFace8";
	rename -uid "72830953-44AE-2E02-1D5F-90957A468F27";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 4.6239085 1.5816432 0 ;
	setAttr ".rs" 58796;
	setAttr ".lt" -type "double3" 7.2164496600635175e-016 1.5103370817829912e-016 1.0167449741104599 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 4.1798016624860583 0.70533288487004542 -0.5 ;
	setAttr ".cbx" -type "double3" 5.0680154161863147 2.4579534626822404 0.5 ;
createNode polyTweak -n "polyTweak19";
	rename -uid "A7E347D0-4D56-6AA8-C253-B683C7D5A6A6";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[16:19]" -type "float3"  0.17952712 -0.35424232 0 0.17952712
		 -0.35424232 0 -0.17952712 0.35424232 0 -0.17952712 0.35424232 0;
createNode polyExtrudeFace -n "polyExtrudeFace9";
	rename -uid "2FC5B05A-4078-705D-6A8F-908E83E92513";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 5.5308356 2.0412667 0 ;
	setAttr ".rs" 46042;
	setAttr ".lt" -type "double3" 0.78966372530986317 1.5906399044887037e-016 1.2060110584518713 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 4.8726166086606799 0.74247087009953761 -0.5 ;
	setAttr ".cbx" -type "double3" 6.1890543299131213 3.3400622702460221 0.5 ;
createNode polyTweak -n "polyTweak20";
	rename -uid "90DF0B8F-4EE7-BECE-3AD9-DF81FD76A862";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[20:23]" -type "float3"  0.214112 -0.42248523 0 0.214112
		 -0.42248523 0 -0.214112 0.42248523 0 -0.214112 0.42248523 0;
createNode polyExtrudeFace -n "polyExtrudeFace10";
	rename -uid "32FA3C6F-4178-2274-B0AD-38AA153E2E33";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 6.940815 1.7683673 0 ;
	setAttr ".rs" 63401;
	setAttr ".lt" -type "double3" 0.70742017984802741 1.9062430601412274e-016 1.0437287563769049 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 6.4284432726316272 0.75735637017297053 -0.38921090960502625 ;
	setAttr ".cbx" -type "double3" 7.4531868296079455 2.7793783045630387 0.38921090960502625 ;
createNode polyTweak -n "polyTweak21";
	rename -uid "936B294E-45C3-7D72-C3FC-12BCA38F8309";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[24:27]" -type "float3"  -0.1685885 0.17407675 0.11078908
		 -0.1685885 0.17407675 -0.11078908 0.1231053 -0.40149277 0.11078908 0.1231053 -0.40149277
		 -0.11078908;
createNode polyExtrudeFace -n "polyExtrudeFace11";
	rename -uid "1D64C734-4B3F-1DBE-69A4-58B6AEF2C43B";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 8.1916027 1.4499851 0 ;
	setAttr ".rs" 54104;
	setAttr ".lt" -type "double3" 0.025944965429004388 2.6258182550289724e-016 1.1404179657122469 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 8.1190632181577502 0.74529213675069117 -0.38921090960502625 ;
	setAttr ".cbx" -type "double3" 8.2641418772153674 2.1546782351355485 0.38921090960502625 ;
createNode polyTweak -n "polyTweak22";
	rename -uid "46E8C3C0-4D23-66F0-CF28-448D714DA138";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[28:31]" -type "float3"  -0.43983278 0.14712667 0 -0.43983278
		 0.14712667 0 0.43983278 -0.46550918 0 0.43983278 -0.46550918 0;
createNode polyExtrudeFace -n "polyExtrudeFace12";
	rename -uid "7654AC0C-415E-94FC-0E40-35AA074F18C0";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 9.3286829 1.5409516 0 ;
	setAttr ".rs" 38023;
	setAttr ".lt" -type "double3" -0.13473816071132083 1.8557465552368213e-016 1.1340965519442205 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 9.2561434107236682 0.83625859983968043 -0.38921090960502625 ;
	setAttr ".cbx" -type "double3" 9.4012220697812854 2.2456445790152482 0.38921090960502625 ;
createNode polyExtrudeFace -n "polyExtrudeFace13";
	rename -uid "D9C9BC31-41EF-28F8-5876-069C1E5CA0EE";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 10.443021 1.7683678 0 ;
	setAttr ".rs" 50684;
	setAttr ".lt" -type "double3" -0.040289922371750053 2.82780427464658e-016 1.3121060678276311 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 10.273549397509557 0.98963050015973353 -0.38921090960502625 ;
	setAttr ".cbx" -type "double3" 10.612491925280553 2.547104964337819 0.38921090960502625 ;
createNode polyTweak -n "polyTweak23";
	rename -uid "23A425D4-4014-F2FC-7B61-DB9721B74B3F";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[36:39]" -type "float3"  0.096931778 -0.096785754 0
		 0.096931778 -0.096785754 0 -0.096931778 0.051302541 0 -0.096931778 0.051302541 0;
createNode polyExtrudeFace -n "polyExtrudeFace14";
	rename -uid "A207DC9F-42F2-CDE8-3CBF-32B9D39C39E2";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 11.716549 2.08675 0 ;
	setAttr ".rs" 51522;
	setAttr ".lt" -type "double3" -0.070536176355801616 1.1361713603490734e-016 1.0764204094128806 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 11.496465523760778 1.2469928956847123 -0.38921090960502625 ;
	setAttr ".cbx" -type "double3" 11.936632950823766 2.9265073634009293 0.38921090960502625 ;
createNode polyTweak -n "polyTweak24";
	rename -uid "BF7E1E0E-4D64-E38D-1D35-0381A5858D5C";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[40:43]" -type "float3"  0.050612681 -0.061020091 0
		 0.050612681 -0.061020091 0 -0.050612681 0.061020087 0 -0.050612681 0.061020087 0;
createNode polyExtrudeFace -n "polyExtrudeFace15";
	rename -uid "8032DB1F-42EA-E2C4-7185-059F94165F4F";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 12.739922 2.4278743 0 ;
	setAttr ".rs" 37627;
	setAttr ".lt" -type "double3" -0.085256371888979859 -2.1461014584371348e-017 0.75114303892746137 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 12.508048852007848 1.5431321955542496 -0.38921090960502625 ;
	setAttr ".cbx" -type "double3" 12.971794922869664 3.3126164770941666 0.38921090960502625 ;
createNode polyTweak -n "polyTweak25";
	rename -uid "C9BECED5-43B6-79E8-B5B4-6E931D58CDE2";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[44:47]" -type "float3"  0.011789609 -0.044984706 0
		 0.011789609 -0.044984706 0 -0.011789609 0.044984706 0 -0.011789609 0.044984706 0;
createNode polyExtrudeFace -n "polyExtrudeFace16";
	rename -uid "BFB15075-44C6-6378-18A3-D988B46F57B1";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 13.444912 2.6800663 0 ;
	setAttr ".rs" 61949;
	setAttr ".lt" -type "double3" 5.5511151231257827e-017 -3.7188883728341781e-019 0.14332668163537987 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 13.233445008318883 1.873186597905629 -0.38921090960502625 ;
	setAttr ".cbx" -type "double3" 13.65637954048441 3.4869459963660172 0.38921090960502625 ;
createNode polyTweak -n "polyTweak26";
	rename -uid "744A8ED9-487D-4217-290C-10987CBDB1D1";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[48:51]" -type "float3"  -0.020406179 0.057154797 0
		 -0.020406179 0.057154797 0 0.020406183 -0.098570041 0 0.020406183 -0.098570041 0;
createNode polyExtrudeFace -n "polyExtrudeFace17";
	rename -uid "1426E3A4-4AA7-0349-17D3-00B38484F3F6";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 13.583557 2.7164023 0 ;
	setAttr ".rs" 47054;
	setAttr ".lt" -type "double3" 2.5326962749261384e-016 -5.3636995402693413e-018 0.087999129825276884 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 13.402297814410192 2.0247851706366471 -0.38921090960502625 ;
	setAttr ".cbx" -type "double3" 13.764817078631383 3.4080194331030778 0.38921090960502625 ;
createNode polyTweak -n "polyTweak27";
	rename -uid "26B0E280-41EC-5993-CBC4-7DAE232C6A13";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[52:55]" -type "float3"  -0.030208079 0.11526266 0
		 -0.030208079 0.11526266 0 0.030208079 -0.11526267 0 0.030208079 -0.11526267 0;
createNode polyExtrudeFace -n "polyExtrudeFace18";
	rename -uid "13EDFDB4-4606-4891-3946-0E897AE815E4";
	setAttr ".ics" -type "componentList" 1 "f[24]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 5.6505299 3.0597205 0 ;
	setAttr ".rs" 47442;
	setAttr ".lt" -type "double3" 2.2204460492503131e-016 -9.1304735885516393e-017 0.86665608528655802 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 4.872616847079259 2.7793785429816178 -0.5 ;
	setAttr ".cbx" -type "double3" 6.4284430342130481 3.3400622702460221 0.5 ;
createNode polyTweak -n "polyTweak28";
	rename -uid "61CC70C6-41F6-88D5-3C1B-DDB7444C715F";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[56:59]" -type "float3"  -0.074442081 0.28404263 0
		 -0.074442081 0.28404263 0 0.074442081 -0.28404263 0 0.074442081 -0.28404263 0;
createNode polyExtrudeFace -n "polyExtrudeFace19";
	rename -uid "5C7291AE-481B-790A-900C-AB85AC5F95DC";
	setAttr ".ics" -type "componentList" 1 "f[24]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 5.8097558 3.8957558 0 ;
	setAttr ".rs" 60655;
	setAttr ".lt" -type "double3" 1.7208456881689926e-015 1.841805711142799e-017 0.97863604176253483 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 5.2957638101987659 3.7105249262671403 -0.5 ;
	setAttr ".cbx" -type "double3" 6.3237474756650744 4.0809863901953625 0.5 ;
createNode polyTweak -n "polyTweak29";
	rename -uid "769059D0-4688-FA4E-4B60-529FC5D4420C";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[60:63]" -type "float3"  0.12932169 -0.074403517 0
		 0.12932169 -0.074403517 0 -0.39852077 0.11581876 0 -0.39852077 0.11581876 0;
createNode polyExtrudeFace -n "polyExtrudeFace20";
	rename -uid "3ECF1AC3-41E4-B911-1248-D8B859239136";
	setAttr ".ics" -type "componentList" 1 "f[24]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 6.4625139 4.9199691 0 ;
	setAttr ".rs" 34162;
	setAttr ".lt" -type "double3" 2.2204460492503131e-016 -2.8351044551429448e-017 1.2052321393443119 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 6.0861676531247912 4.6754742956976818 -0.5 ;
	setAttr ".cbx" -type "double3" 6.8388605909757434 5.1644641257147716 0.5 ;
createNode polyTweak -n "polyTweak30";
	rename -uid "2A575132-409F-807E-FFB6-90822E175359";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[64:67]" -type "float3"  0.45861351 0.16280217 0 0.45861351
		 0.16280217 0 0.18332297 0.044273987 0 0.18332297 0.044273987 0;
createNode polyExtrudeFace -n "polyExtrudeFace21";
	rename -uid "1B236E96-48A1-17D7-393A-60A0337EAA45";
	setAttr ".ics" -type "componentList" 1 "f[24]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 7.253705 5.8995872 0 ;
	setAttr ".rs" 55749;
	setAttr ".lt" -type "double3" 1.1102230246251565e-015 -1.0495956136492467e-016 1.1937728432640273 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 6.8564225035123645 5.6147157049994396 -0.5 ;
	setAttr ".cbx" -type "double3" 7.6509874658994494 6.1844583845953869 0.5 ;
createNode polyTweak -n "polyTweak31";
	rename -uid "7ACB92D9-40BB-35CD-0572-8CBC9EE1338D";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[68:71]" -type "float3"  0.11366366 0.0093149925 0
		 0.11366366 0.0093149925 0 0.15553546 -0.071437865 0 0.15553546 -0.071437865 0;
createNode polyExtrudeFace -n "polyExtrudeFace22";
	rename -uid "9D2A4D7F-464B-CB22-9B19-13A0F90C1C43";
	setAttr ".ics" -type "componentList" 1 "f[24]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 8.1046534 6.7247772 0 ;
	setAttr ".rs" 48022;
	setAttr ".lt" -type "double3" 0.0086882528670931825 -1.4782392180277681e-016 0.80093393925906275 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 7.7312024431638537 6.1658912993292736 -0.5 ;
	setAttr ".cbx" -type "double3" 8.4781034784726916 7.2836629248480724 0.5 ;
createNode polyTweak -n "polyTweak32";
	rename -uid "D6FABC59-422C-56CF-FFB5-87A2642FCCED";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[72:75]" -type "float3"  0.17913939 0.129061 0 0.17913939
		 0.129061 0 0.13147548 -0.41896769 0 0.13147548 -0.41896769 0;
createNode polyExtrudeFace -n "polyExtrudeFace23";
	rename -uid "156D4335-4E31-7A35-7311-9DB0D7AA1374";
	setAttr ".ics" -type "componentList" 1 "f[24]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 8.6718855 7.1314788 0 ;
	setAttr ".rs" 54355;
	setAttr ".lt" -type "double3" 1.3877787807814457e-016 2.9770828287387929e-017 0.15406426298254558 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 8.5582216577939807 6.2384611464362072 -0.5 ;
	setAttr ".cbx" -type "double3" 8.7855490045957385 8.0244966841559346 0.5 ;
createNode polyTweak -n "polyTweak33";
	rename -uid "53BB2711-483E-FB7D-F902-8D882E9AF91F";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[76:79]" -type "float3"  0.15624903 0.30307055 0 0.15624903
		 0.30307055 0 -0.36332527 -0.36519337 0 -0.36332527 -0.36519337 0;
createNode polyExtrudeFace -n "polyExtrudeFace24";
	rename -uid "14B5BC0B-47B8-296A-F53C-42B6A4C06A1C";
	setAttr ".ics" -type "componentList" 1 "f[24]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 8.8040094 7.1509318 0 ;
	setAttr ".rs" 48774;
	setAttr ".lt" -type "double3" 0.23642335471656062 5.0007185220190726e-017 0.80206680449552614 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 8.6903460817746936 6.2579132414679455 -0.5 ;
	setAttr ".cbx" -type "double3" 8.9176734285764514 8.0439502096991475 0.5 ;
createNode polyTweak -n "polyTweak34";
	rename -uid "39F3EFAF-423F-2AAC-C423-0B988AB83867";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[80:83]" -type "float3"  -0.020707626 0 0 -0.020707626
		 0 0 -0.020707626 0 0 -0.020707626 0 0;
createNode polyExtrudeFace -n "polyExtrudeFace25";
	rename -uid "1244AB21-47B8-9F9B-DF1E-D99EB34574D9";
	setAttr ".ics" -type "componentList" 1 "f[24]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 9.629508 7.0176711 0 ;
	setAttr ".rs" 45430;
	setAttr ".lt" -type "double3" -3.3306690738754696e-016 -4.8608417383610283e-017 
		0.94343116839714158 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 9.4546569185666858 6.2331444121222424 -0.5 ;
	setAttr ".cbx" -type "double3" 9.8043587999753772 7.8021975851874279 0.5 ;
createNode polyTweak -n "polyTweak35";
	rename -uid "49C58BB4-432B-0912-1116-6B96F554C522";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[84:87]" -type "float3"  0.28851461 -0.10849179 0 0.28851461
		 -0.10849179 0 -0.28851461 0.10849179 0 -0.28851461 0.10849179 0;
createNode polyExtrudeFace -n "polyExtrudeFace26";
	rename -uid "5BBBD1E4-4C63-B91E-C95E-F4B6D3CD6DC0";
	setAttr ".ics" -type "componentList" 1 "f[24]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 10.539991 6.7089014 0 ;
	setAttr ".rs" 33408;
	setAttr ".lt" -type "double3" 0.04615826499016723 -4.4830661762772269e-017 0.84370078572846385 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 10.221666653674108 5.8453132010321545 -0.5 ;
	setAttr ".cbx" -type "double3" 10.858315308611852 7.5724893904547619 0.5 ;
createNode polyTweak -n "polyTweak36";
	rename -uid "71C39192-46C3-4E26-6DE3-26BA74C17965";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[88:91]" -type "float3"  0.1331197 -0.024476521 0 0.1331197
		 -0.024476521 0 -0.15382728 -0.18259975 0 -0.15382728 -0.18259975 0;
createNode polyExtrudeFace -n "polyExtrudeFace27";
	rename -uid "6E86A9D8-4244-DE7D-1EE8-EC9570BAE06D";
	setAttr ".ics" -type "componentList" 1 "f[24]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 11.347589 6.4604096 0 ;
	setAttr ".rs" 58313;
	setAttr ".lt" -type "double3" 0.018967183845454788 -6.4946984348631758e-017 0.29094843039746043 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 10.989475567858678 5.4888788557867931 -0.5 ;
	setAttr ".cbx" -type "double3" 11.705701668780309 7.4319406844000744 0.5 ;
createNode polyTweak -n "polyTweak37";
	rename -uid "F9F310D4-409E-80A6-D808-578125D246A9";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[92:95]" -type "float3"  0.039788544 0.10794297 0 0.039788544
		 0.10794297 0 -0.039788526 -0.10794297 0 -0.039788526 -0.10794297 0;
createNode polyExtrudeFace -n "polyExtrudeFace28";
	rename -uid "9B28A53B-421E-80EB-3BBB-60AA899B80CF";
	setAttr ".ics" -type "componentList" 1 "f[24]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 11.627141 6.3775792 0 ;
	setAttr ".rs" 44112;
	setAttr ".lt" -type "double3" 0.23502565112149479 -1.5690731616970334e-016 0.87179694133577368 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 11.289478142779332 5.3987528181891369 -0.5 ;
	setAttr ".cbx" -type "double3" 11.96480353645609 7.3564058638434338 0.5 ;
createNode polyTweak -n "polyTweak38";
	rename -uid "C2F3F3BB-4F98-AD99-7032-2380B94E3E39";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[96:99]" -type "float3"  -0.020450694 0.0072956448
		 0 -0.020450694 0.0072956448 0 0.020450694 -0.0072956448 0 0.020450694 -0.0072956448
		 0;
createNode polyExtrudeFace -n "polyExtrudeFace29";
	rename -uid "3963FA9B-4A23-FC38-7735-63806C3AA0B0";
	setAttr ".ics" -type "componentList" 1 "f[24]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 12.558984 6.2947483 0 ;
	setAttr ".rs" 44477;
	setAttr ".lt" -type "double3" 0.040561734116876802 -9.4834092190479905e-017 0.57261852685765935 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 12.327010949175817 5.1343661642889904 -0.5 ;
	setAttr ".cbx" -type "double3" 12.790956337969762 7.455130705914967 0.5 ;
createNode polyTweak -n "polyTweak39";
	rename -uid "402E7AE2-4AD7-21DC-EF04-D68043975C03";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[100:103]" -type "float3"  -0.074628375 0.16084823 0
		 -0.074628375 0.16084823 0 0.13675125 -0.2022635 0 0.13675125 -0.2022635 0;
createNode polyExtrudeFace -n "polyExtrudeFace30";
	rename -uid "7CB83794-49C9-23CB-532A-718310190130";
	setAttr ".ics" -type "componentList" 1 "f[24]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 13.128444 6.2222719 0 ;
	setAttr ".rs" 62922;
	setAttr ".lt" -type "double3" -1.4771170397942512e-015 3.9591087205711071e-020 0.069014568862230771 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 13.008303483050328 5.0450445509772228 -0.5 ;
	setAttr ".cbx" -type "double3" 13.248583634417516 7.399499545178883 0.5 ;
createNode polyTweak -n "polyTweak40";
	rename -uid "53058F2D-4814-63A9-3809-F7A19D0B51A2";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[104:107]" -type "float3"  -0.11183295 0.016845321 0
		 -0.11183295 0.016845321 0 0.11183295 -0.016845321 0 0.11183295 -0.016845321 0;
createNode polyExtrudeFace -n "polyExtrudeFace31";
	rename -uid "EA89928E-45E1-B442-7993-618461533F07";
	setAttr ".ics" -type "componentList" 1 "f[24]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.039627869647008 0.57253612049626612 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 13.197103 6.2152653 0 ;
	setAttr ".rs" 52820;
	setAttr ".lt" -type "double3" -5.5337678883660146e-016 3.4481435391336413e-018 0.061168045571795691 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 13.081252892535192 5.0800796843390392 -0.5 ;
	setAttr ".cbx" -type "double3" 13.312951882403356 7.3504511214117931 0.5 ;
createNode polyTweak -n "polyTweak41";
	rename -uid "A8A77470-48ED-4649-1F23-3DBFBF28D3E9";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[108:111]" -type "float3"  -0.004290523 -0.042041704
		 0 -0.004290523 -0.042041704 0 0.004290523 0.042041704 0 0.004290523 0.042041704 0;
createNode polyCube -n "polyCube4";
	rename -uid "6254BD09-4A4C-14B4-33D8-C095D5A3A55C";
	setAttr ".cuv" 4;
createNode polySplitRing -n "polySplitRing18";
	rename -uid "03FFCBF1-470C-CF55-CD7D-7A84AF567D7E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[0:3]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".wt" 0.036838836967945099;
	setAttr ".re" 0;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak42";
	rename -uid "83735FA5-4908-686C-F3B2-A585D632275B";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[0]" -type "float3" -2.5868196e-014 0.45533398 0 ;
	setAttr ".tk[2]" -type "float3" -2.5979219e-014 -0.45533398 0 ;
	setAttr ".tk[4]" -type "float3" -2.5979219e-014 -0.45533398 0 ;
	setAttr ".tk[6]" -type "float3" -2.5868196e-014 0.45533398 0 ;
createNode polyExtrudeFace -n "polyExtrudeFace32";
	rename -uid "26CBE7DB-4317-A383-A768-E1BCA4A08539";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -4.4180198 -1.5609159 0 ;
	setAttr ".rs" 62372;
	setAttr ".lt" -type "double3" 5.5511151231257827e-017 1.0577704464392022e-016 0.94946097178353872 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -4.7033908158506321 -1.8177603177181898 -0.5 ;
	setAttr ".cbx" -type "double3" -4.1326485064461584 -1.3040716856057659 0.5 ;
createNode polyTweak -n "polyTweak43";
	rename -uid "2FD7C84A-4D54-DF7C-B320-99972EA80BB6";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk";
	setAttr ".tk[1]" -type "float3" -0.073975839 0.10165308 0 ;
	setAttr ".tk[3]" -type "float3" -0.073975839 -0.13047789 0 ;
	setAttr ".tk[5]" -type "float3" -0.073975839 -0.13047789 0 ;
	setAttr ".tk[7]" -type "float3" -0.073975839 0.10165308 0 ;
	setAttr ".tk[8]" -type "float3" -5.6621374e-015 -0.017553387 0 ;
	setAttr ".tk[9]" -type "float3" -5.6621374e-015 0.017553387 0 ;
	setAttr ".tk[10]" -type "float3" -5.6621374e-015 0.017553387 0 ;
	setAttr ".tk[11]" -type "float3" -5.6621374e-015 -0.017553387 0 ;
createNode polyExtrudeFace -n "polyExtrudeFace33";
	rename -uid "2116C3C7-4589-7916-9BFF-0EBEA51DFB35";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -3.8490348 -0.85519981 0 ;
	setAttr ".rs" 62038;
	setAttr ".lt" -type "double3" 1.1102230246251565e-015 1.5685699319622689e-016 1.4079579641053765 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -4.4095716946291308 -1.3597029407965553 -0.5 ;
	setAttr ".cbx" -type "double3" -3.2884980216256925 -0.35069674398608153 0.5 ;
createNode polyTweak -n "polyTweak44";
	rename -uid "EB189008-42ED-1BE7-D42E-918E100C225E";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[12:15]" -type "float3"  -0.044276312 -0.32100978 0
		 -0.044276312 -0.32100978 0 -0.044276517 0.41939798 0 -0.044276517 0.41939798 0;
createNode polyExtrudeFace -n "polyExtrudeFace34";
	rename -uid "9373AF06-4449-4A54-301D-FEB3B8650517";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -2.920377 0.32367814 0 ;
	setAttr ".rs" 64216;
	setAttr ".lt" -type "double3" -1.1102230246251565e-016 2.9902665491691634e-016 1.4955272488042444 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -3.9813675063235321 -0.63125150921492423 -0.5 ;
	setAttr ".cbx" -type "double3" -1.8593865631971189 1.2786077566604397 0.5 ;
createNode polyTweak -n "polyTweak45";
	rename -uid "2A1036D2-4C86-4298-2C32-429F0125645A";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[16:19]" -type "float3"  0.0895328 -0.57491201 0 0.0895328
		 -0.57491201 0 0.0895328 0.77169526 0 0.0895328 0.77169526 0;
createNode polyExtrudeFace -n "polyExtrudeFace35";
	rename -uid "673CB913-496F-EAA8-2202-61BD1A5629C3";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -2.0390329 1.4485121 0 ;
	setAttr ".rs" 51818;
	setAttr ".lt" -type "double3" -0.18599635437343343 1.9839630227458123e-016 1.9874712207471104 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -3.3652573326962072 0.25486223208486747 -0.5 ;
	setAttr ".cbx" -type "double3" -0.71280833367269469 2.6421618515542131 0.5 ;
createNode polyTweak -n "polyTweak46";
	rename -uid "2E6D8813-4E0E-25CF-58F7-5CB6FCEF7E32";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[20:23]" -type "float3"  -0.06985873 -0.25943771 0
		 -0.06985873 -0.25943771 0 -0.06985873 0.45424697 0 -0.06985873 0.45424697 0;
createNode polyExtrudeFace -n "polyExtrudeFace36";
	rename -uid "5963E9F7-4926-9933-E37D-278702A1D91D";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.6022117 2.0971255 0 ;
	setAttr ".rs" 52845;
	setAttr ".lt" -type "double3" 0.016093340291261193 2.0353991011132976e-016 0.95090337000486391 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -3.269738470701713 0.97969931423414147 -0.5 ;
	setAttr ".cbx" -type "double3" 0.065315086332852879 3.2145516144644275 0.5 ;
createNode polyTweak -n "polyTweak47";
	rename -uid "0ED33A56-4A7D-C128-EF4A-5CB143267518";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[24:27]" -type "float3"  -0.9281652 -0.27945954 0 -0.9281652
		 -0.27945954 0 -1.49812436 0.12592331 0 -1.49812436 0.12592331 0;
createNode polyExtrudeFace -n "polyExtrudeFace37";
	rename -uid "4F3B02A9-4D63-55FB-A93B-8181B60F561E";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.0594949 2.8781083 0 ;
	setAttr ".rs" 58458;
	setAttr ".lt" -type "double3" 3.3306690738754696e-016 -5.5190605386467153e-016 1.4437014646895836 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -2.7865729685849168 1.7207762748838751 -0.5 ;
	setAttr ".cbx" -type "double3" 0.66758336060268597 4.0354403564182739 0.5 ;
createNode polyTweak -n "polyTweak48";
	rename -uid "97B34E6D-4168-E159-3824-AF984B0DACCD";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[28:31]" -type "float3"  0.010177493 -0.070959777 0
		 0.010177493 -0.070959777 0 -0.010177135 0.070959784 0 -0.010177135 0.070959784 0;
createNode polyExtrudeFace -n "polyExtrudeFace38";
	rename -uid "E53854A9-4B3D-75E7-EF4A-2EAB8E4A7303";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.25581673 4.0774312 0 ;
	setAttr ".rs" 50733;
	setAttr ".lt" -type "double3" 9.0965057508766647e-033 -7.2646200637498713e-017 1.4759924448100281 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.9828950606084947 2.9200993084972358 -0.5 ;
	setAttr ".cbx" -type "double3" 1.4712615875734016 5.2347630356077133 0.5 ;
createNode polyExtrudeFace -n "polyExtrudeFace39";
	rename -uid "FBEA0DBA-455C-0F34-630B-6D8CDE420DE4";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.40334365 5.4660707 0 ;
	setAttr ".rs" 58294;
	setAttr ".lt" -type "double3" 2.2204460492503131e-016 -8.9079042474411201e-016 2.790819116228846 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.560352891542697 4.4927285494593132 -0.5 ;
	setAttr ".cbx" -type "double3" 2.3670402052815644 6.4394130126023388 0.5 ;
createNode polyTweak -n "polyTweak49";
	rename -uid "6E09F777-48DA-BBCE-811E-EB9E5DC439D2";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[36:39]" -type "float3"  0.30712199 0.17669323 0 0.30712199
		 0.17669323 0 -0.2829752 0.2822707 0 -0.2829752 0.2822707 0;
createNode polyExtrudeFace -n "polyExtrudeFace40";
	rename -uid "5A63E9A1-454C-E15F-C3E8-DEA15E950976";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1.7202009 7.9923854 0 ;
	setAttr ".rs" 50356;
	setAttr ".lt" -type "double3" -2.1094237467877974e-015 8.161444144538519e-017 1.0517972292618327 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.51148786921574096 6.8862091104063339 -0.5 ;
	setAttr ".cbx" -type "double3" 3.9518897204787633 9.0985617654074424 0.5 ;
createNode polyTweak -n "polyTweak50";
	rename -uid "9C6118CC-4E1A-88E1-AF5F-61B7269C1CBE";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[40:43]" -type "float3"  0.15153763 -0.3283467 0 0.15153763
		 -0.3283467 0 -0.0095569538 0.24776676 0 -0.0095569538 0.24776676 0;
createNode polyExtrudeFace -n "polyExtrudeFace41";
	rename -uid "4CBB1250-47B3-DEB3-28D7-DBBD7C296930";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 2.1873097 8.9347687 0 ;
	setAttr ".rs" 33163;
	setAttr ".lt" -type "double3" -2.7755575615628914e-016 1.1354398861404896e-016 0.50780158509686046 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.04437929168491106 7.8285929691155687 -0.5 ;
	setAttr ".cbx" -type "double3" 4.4189988651389216 10.040943568432406 0.5 ;
createNode polyExtrudeFace -n "polyExtrudeFace42";
	rename -uid "4FE21228-439E-1D69-914D-38BA30A402E9";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 2.412827 9.3897448 0 ;
	setAttr ".rs" 41041;
	setAttr ".lt" -type "double3" 1.2351231148954867e-015 3.1284440622640543e-018 0.25380056703983767 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0.1811377200101818 8.2835702280275623 -0.5 ;
	setAttr ".cbx" -type "double3" 4.6445160895394197 10.495918452665832 0.5 ;
createNode polyExtrudeFace -n "polyExtrudeFace43";
	rename -uid "47588112-40A3-2B03-3CD5-578E323A4521";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 2.5255406 9.6171436 0 ;
	setAttr ".rs" 46972;
	setAttr ".lt" -type "double3" -5.5511151231257827e-017 -6.720424492635568e-017 0.69181894552371992 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0.2938512114669436 8.510969719165228 -0.5 ;
	setAttr ".cbx" -type "double3" 4.7572297582081431 10.723318103300644 0.5 ;
createNode polyExtrudeFace -n "polyExtrudeFace44";
	rename -uid "6138E202-4F15-34FF-39FC-629EA815AE72";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 2.8327804 10.236995 0 ;
	setAttr ".rs" 39331;
	setAttr ".lt" -type "double3" 1.0269562977782698e-015 3.7641474967040318e-019 0.24213660122430647 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0.6010905056947724 9.1308215990940838 -0.5 ;
	setAttr ".cbx" -type "double3" 5.0644703284131447 11.343168565533812 0.5 ;
createNode polyExtrudeFace -n "polyExtrudeFace45";
	rename -uid "51DCC669-480B-3673-B286-8D85AD7189CA";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 3.0077159 10.366716 0 ;
	setAttr ".rs" 38314;
	setAttr ".lt" -type "double3" -1.4988010832439613e-015 -1.8455612535826011e-016 
		0.93610965002904845 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0.84342709282864892 9.3477680522531053 -0.5 ;
	setAttr ".cbx" -type "double3" 5.1720048540268246 11.385664469939035 0.5 ;
createNode polyTweak -n "polyTweak51";
	rename -uid "6887D374-4DE2-8044-7963-83A32EC7ACCA";
	setAttr ".uopa" yes;
	setAttr -s 10 ".tk";
	setAttr ".tk[46]" -type "float3" -0.030060856 0.0689594 0 ;
	setAttr ".tk[47]" -type "float3" -0.030060856 0.0689594 0 ;
	setAttr ".tk[50]" -type "float3" -0.12083261 0.26463899 0 ;
	setAttr ".tk[51]" -type "float3" -0.12083261 0.26463899 0 ;
	setAttr ".tk[54]" -type "float3" -0.1019704 0.19626875 0 ;
	setAttr ".tk[55]" -type "float3" -0.1019704 0.19626875 0 ;
	setAttr ".tk[58]" -type "float3" 0.087821603 -0.038309366 0 ;
	setAttr ".tk[59]" -type "float3" 0.087821603 -0.038309366 0 ;
	setAttr ".tk[62]" -type "float3" -0.039485596 -0.21690062 0 ;
	setAttr ".tk[63]" -type "float3" -0.039485596 -0.21690062 0 ;
createNode polySplitRing -n "polySplitRing19";
	rename -uid "9C60D83F-4BA2-99A9-0D29-2CB2290B23AF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[124:125]" "e[127]" "e[129]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".wt" 0.31528866291046143;
	setAttr ".re" 129;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak52";
	rename -uid "E6D9E096-4335-E67D-B00E-3690C2BF6FBB";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[64:67]" -type "float3"  0.057726517 -0.12923671 0
		 0.057726517 -0.12923671 0 0.38314402 -1.20862007 0 0.38314402 -1.20862007 0;
createNode polyExtrudeFace -n "polyExtrudeFace46";
	rename -uid "B1D4C9F5-4E62-A244-7C5C-ABA9BED962C6";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 4.0511236 10.930003 0 ;
	setAttr ".rs" 41601;
	setAttr ".lt" -type "double3" 0.25243868668243186 1.253553953860382e-016 0.67174956596413826 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 2.3968265338681833 10.151159432829315 -0.5 ;
	setAttr ".cbx" -type "double3" 5.7054211314832193 11.708846606569477 0.5 ;
createNode polyTweak -n "polyTweak53";
	rename -uid "4457ABFE-4DFB-206D-EDE6-60A1CBB23904";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[68:69]" -type "float3"  -0.083924308 -0.05630612 0
		 -0.083924308 -0.05630612 0;
createNode polyExtrudeFace -n "polyExtrudeFace47";
	rename -uid "DC26DF7A-4DAE-F391-30EA-C282C3E73432";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 4.6085286 11.330191 0 ;
	setAttr ".rs" 56003;
	setAttr ".lt" -type "double3" 8.8817841970012523e-016 7.2112754660737671e-018 0.33897850000946661 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 3.3911055674263713 10.37873592156102 -0.5 ;
	setAttr ".cbx" -type "double3" 5.825951249320803 12.281646214329914 0.5 ;
createNode polyTweak -n "polyTweak54";
	rename -uid "B0EA1693-4C52-8E96-1895-1CA0BFE65A1F";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[72:75]" -type "float3"  -0.4662388 0.11044775 0 -0.4662388
		 0.11044775 0 0.37488112 -0.30804598 0 0.37488112 -0.30804598 0;
createNode polyExtrudeFace -n "polyExtrudeFace48";
	rename -uid "1A045DA1-4CB0-28CC-42C9-F18DA3E93513";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 4.817265 11.597277 0 ;
	setAttr ".rs" 61077;
	setAttr ".lt" -type "double3" -1.2212453270876722e-015 3.9894764898382425e-017 0.89011954979293262 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 3.4476724854069536 10.526894935493813 -0.5 ;
	setAttr ".cbx" -type "double3" 6.1868572815518972 12.667658408202842 0.5 ;
createNode polyTweak -n "polyTweak55";
	rename -uid "5B91C09D-401A-E9C7-FEA6-05818DD61AFA";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[76:79]" -type "float3"  0.013403343 -0.19266407 0
		 0.013403343 -0.19266407 0 -0.013403343 0.19266407 0 -0.013403343 0.19266407 0;
createNode polyExtrudeFace -n "polyExtrudeFace49";
	rename -uid "9B4C51A3-4B49-4CBB-C962-F290B0483492";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 5.3653855 12.298616 0 ;
	setAttr ".rs" 54093;
	setAttr ".lt" -type "double3" -1.2212453270876722e-015 -7.0221931291051022e-017 
		0.78242769052232441 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 3.9957927886055424 11.228234494317041 -0.5 ;
	setAttr ".cbx" -type "double3" 6.7349779945531454 13.368998335863223 0.5 ;
createNode polyExtrudeFace -n "polyExtrudeFace50";
	rename -uid "15F138FC-4A45-8B70-2A68-FB9076C47EB8";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 6.6724854 13.28484 0 ;
	setAttr ".rs" 44983;
	setAttr ".lt" -type "double3" 1.7248906526500735e-015 -9.1193605274427305e-017 2.3365253311861918 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 5.9003952819891188 11.763441197335085 -0.5 ;
	setAttr ".cbx" -type "double3" 7.4445749622639701 14.806238251712353 0.5 ;
createNode polyTweak -n "polyTweak56";
	rename -uid "72B84C4A-44E6-82D1-D836-F6BB98B1B172";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk[82:87]" -type "float3"  0.36119914 -0.059484217 0
		 0.36119914 -0.059484217 0 0.091975085 -0.22368868 -2.9802322e-008 0.091975063 -0.22368877
		 2.9802322e-008 1.56187189 -0.50847131 -2.9802322e-008 1.56187177 -0.50847143 2.9802322e-008;
createNode polyExtrudeFace -n "polyExtrudeFace51";
	rename -uid "2BA0AB93-4DD2-51D7-FBF5-A98947BEB76D";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 8.5238495 14.71376 0 ;
	setAttr ".rs" 63178;
	setAttr ".lt" -type "double3" 2.4980018054066022e-015 -1.0173748803906579e-016 1.6729754529088561 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 7.9396723001091933 12.454170422194991 -0.5 ;
	setAttr ".cbx" -type "double3" 9.1080263443921865 16.973351099463745 0.5 ;
createNode polyTweak -n "polyTweak57";
	rename -uid "C833A325-4F8D-6307-035B-65B815BE9C75";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[88:91]" -type "float3"  -0.55358094 0.066982992 0
		 -0.55358094 0.066982992 0 0.79520476 0.77531117 0 0.79520476 0.77531117 0;
createNode polyExtrudeFace -n "polyExtrudeFace52";
	rename -uid "27EE61E0-45F6-1500-0BE8-70878F73F148";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 10.143569 15.132511 0 ;
	setAttr ".rs" 62199;
	setAttr ".lt" -type "double3" 4.0939474033052647e-015 -4.49754336665352e-016 1.6508079286281172 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 10.022190718616049 12.593153653027578 -0.5 ;
	setAttr ".cbx" -type "double3" 10.264947614728825 17.671869254275688 0.5 ;
createNode polyTweak -n "polyTweak58";
	rename -uid "F6147B86-4AF9-2982-76AA-D7BE050F8091";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[92:95]" -type "float3"  -0.51754707 0.15683162 0 -0.51754707
		 0.15683162 0 0.51754707 -0.15683162 0 0.51754707 -0.15683162 0;
createNode polyExtrudeFace -n "polyExtrudeFace53";
	rename -uid "7F32719D-41F9-9F0D-B734-09B742A8C783";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 11.939558 15.10103 0 ;
	setAttr ".rs" 62042;
	setAttr ".lt" -type "double3" -0.27429334159988078 -1.2041300302434664e-016 2.2623037189585444 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 11.43136763562919 12.471240450198914 -0.5 ;
	setAttr ".cbx" -type "double3" 12.447747767999484 17.730821088625582 0.5 ;
createNode polyTweak -n "polyTweak59";
	rename -uid "22320A94-44C2-673A-B2C3-02BA85F9D2A5";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[96:99]" -type "float3"  -0.47198418 0.2243531 0 -0.47198418
		 0.2243531 0 0.50478464 -0.59054255 0 0.50478464 -0.59054255 0;
createNode polyExtrudeFace -n "polyExtrudeFace54";
	rename -uid "A9AFC482-4580-2223-7DDB-03B2ED25E3A6";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 13.998429 14.29219 0 ;
	setAttr ".rs" 37580;
	setAttr ".lt" -type "double3" -0.20771489929204634 -4.1587989746548693e-016 2.0688352862620145 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 12.657479546936859 11.973587558722754 -0.5 ;
	setAttr ".cbx" -type "double3" 15.339379280043246 16.610791696108013 0.5 ;
createNode polyTweak -n "polyTweak60";
	rename -uid "2D57E4AC-45EF-6D4C-25E5-7D8AA2B77009";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[100:103]" -type "float3"  -0.48156619 0.83535039 0 -0.48156619
		 0.83535039 0 0.170031 -0.81895995 0 0.170031 -0.81895995 0;
createNode polyExtrudeFace -n "polyExtrudeFace55";
	rename -uid "EDC1B045-4F91-C50D-A25F-7291DA632296";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 15.313221 12.679711 0 ;
	setAttr ".rs" 54061;
	setAttr ".lt" -type "double3" -0.31645127072018986 -4.8095959918068384e-016 1.9694249309241196 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 13.976455106845396 11.11668591470715 -0.5 ;
	setAttr ".cbx" -type "double3" 16.649987652833186 14.242736470197181 0.5 ;
createNode polyTweak -n "polyTweak61";
	rename -uid "B004E84E-40ED-34D8-438F-E7943846FF69";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[104:107]" -type "float3"  0.020449094 0.51340747 0 0.020449094
		 0.51340747 0 -1.10836029 -0.49130234 0 -1.10836029 -0.49130234 0;
createNode polyExtrudeFace -n "polyExtrudeFace56";
	rename -uid "168D9BDB-4D59-F737-618A-9DBA547C6573";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 15.887012 11.044416 0 ;
	setAttr ".rs" 33482;
	setAttr ".lt" -type "double3" 0.3197700004315821 -1.8267965584166322e-016 1.2792610137773619 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 14.88828359420333 10.552937747551995 -0.5 ;
	setAttr ".cbx" -type "double3" 16.88573852389144 11.535895009274492 0.5 ;
createNode polyTweak -n "polyTweak62";
	rename -uid "19F0E39C-4444-9289-9C90-3D9E871315C0";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[108:111]" -type "float3"  0.45748922 0.92192125 -2.3283064e-010
		 0.45748922 0.92192125 5.8207661e-010 -1.58771396 -0.0092485184 -5.8207661e-010 -1.58771396
		 -0.0092485184 2.3283064e-010;
createNode polyExtrudeFace -n "polyExtrudeFace57";
	rename -uid "16ED12A2-41F5-5566-4D13-09B264B61DB8";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 16.738764 10.037799 0 ;
	setAttr ".rs" 34003;
	setAttr ".lt" -type "double3" 0.14551889635843562 -1.1390613834833137e-016 0.48991144412730531 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 16.064672543919635 9.5452881219227272 -0.5 ;
	setAttr ".cbx" -type "double3" 17.412854148911233 10.530310559073607 0.5 ;
createNode polyTweak -n "polyTweak63";
	rename -uid "A1729194-447F-651A-1A52-0E94370BF190";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[112:115]" -type "float3"  0.2164077 -0.24198747 0 0.2164077
		 -0.24198747 0 -0.21640772 0.24198744 0 -0.21640772 0.24198744 0;
createNode polyExtrudeFace -n "polyExtrudeFace58";
	rename -uid "E1EF9685-4E6E-C3BC-E847-E2829B4CB5A0";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 17.145281 9.7280703 0 ;
	setAttr ".rs" 33419;
	setAttr ".lt" -type "double3" -0.055255943507356436 -1.3324869014333069e-016 0.53501470602392165 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 16.471190527390579 9.2355591589848434 -0.5 ;
	setAttr ".cbx" -type "double3" 17.819372132382174 10.220581596135725 0.5 ;
createNode polyExtrudeFace -n "polyExtrudeFace59";
	rename -uid "24C92CBA-4225-4456-C014-74BC3F7BC514";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 17.338861 9.224762 0 ;
	setAttr ".rs" 43077;
	setAttr ".lt" -type "double3" -0.16366604393525652 -1.5151665572749725e-016 0.65860489578813042 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 16.826234552785255 9.0658220319120826 -0.5 ;
	setAttr ".cbx" -type "double3" 17.851488413630641 9.3837017667234388 0.5 ;
createNode polyTweak -n "polyTweak64";
	rename -uid "60147182-4BE9-6914-B700-DFB8098AC7EA";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[120:123]" -type "float3"  0.27537593 0.13479324 0 0.27537593
		 0.13479324 0 -0.43653068 -0.07148613 0 -0.43653068 -0.07148613 0;
createNode polyExtrudeFace -n "polyExtrudeFace60";
	rename -uid "59BC9C72-4E84-B445-9AD3-178C7DCFCF45";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 17.377577 8.5472317 0 ;
	setAttr ".rs" 64265;
	setAttr ".lt" -type "double3" -0.063678942999295524 -1.1175696592666487e-016 0.49926402769981199 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 16.902227622594534 8.4866032030728267 -0.5 ;
	setAttr ".cbx" -type "double3" 17.85292557462218 8.6078605437257156 0.5 ;
createNode polyTweak -n "polyTweak65";
	rename -uid "52D0A4D7-40BD-71A4-1901-FE8240082A45";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[124:127]" -type "float3"  0.098010458 0.038060453 0
		 0.098010458 0.038060453 0 -0.098010458 -0.038060453 0 -0.098010458 -0.038060453 0;
createNode polyExtrudeFace -n "polyExtrudeFace61";
	rename -uid "5BCD9D71-4772-0EB8-93B6-F48CFB385437";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 17.377577 8.0439224 0 ;
	setAttr ".rs" 63255;
	setAttr ".lt" -type "double3" -0.046924481694957623 -1.165926038754146e-016 0.52091578750979173 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 16.902226965810332 7.9832950674254537 -0.5 ;
	setAttr ".cbx" -type "double3" 17.852927044381506 8.1045504941125817 0.5 ;
createNode polyExtrudeFace -n "polyExtrudeFace62";
	rename -uid "0476027C-4640-56AA-4E23-96891C456A9B";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 17.396936 7.521256 0 ;
	setAttr ".rs" 39752;
	setAttr ".lt" -type "double3" -0.0032311375631281364 -3.7610517379165974e-017 0.33134024614591573 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 16.896122002919174 7.4573794350095071 -0.5 ;
	setAttr ".cbx" -type "double3" 17.897751713762712 7.5851323982668539 0.5 ;
createNode polyTweak -n "polyTweak66";
	rename -uid "B98D72E7-4B2D-79D5-F85C-D396A605B0C9";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[132:135]" -type "float3"  -0.019448856 0.016754137 0
		 -0.019448856 0.016754137 0 0.019448856 -0.016754137 0 0.019448856 -0.016754137 0;
createNode polyExtrudeFace -n "polyExtrudeFace63";
	rename -uid "C24B7D3A-4B9E-2FD8-CD12-408B6E439215";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 17.435652 7.1921692 0 ;
	setAttr ".rs" 65374;
	setAttr ".lt" -type "double3" 5.0792703376600912e-015 -6.9771270263953528e-017 0.62860519598137599 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 16.693385832980521 7.0974970297033462 -0.5 ;
	setAttr ".cbx" -type "double3" 18.17791822775791 7.2868412873809838 0.5 ;
createNode polyTweak -n "polyTweak67";
	rename -uid "547606E9-4F88-B652-A882-818E44F64BF4";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[136:139]" -type "float3"  -0.18441637 0.15886454 0 -0.18441637
		 0.15886454 0 0.18441637 -0.15886454 0 0.18441637 -0.15886454 0;
createNode polyExtrudeFace -n "polyExtrudeFace64";
	rename -uid "FE94A511-4F94-0CE8-DCEA-A0A0E83D9CF1";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 16.815525 6.5050106 0 ;
	setAttr ".rs" 37425;
	setAttr ".lt" -type "double3" -4.1390502136806617e-015 -2.1908995712935191e-019 
		0.55492616403396733 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 15.025839135517053 6.4103382606770101 -0.5 ;
	setAttr ".cbx" -type "double3" 18.605211784027702 6.5996826624983616 0.5 ;
createNode polyTweak -n "polyTweak68";
	rename -uid "F64D28A9-4E21-45EC-F16F-3DACCDEA9377";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[140:143]" -type "float3"  -1.21603346 1.25601721 0 -1.21603346
		 1.25601721 0 0.18537188 -0.30103767 0 0.18537188 -0.30103767 0;
createNode polyExtrudeFace -n "polyExtrudeFace65";
	rename -uid "1EA0117C-4ABE-7AFE-333F-1186F149239B";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 16.463209 5.95086 0 ;
	setAttr ".rs" 62269;
	setAttr ".lt" -type "double3" 1.4502288259166107e-015 -1.9527723577357411e-016 0.87922666078584311 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 14.321997135911719 5.8375930921274541 -0.5 ;
	setAttr ".cbx" -type "double3" 18.604419964387542 6.0641273425890727 0.5 ;
createNode polyTweak -n "polyTweak69";
	rename -uid "8C6C786C-481F-B2DE-ABF0-AE924725F1A8";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[144:147]" -type "float3"  -0.50428843 0.53250152 0 -0.50428843
		 0.53250152 0 -0.0063161906 0.034814417 0 -0.0063161906 0.034814417 0;
createNode polyExtrudeFace -n "polyExtrudeFace66";
	rename -uid "233C033E-4459-2CB5-A3CF-21B9D1789E74";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 16.636864 4.8184414 0 ;
	setAttr ".rs" 42449;
	setAttr ".lt" -type "double3" 0.10007200657362414 -1.815594956620671e-016 0.81739053024228459 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 14.421234708631435 4.7012367099059773 -0.5 ;
	setAttr ".cbx" -type "double3" 18.852492745796042 4.9356459916280322 0.5 ;
createNode polyTweak -n "polyTweak70";
	rename -uid "67B59683-4A39-1A01-5BE6-7791C7BF9B53";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[148:151]" -type "float3"  -0.15671584 -0.21207307 0
		 -0.15671584 -0.21207307 0 -0.051293217 -0.31743547 0 -0.051293217 -0.31743547 0;
createNode polyExtrudeFace -n "polyExtrudeFace67";
	rename -uid "02860215-436F-8FC2-F01D-28B4B5DEF7C5";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 16.970789 3.7848604 0 ;
	setAttr ".rs" 46202;
	setAttr ".lt" -type "double3" 0.19337321946314426 -1.1475045608541448e-016 0.53973217571876719 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 14.933102223728827 3.2776134031807485 -0.5 ;
	setAttr ".cbx" -type "double3" 19.00847717914866 4.2921073231304714 0.5 ;
createNode polyTweak -n "polyTweak71";
	rename -uid "126AB394-4A1B-52DB-8EC6-C288512BB2A4";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[152:155]" -type "float3"  -0.20868693 -0.68394727 0
		 -0.20868693 -0.68394727 0 0.13305499 0.10243651 0 0.13305499 0.10243651 0;
createNode polyExtrudeFace -n "polyExtrudeFace68";
	rename -uid "B6EB9A9C-4980-A5AB-E54C-C397C7C1393F";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 17.336517 3.3396249 0 ;
	setAttr ".rs" 61051;
	setAttr ".lt" -type "double3" 0.13713962431352589 -5.1637705238436567e-017 0.25817778672104308 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 15.504074211746424 2.7785681948128524 -0.5 ;
	setAttr ".cbx" -type "double3" 19.168958819495035 3.9006814784129396 0.5 ;
createNode polyTweak -n "polyTweak72";
	rename -uid "7A88B060-403A-D9FD-1BFA-D88E9C2910FE";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[156:159]" -type "float3"  0.15285927 -0.20273302 0 0.15285927
		 -0.20273302 0 -0.041757353 0.17436893 0 -0.041757353 0.17436893 0;
createNode polyExtrudeFace -n "polyExtrudeFace69";
	rename -uid "FE1FBC83-48FB-F030-13CE-67B28F4104BD";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 17.543232 3.1329083 0 ;
	setAttr ".rs" 40776;
	setAttr ".lt" -type "double3" 0.1241132814979416 -1.9419307952916358e-017 0.13778022789975844 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 15.807782837490748 2.6015491141727707 -0.5 ;
	setAttr ".cbx" -type "double3" 19.278681473796208 3.6642675721390705 0.5 ;
createNode polyTweak -n "polyTweak73";
	rename -uid "6764B884-4345-0494-700E-D3BA75A2C794";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[160:163]" -type "float3"  0.086959779 -0.052226175 0
		 0.086959779 -0.052226175 0 -0.086959779 0.052226175 0 -0.086959779 0.052226175 0;
createNode polyExtrudeFace -n "polyExtrudeFace70";
	rename -uid "03BE1C66-482B-C430-00FF-5A965AC3A3E1";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".ws" yes;
	setAttr ".mp" -type "matrix" 0.6689795199590608 0.74328083647793886 0 0 -0.74328083647793886 0.6689795199590608 0 0
		 0 0 1 0 -4.7137335680730006 -1.8679299996434993 0 1;
	setAttr ".pvt" -type "float3" 17.543232 3.1329083 0 ;
	setAttr ".rs" 40776;
	setAttr ".lt" -type "double3" 0.1241132814979416 -1.9419307952916358e-017 0.13778022789975844 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 15.807782837490748 2.6015491141727707 -0.5 ;
	setAttr ".cbx" -type "double3" 19.278681473796208 3.6642675721390705 0.5 ;
createNode polyCube -n "polyCube5";
	rename -uid "1156CF4A-4D8A-8593-A32B-32958C33CAB1";
	setAttr ".cuv" 4;
createNode polySplitRing -n "polySplitRing20";
	rename -uid "99EEE657-4EA5-742C-1A0A-919E80322E75";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[4:5]" "e[8:9]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 6.9817939883132452 -3.7818050770030065 -1.923679252240289 1;
	setAttr ".wt" 0.59636229276657104;
	setAttr ".dr" no;
	setAttr ".re" 4;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak74";
	rename -uid "AFE0C868-4ED9-2274-455C-8781E79FB47E";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  -15.0024824142 -3.8614819
		 0 12.53649902 -3.8614819 0 -11.41229725 1.39548516 0 12.53649902 3.8614819 0 -11.41229725
		 1.39548516 0 12.53649902 3.8614819 0 -15.0024824142 -3.8614819 0 12.53649902 -3.8614819
		 0;
createNode polySplitRing -n "polySplitRing21";
	rename -uid "421A8156-4966-4295-D602-18ACB300540C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[4:5]" "e[13]" "e[15]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 6.9817939883132452 -3.7818050770030065 -1.923679252240289 1;
	setAttr ".wt" 0.66737127304077148;
	setAttr ".dr" no;
	setAttr ".re" 4;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak75";
	rename -uid "0A88011D-455D-DE01-1C90-A3AD0020B7CA";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[8:9]" -type "float3"  0.68902838 0.79782248 0 0.68902838
		 0.79782248 0;
createNode polyExtrudeFace -n "polyExtrudeFace71";
	rename -uid "6F3FE7A7-4935-B2C5-3FEC-9C9A8E5035D2";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 6.9817939883132452 -3.7818050770030065 -1.923679252240289 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 7.5438948 -0.65332168 -1.9236792 ;
	setAttr ".rs" 49748;
	setAttr ".lt" -type "double3" 5.5511151231257827e-016 -8.5412906295245424e-016 4.1479594161013438 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -4.9305032605270869 -1.8863199142680211 -2.423679252240289 ;
	setAttr ".cbx" -type "double3" 20.018293011750746 0.57967658956193491 -1.423679252240289 ;
createNode polyTweak -n "polyTweak76";
	rename -uid "5EE81D80-4CAA-0C42-0595-0FB601DF26D6";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[0]" -type "float3" 0.32638192 1.7044388 0 ;
	setAttr ".tk[6]" -type "float3" 0.32638192 1.7044388 0 ;
	setAttr ".tk[12]" -type "float3" 0.14505862 0 0 ;
	setAttr ".tk[13]" -type "float3" 0.14505862 0 0 ;
createNode polySplitRing -n "polySplitRing22";
	rename -uid "859FADE1-4FAA-D720-4556-7DB5290E689F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[28:29]" "e[31]" "e[33]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 6.9817939883132452 -3.7818050770030065 -1.923679252240289 1;
	setAttr ".wt" 0.18206407129764557;
	setAttr ".re" 29;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak77";
	rename -uid "65AE1173-4B20-F391-D3E8-BB8BC6E06FCB";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk";
	setAttr ".tk[3]" -type "float3" -3.7252903e-009 1.3514518 0 ;
	setAttr ".tk[5]" -type "float3" -3.7252903e-009 1.3514518 0 ;
	setAttr ".tk[16]" -type "float3" 1.9251637 0.19028765 0 ;
	setAttr ".tk[17]" -type "float3" -0.41153765 -0.86601353 0 ;
	setAttr ".tk[18]" -type "float3" -0.41153765 -0.86601353 0 ;
	setAttr ".tk[19]" -type "float3" 1.9251637 0.19028765 0 ;
createNode polySplitRing -n "polySplitRing23";
	rename -uid "A23154C8-472C-4280-ADCD-59B439B776C6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[36:37]" "e[39]" "e[41]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 6.9817939883132452 -3.7818050770030065 -1.923679252240289 1;
	setAttr ".wt" 0.21406489610671997;
	setAttr ".re" 36;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing24";
	rename -uid "F58029C7-480D-FC0B-A941-49A65A1AB8EF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[44:45]" "e[47]" "e[49]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 6.9817939883132452 -3.7818050770030065 -1.923679252240289 1;
	setAttr ".wt" 0.35304185748100281;
	setAttr ".re" 44;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyExtrudeFace -n "polyExtrudeFace72";
	rename -uid "5F1843F2-4A58-F910-59CA-768E5C078465";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 6.9817939883132452 -3.7818050770030065 -1.923679252240289 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 7.8927026 3.1366601 -1.9236794 ;
	setAttr ".rs" 49190;
	setAttr ".lt" -type "double3" 9.9920072216264089e-016 -4.3837654646588955e-016 10.012450207629714 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -3.413345705900622 2.4318122478260951 -2.4236793714495786 ;
	setAttr ".cbx" -type "double3" 19.198751080598402 3.8415078731312708 -1.423679252240289 ;
createNode polyTweak -n "polyTweak78";
	rename -uid "DD6AA9A0-4923-4224-0E4C-61AEBB647FDB";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk";
	setAttr ".tk[20]" -type "float3" -0.10942621 -0.14590162 0 ;
	setAttr ".tk[23]" -type "float3" -0.10942621 -0.14590162 0 ;
	setAttr ".tk[24]" -type "float3" -0.21885246 -0.018237703 0 ;
	setAttr ".tk[27]" -type "float3" -0.21885246 -0.018237703 0 ;
	setAttr ".tk[28]" -type "float3" -0.10942621 0.31004095 0 ;
	setAttr ".tk[31]" -type "float3" -0.10942621 0.31004095 0 ;
createNode polyUnite -n "polyUnite1";
	rename -uid "60025BE7-46FE-A424-0CF7-F4B92B455C2F";
	setAttr -s 5 ".ip";
	setAttr -s 5 ".im";
createNode groupId -n "groupId1";
	rename -uid "FF877E58-4C78-23B1-0E67-0AA6F93F900A";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "AE659ADA-4306-ED9B-6151-2CA3349ADD5B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:113]";
createNode groupId -n "groupId2";
	rename -uid "AB212DBB-4E19-2166-0344-EB9C0DF959A1";
	setAttr ".ihi" 0;
createNode groupId -n "groupId3";
	rename -uid "EDF2362A-4458-FD9A-79FA-43A8A110FBFB";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "EE15FB85-4235-1B1F-FEB4-09AF2F897868";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:49]";
createNode groupId -n "groupId4";
	rename -uid "E597675D-42D3-9520-E169-BAAB07C05D67";
	setAttr ".ihi" 0;
createNode groupId -n "groupId5";
	rename -uid "D766FFB1-471F-2C4B-B30B-C19D3F4B9000";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "7AEBA212-448D-E735-7303-1C9990D4AB9C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:57]";
createNode groupId -n "groupId6";
	rename -uid "2E2CB452-4CFF-57E2-B38D-BA8D16F24370";
	setAttr ".ihi" 0;
createNode groupId -n "groupId7";
	rename -uid "745DF2BF-4883-D316-4884-C5AC91FFB4AA";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	rename -uid "614A6DB9-4A19-1128-91FA-6B9A9D103444";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:165]";
createNode groupId -n "groupId8";
	rename -uid "A989324B-418A-8CC0-E173-C68649B8C7CE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId9";
	rename -uid "4E27A824-42B6-11A6-79A8-6C86B4213FC9";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts5";
	rename -uid "2D883F8E-4A30-230B-D758-C4A69DF6B8ED";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:157]";
createNode groupId -n "groupId10";
	rename -uid "3D06F8BC-411B-F17A-5644-E38629D692DB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId11";
	rename -uid "B1E2C087-416D-C462-2473-A3B2871465D3";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts6";
	rename -uid "4830BB0D-4EAC-5211-2838-DFB808253204";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:545]";
createNode deleteComponent -n "deleteComponent1";
	rename -uid "E9048F5C-4F49-B2AD-CD87-779267C57BE4";
	setAttr ".dc" -type "componentList" 98 "f[2]" "f[7]" "f[11]" "f[15]" "f[19]" "f[23]" "f[27]" "f[31]" "f[35]" "f[39]" "f[43]" "f[47]" "f[51]" "f[55]" "f[61]" "f[65]" "f[69]" "f[73]" "f[77]" "f[81]" "f[85]" "f[89]" "f[93]" "f[97]" "f[101]" "f[105]" "f[109]" "f[113]" "f[116]" "f[127:133]" "f[149]" "f[153]" "f[157]" "f[161]" "f[166]" "f[177:183]" "f[199]" "f[203]" "f[207]" "f[211]" "f[215]" "f[219]" "f[224]" "f[230]" "f[233]" "f[237]" "f[241]" "f[245]" "f[249]" "f[253]" "f[257]" "f[261]" "f[265]" "f[269]" "f[273]" "f[277]" "f[281]" "f[285]" "f[289]" "f[293]" "f[297]" "f[301]" "f[305]" "f[309]" "f[313]" "f[317]" "f[321]" "f[325]" "f[329]" "f[333]" "f[337]" "f[341]" "f[345]" "f[349]" "f[353]" "f[357]" "f[361]" "f[365]" "f[369]" "f[373]" "f[377]" "f[381]" "f[385]" "f[390]" "f[419:443]" "f[495]" "f[501]" "f[503]" "f[507]" "f[511]" "f[515]" "f[521]" "f[525]" "f[528]" "f[532]" "f[536]" "f[540]" "f[544]";
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 12 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 11 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "groupParts5.og" "pCubeShape1.i";
connectAttr "groupId9.id" "pCubeShape1.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape1.iog.og[0].gco";
connectAttr "groupId10.id" "pCubeShape1.ciog.cog[0].cgid";
connectAttr ":defaultColorMgtGlobals.cme" "imagePlaneShape1.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "imagePlaneShape1.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "imagePlaneShape1.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "imagePlaneShape1.ws";
connectAttr ":frontShape.msg" "imagePlaneShape1.ltc";
connectAttr "groupId3.id" "pCubeShape2.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape2.iog.og[0].gco";
connectAttr "groupParts2.og" "pCubeShape2.i";
connectAttr "groupId4.id" "pCubeShape2.ciog.cog[0].cgid";
connectAttr "groupId5.id" "pCubeShape3.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape3.iog.og[0].gco";
connectAttr "groupParts3.og" "pCubeShape3.i";
connectAttr "groupId6.id" "pCubeShape3.ciog.cog[0].cgid";
connectAttr "groupId1.id" "pCubeShape4.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape4.iog.og[0].gco";
connectAttr "groupParts1.og" "pCubeShape4.i";
connectAttr "groupId2.id" "pCubeShape4.ciog.cog[0].cgid";
connectAttr "groupId7.id" "pCubeShape5.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape5.iog.og[0].gco";
connectAttr "groupParts4.og" "pCubeShape5.i";
connectAttr "groupId8.id" "pCubeShape5.ciog.cog[0].cgid";
connectAttr "polyExtrudeFace72.out" "pCubeShape6.i";
connectAttr "deleteComponent1.og" "pCube7Shape.i";
connectAttr "groupId11.id" "pCube7Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCube7Shape.iog.og[0].gco";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCube1.out" "polyExtrudeFace1.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polyTweak1.out" "polySplitRing1.ip";
connectAttr "pCubeShape1.wm" "polySplitRing1.mp";
connectAttr "polyExtrudeFace1.out" "polyTweak1.ip";
connectAttr "polySplitRing1.out" "polyExtrudeFace2.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyTweak2.out" "polySplitRing2.ip";
connectAttr "pCubeShape1.wm" "polySplitRing2.mp";
connectAttr "polyExtrudeFace2.out" "polyTweak2.ip";
connectAttr "polySplitRing2.out" "polySplitRing3.ip";
connectAttr "pCubeShape1.wm" "polySplitRing3.mp";
connectAttr "polySplitRing3.out" "polySplitRing4.ip";
connectAttr "pCubeShape1.wm" "polySplitRing4.mp";
connectAttr "polySplitRing4.out" "polySplitRing5.ip";
connectAttr "pCubeShape1.wm" "polySplitRing5.mp";
connectAttr "polyTweak3.out" "polyExtrudeFace3.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace3.mp";
connectAttr "polySplitRing5.out" "polyTweak3.ip";
connectAttr "polyTweak4.out" "polyExtrudeFace4.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace4.mp";
connectAttr "polyExtrudeFace3.out" "polyTweak4.ip";
connectAttr "polyTweak5.out" "polySplitRing6.ip";
connectAttr "pCubeShape1.wm" "polySplitRing6.mp";
connectAttr "polyExtrudeFace4.out" "polyTweak5.ip";
connectAttr "polyTweak6.out" "polySplitRing7.ip";
connectAttr "pCubeShape1.wm" "polySplitRing7.mp";
connectAttr "polySplitRing6.out" "polyTweak6.ip";
connectAttr "polyTweak7.out" "polySplitRing8.ip";
connectAttr "pCubeShape1.wm" "polySplitRing8.mp";
connectAttr "polySplitRing7.out" "polyTweak7.ip";
connectAttr "polyTweak8.out" "polySplitRing9.ip";
connectAttr "pCubeShape1.wm" "polySplitRing9.mp";
connectAttr "polySplitRing8.out" "polyTweak8.ip";
connectAttr "polyTweak9.out" "polySplitRing10.ip";
connectAttr "pCubeShape1.wm" "polySplitRing10.mp";
connectAttr "polySplitRing9.out" "polyTweak9.ip";
connectAttr "polyTweak10.out" "polyExtrudeFace5.ip";
connectAttr "pCubeShape2.wm" "polyExtrudeFace5.mp";
connectAttr "polyCube2.out" "polyTweak10.ip";
connectAttr "polyTweak11.out" "polySplitRing11.ip";
connectAttr "pCubeShape2.wm" "polySplitRing11.mp";
connectAttr "polyExtrudeFace5.out" "polyTweak11.ip";
connectAttr "polyTweak12.out" "polySplitRing12.ip";
connectAttr "pCubeShape2.wm" "polySplitRing12.mp";
connectAttr "polySplitRing11.out" "polyTweak12.ip";
connectAttr "polyTweak13.out" "polySplitRing13.ip";
connectAttr "pCubeShape2.wm" "polySplitRing13.mp";
connectAttr "polySplitRing12.out" "polyTweak13.ip";
connectAttr "polyTweak14.out" "polySplitRing14.ip";
connectAttr "pCubeShape2.wm" "polySplitRing14.mp";
connectAttr "polySplitRing13.out" "polyTweak14.ip";
connectAttr "polySurfaceShape1.o" "polySplitRing15.ip";
connectAttr "pCubeShape3.wm" "polySplitRing15.mp";
connectAttr "polyTweak15.out" "polySplitRing16.ip";
connectAttr "pCubeShape3.wm" "polySplitRing16.mp";
connectAttr "polySplitRing15.out" "polyTweak15.ip";
connectAttr "polyTweak16.out" "polySplitRing17.ip";
connectAttr "pCubeShape4.wm" "polySplitRing17.mp";
connectAttr "polyCube3.out" "polyTweak16.ip";
connectAttr "polyTweak17.out" "polyExtrudeFace6.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace6.mp";
connectAttr "polySplitRing17.out" "polyTweak17.ip";
connectAttr "polyTweak18.out" "polyExtrudeFace7.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace7.mp";
connectAttr "polyExtrudeFace6.out" "polyTweak18.ip";
connectAttr "polyTweak19.out" "polyExtrudeFace8.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace8.mp";
connectAttr "polyExtrudeFace7.out" "polyTweak19.ip";
connectAttr "polyTweak20.out" "polyExtrudeFace9.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace9.mp";
connectAttr "polyExtrudeFace8.out" "polyTweak20.ip";
connectAttr "polyTweak21.out" "polyExtrudeFace10.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace10.mp";
connectAttr "polyExtrudeFace9.out" "polyTweak21.ip";
connectAttr "polyTweak22.out" "polyExtrudeFace11.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace11.mp";
connectAttr "polyExtrudeFace10.out" "polyTweak22.ip";
connectAttr "polyExtrudeFace11.out" "polyExtrudeFace12.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace12.mp";
connectAttr "polyTweak23.out" "polyExtrudeFace13.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace13.mp";
connectAttr "polyExtrudeFace12.out" "polyTweak23.ip";
connectAttr "polyTweak24.out" "polyExtrudeFace14.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace14.mp";
connectAttr "polyExtrudeFace13.out" "polyTweak24.ip";
connectAttr "polyTweak25.out" "polyExtrudeFace15.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace15.mp";
connectAttr "polyExtrudeFace14.out" "polyTweak25.ip";
connectAttr "polyTweak26.out" "polyExtrudeFace16.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace16.mp";
connectAttr "polyExtrudeFace15.out" "polyTweak26.ip";
connectAttr "polyTweak27.out" "polyExtrudeFace17.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace17.mp";
connectAttr "polyExtrudeFace16.out" "polyTweak27.ip";
connectAttr "polyTweak28.out" "polyExtrudeFace18.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace18.mp";
connectAttr "polyExtrudeFace17.out" "polyTweak28.ip";
connectAttr "polyTweak29.out" "polyExtrudeFace19.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace19.mp";
connectAttr "polyExtrudeFace18.out" "polyTweak29.ip";
connectAttr "polyTweak30.out" "polyExtrudeFace20.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace20.mp";
connectAttr "polyExtrudeFace19.out" "polyTweak30.ip";
connectAttr "polyTweak31.out" "polyExtrudeFace21.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace21.mp";
connectAttr "polyExtrudeFace20.out" "polyTweak31.ip";
connectAttr "polyTweak32.out" "polyExtrudeFace22.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace22.mp";
connectAttr "polyExtrudeFace21.out" "polyTweak32.ip";
connectAttr "polyTweak33.out" "polyExtrudeFace23.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace23.mp";
connectAttr "polyExtrudeFace22.out" "polyTweak33.ip";
connectAttr "polyTweak34.out" "polyExtrudeFace24.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace24.mp";
connectAttr "polyExtrudeFace23.out" "polyTweak34.ip";
connectAttr "polyTweak35.out" "polyExtrudeFace25.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace25.mp";
connectAttr "polyExtrudeFace24.out" "polyTweak35.ip";
connectAttr "polyTweak36.out" "polyExtrudeFace26.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace26.mp";
connectAttr "polyExtrudeFace25.out" "polyTweak36.ip";
connectAttr "polyTweak37.out" "polyExtrudeFace27.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace27.mp";
connectAttr "polyExtrudeFace26.out" "polyTweak37.ip";
connectAttr "polyTweak38.out" "polyExtrudeFace28.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace28.mp";
connectAttr "polyExtrudeFace27.out" "polyTweak38.ip";
connectAttr "polyTweak39.out" "polyExtrudeFace29.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace29.mp";
connectAttr "polyExtrudeFace28.out" "polyTweak39.ip";
connectAttr "polyTweak40.out" "polyExtrudeFace30.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace30.mp";
connectAttr "polyExtrudeFace29.out" "polyTweak40.ip";
connectAttr "polyTweak41.out" "polyExtrudeFace31.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace31.mp";
connectAttr "polyExtrudeFace30.out" "polyTweak41.ip";
connectAttr "polyTweak42.out" "polySplitRing18.ip";
connectAttr "pCubeShape5.wm" "polySplitRing18.mp";
connectAttr "polyCube4.out" "polyTweak42.ip";
connectAttr "polyTweak43.out" "polyExtrudeFace32.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace32.mp";
connectAttr "polySplitRing18.out" "polyTweak43.ip";
connectAttr "polyTweak44.out" "polyExtrudeFace33.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace33.mp";
connectAttr "polyExtrudeFace32.out" "polyTweak44.ip";
connectAttr "polyTweak45.out" "polyExtrudeFace34.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace34.mp";
connectAttr "polyExtrudeFace33.out" "polyTweak45.ip";
connectAttr "polyTweak46.out" "polyExtrudeFace35.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace35.mp";
connectAttr "polyExtrudeFace34.out" "polyTweak46.ip";
connectAttr "polyTweak47.out" "polyExtrudeFace36.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace36.mp";
connectAttr "polyExtrudeFace35.out" "polyTweak47.ip";
connectAttr "polyTweak48.out" "polyExtrudeFace37.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace37.mp";
connectAttr "polyExtrudeFace36.out" "polyTweak48.ip";
connectAttr "polyExtrudeFace37.out" "polyExtrudeFace38.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace38.mp";
connectAttr "polyTweak49.out" "polyExtrudeFace39.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace39.mp";
connectAttr "polyExtrudeFace38.out" "polyTweak49.ip";
connectAttr "polyTweak50.out" "polyExtrudeFace40.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace40.mp";
connectAttr "polyExtrudeFace39.out" "polyTweak50.ip";
connectAttr "polyExtrudeFace40.out" "polyExtrudeFace41.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace41.mp";
connectAttr "polyExtrudeFace41.out" "polyExtrudeFace42.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace42.mp";
connectAttr "polyExtrudeFace42.out" "polyExtrudeFace43.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace43.mp";
connectAttr "polyExtrudeFace43.out" "polyExtrudeFace44.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace44.mp";
connectAttr "polyTweak51.out" "polyExtrudeFace45.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace45.mp";
connectAttr "polyExtrudeFace44.out" "polyTweak51.ip";
connectAttr "polyTweak52.out" "polySplitRing19.ip";
connectAttr "pCubeShape5.wm" "polySplitRing19.mp";
connectAttr "polyExtrudeFace45.out" "polyTweak52.ip";
connectAttr "polyTweak53.out" "polyExtrudeFace46.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace46.mp";
connectAttr "polySplitRing19.out" "polyTweak53.ip";
connectAttr "polyTweak54.out" "polyExtrudeFace47.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace47.mp";
connectAttr "polyExtrudeFace46.out" "polyTweak54.ip";
connectAttr "polyTweak55.out" "polyExtrudeFace48.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace48.mp";
connectAttr "polyExtrudeFace47.out" "polyTweak55.ip";
connectAttr "polyExtrudeFace48.out" "polyExtrudeFace49.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace49.mp";
connectAttr "polyTweak56.out" "polyExtrudeFace50.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace50.mp";
connectAttr "polyExtrudeFace49.out" "polyTweak56.ip";
connectAttr "polyTweak57.out" "polyExtrudeFace51.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace51.mp";
connectAttr "polyExtrudeFace50.out" "polyTweak57.ip";
connectAttr "polyTweak58.out" "polyExtrudeFace52.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace52.mp";
connectAttr "polyExtrudeFace51.out" "polyTweak58.ip";
connectAttr "polyTweak59.out" "polyExtrudeFace53.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace53.mp";
connectAttr "polyExtrudeFace52.out" "polyTweak59.ip";
connectAttr "polyTweak60.out" "polyExtrudeFace54.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace54.mp";
connectAttr "polyExtrudeFace53.out" "polyTweak60.ip";
connectAttr "polyTweak61.out" "polyExtrudeFace55.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace55.mp";
connectAttr "polyExtrudeFace54.out" "polyTweak61.ip";
connectAttr "polyTweak62.out" "polyExtrudeFace56.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace56.mp";
connectAttr "polyExtrudeFace55.out" "polyTweak62.ip";
connectAttr "polyTweak63.out" "polyExtrudeFace57.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace57.mp";
connectAttr "polyExtrudeFace56.out" "polyTweak63.ip";
connectAttr "polyExtrudeFace57.out" "polyExtrudeFace58.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace58.mp";
connectAttr "polyTweak64.out" "polyExtrudeFace59.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace59.mp";
connectAttr "polyExtrudeFace58.out" "polyTweak64.ip";
connectAttr "polyTweak65.out" "polyExtrudeFace60.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace60.mp";
connectAttr "polyExtrudeFace59.out" "polyTweak65.ip";
connectAttr "polyExtrudeFace60.out" "polyExtrudeFace61.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace61.mp";
connectAttr "polyTweak66.out" "polyExtrudeFace62.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace62.mp";
connectAttr "polyExtrudeFace61.out" "polyTweak66.ip";
connectAttr "polyTweak67.out" "polyExtrudeFace63.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace63.mp";
connectAttr "polyExtrudeFace62.out" "polyTweak67.ip";
connectAttr "polyTweak68.out" "polyExtrudeFace64.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace64.mp";
connectAttr "polyExtrudeFace63.out" "polyTweak68.ip";
connectAttr "polyTweak69.out" "polyExtrudeFace65.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace65.mp";
connectAttr "polyExtrudeFace64.out" "polyTweak69.ip";
connectAttr "polyTweak70.out" "polyExtrudeFace66.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace66.mp";
connectAttr "polyExtrudeFace65.out" "polyTweak70.ip";
connectAttr "polyTweak71.out" "polyExtrudeFace67.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace67.mp";
connectAttr "polyExtrudeFace66.out" "polyTweak71.ip";
connectAttr "polyTweak72.out" "polyExtrudeFace68.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace68.mp";
connectAttr "polyExtrudeFace67.out" "polyTweak72.ip";
connectAttr "polyTweak73.out" "polyExtrudeFace69.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace69.mp";
connectAttr "polyExtrudeFace68.out" "polyTweak73.ip";
connectAttr "polyTweak74.out" "polySplitRing20.ip";
connectAttr "pCubeShape6.wm" "polySplitRing20.mp";
connectAttr "polyCube5.out" "polyTweak74.ip";
connectAttr "polyTweak75.out" "polySplitRing21.ip";
connectAttr "pCubeShape6.wm" "polySplitRing21.mp";
connectAttr "polySplitRing20.out" "polyTweak75.ip";
connectAttr "polyTweak76.out" "polyExtrudeFace71.ip";
connectAttr "pCubeShape6.wm" "polyExtrudeFace71.mp";
connectAttr "polySplitRing21.out" "polyTweak76.ip";
connectAttr "polyTweak77.out" "polySplitRing22.ip";
connectAttr "pCubeShape6.wm" "polySplitRing22.mp";
connectAttr "polyExtrudeFace71.out" "polyTweak77.ip";
connectAttr "polySplitRing22.out" "polySplitRing23.ip";
connectAttr "pCubeShape6.wm" "polySplitRing23.mp";
connectAttr "polySplitRing23.out" "polySplitRing24.ip";
connectAttr "pCubeShape6.wm" "polySplitRing24.mp";
connectAttr "polyTweak78.out" "polyExtrudeFace72.ip";
connectAttr "pCubeShape6.wm" "polyExtrudeFace72.mp";
connectAttr "polySplitRing24.out" "polyTweak78.ip";
connectAttr "pCubeShape4.o" "polyUnite1.ip[0]";
connectAttr "pCubeShape2.o" "polyUnite1.ip[1]";
connectAttr "pCubeShape3.o" "polyUnite1.ip[2]";
connectAttr "pCubeShape5.o" "polyUnite1.ip[3]";
connectAttr "pCubeShape1.o" "polyUnite1.ip[4]";
connectAttr "pCubeShape4.wm" "polyUnite1.im[0]";
connectAttr "pCubeShape2.wm" "polyUnite1.im[1]";
connectAttr "pCubeShape3.wm" "polyUnite1.im[2]";
connectAttr "pCubeShape5.wm" "polyUnite1.im[3]";
connectAttr "pCubeShape1.wm" "polyUnite1.im[4]";
connectAttr "polyExtrudeFace31.out" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "polySplitRing14.out" "groupParts2.ig";
connectAttr "groupId3.id" "groupParts2.gi";
connectAttr "polySplitRing16.out" "groupParts3.ig";
connectAttr "groupId5.id" "groupParts3.gi";
connectAttr "polyExtrudeFace69.out" "groupParts4.ig";
connectAttr "groupId7.id" "groupParts4.gi";
connectAttr "polySplitRing10.out" "groupParts5.ig";
connectAttr "groupId9.id" "groupParts5.gi";
connectAttr "polyUnite1.out" "groupParts6.ig";
connectAttr "groupId11.id" "groupParts6.gi";
connectAttr "groupParts6.og" "deleteComponent1.ig";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCubeShape6.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape4.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape4.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape2.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape2.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape3.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape3.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape5.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape5.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape1.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape1.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCube7Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId1.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId2.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId3.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId4.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId5.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId6.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId7.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId8.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId9.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId10.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId11.msg" ":initialShadingGroup.gn" -na;
// End of MountainSectionOne_model_003.ma
