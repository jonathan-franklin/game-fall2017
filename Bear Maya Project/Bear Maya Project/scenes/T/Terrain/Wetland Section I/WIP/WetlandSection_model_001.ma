//Maya ASCII 2017ff05 scene
//Name: WetlandSection_model_001.ma
//Last modified: Mon, Sep 18, 2017 07:35:49 PM
//Codeset: 1252
requires maya "2017ff05";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Home Premium Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "B5D95F40-4319-A643-7768-F3B2608DF03D";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -22.469323387135802 42.908176593630522 119.30823093674884 ;
	setAttr ".r" -type "double3" -20.138352729605419 -8.1999999999836515 4.0167597762808097e-016 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "5FD52221-482D-320C-E92D-8DBEFF888C09";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 131.06868748427627;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "9B11BBC6-434F-EA66-C6E4-A4A8666989FF";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "6E8A399F-4FD5-C6ED-B148-73981F5C5778";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "C29F6173-4968-90E1-72F3-C98FBEAEC264";
	setAttr ".t" -type "double3" 4.5834444733376483 -1.6372947034482803 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "20DC7EEB-4417-AD65-C650-39BCD8F7EA1D";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 4.7520800712055848;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "1699414A-4736-B0BB-C85B-43A90318EB26";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "98BCA90B-4F4F-C088-D004-178FBAA4A43A";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "imagePlane1";
	rename -uid "32D23EC5-4B41-7F13-482F-FCAB22D55898";
	setAttr ".t" -type "double3" 0 0 -10.548057019225356 ;
createNode imagePlane -n "imagePlaneShape1" -p "imagePlane1";
	rename -uid "87C9D667-44BB-0BAF-57B3-12BE2D3A8260";
	setAttr -k off ".v";
	setAttr ".fc" 204;
	setAttr ".imn" -type "string" "C:/Users/jonat/Git Repos/game-fall2017/Design/Level Design/IMG_20170918_141850.jpg";
	setAttr ".cov" -type "short2" 4032 3024 ;
	setAttr ".dlc" no;
	setAttr ".w" 40.32;
	setAttr ".h" 30.240000000000002;
	setAttr ".cs" -type "string" "sRGB";
createNode transform -n "pCube1";
	rename -uid "0D412293-48C6-5F17-8042-3A850E73075C";
	setAttr ".t" -type "double3" -18.721318222911126 -2.3081077261123331 0 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	rename -uid "7D5ED6F7-40A8-764E-025B-C18CC1EE8D21";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube2";
	rename -uid "CA2E0A99-4215-3D47-7198-8F9291565F49";
	setAttr ".t" -type "double3" -8.9164455593529741 -1.788188257782328 0 ;
	setAttr ".s" -type "double3" 1.0848556963281377 1.0848556963281377 1.0848556963281377 ;
createNode mesh -n "pCubeShape2" -p "pCube2";
	rename -uid "D2066612-4613-4F8F-7FD5-21978E699FA5";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube3";
	rename -uid "423F2E3C-41E2-3C94-1BA3-4E914D4E1F37";
	setAttr ".t" -type "double3" 2.7950496675498684 -1.788188257782328 0 ;
	setAttr ".s" -type "double3" 1.0848556963281377 1.0848556963281377 1.0848556963281377 ;
	setAttr ".rp" -type "double3" 10.807392337688665 0.52297910024429772 0 ;
	setAttr ".sp" -type "double3" 9.9620552062988281 0.48207250237464905 0 ;
	setAttr ".spt" -type "double3" 0.84533713138983679 0.040906597869648675 0 ;
createNode mesh -n "pCubeShape3" -p "pCube3";
	rename -uid "726A69F7-4A3B-AAF2-81B5-31961365A40C";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape1" -p "pCube3";
	rename -uid "F8031866-4DA6-DBF6-B611-E9BEA6BE6500";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 54 ".uvst[0].uvsp[0:53]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0 0.875 0 0.875 0.25 0.625 0.25 0.625 0 0.875
		 0 0.875 0.25 0.625 0.25 0.625 0 0.875 0 0.875 0.25 0.625 0.25 0.625 0 0.875 0 0.875
		 0.25 0.625 0.25 0.625 0 0.875 0 0.875 0.25 0.625 0.25 0.625 0 0.875 0 0.875 0.25
		 0.625 0.25 0.125 0 0.375 0 0.375 0.25 0.125 0.25 0.125 0 0.375 0 0.375 0.25 0.125
		 0.25 0.125 0 0.375 0 0.375 0.25 0.125 0.25 0.125 0 0.375 0 0.375 0.25 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 48 ".pt[0:47]" -type "float3"  -0.18184337 -0.14263169 0 
		0.29985744 -0.14030074 0 -0.18184337 0.0065959571 0 0.29985744 0.18111153 0 -0.18184337 
		0.0065959571 0 0.29985744 0.18111153 0 -0.18184337 -0.14263169 0 0.29985744 -0.14030074 
		0 1.4457011 -0.14624743 0 1.4457011 -0.14624743 0 1.4457011 0.078229479 0 1.4457011 
		0.078229479 0 2.480531 -0.021500174 0 2.480531 -0.021500174 0 2.480531 0.021500174 
		0 2.480531 0.021500174 0 3.4102588 0.050035238 0 3.4102588 0.050035238 0 3.4102588 
		0.22203654 0 3.4102588 0.22203654 0 4.0817628 0.23195401 0 4.0817628 0.23195401 0 
		4.0817628 0.38020748 0 4.0817628 0.38020748 0 4.4327822 0.37420517 0 4.4327822 0.37420517 
		0 4.4327822 0.45561379 0 4.4327822 0.45561379 0 4.4383559 0.044831749 0 4.4383559 
		0.044831749 0 4.4383559 0.51255774 0 4.4383559 0.51255774 0 -0.66212547 -0.1251218 
		0 -0.66212547 -0.1251218 0 -0.66212547 -0.078932106 0 -0.66212547 -0.078932106 0 
		-0.9385528 -0.13915728 0 -0.9385528 -0.13915728 0 -0.9385528 -0.010482259 0 -0.9385528 
		-0.010482259 0 -0.96503913 0.032591499 0 -0.96503913 0.032591499 0 -0.96503913 0.049030047 
		0 -0.96503913 0.049030047 0 -0.93895787 0.1919113 0 -0.93895787 0.1919113 0 -0.93895787 
		-0.014256667 0 -0.93895787 -0.014256667 0;
	setAttr -s 48 ".vt[0:47]"  -0.23391247 -0.46428752 0.5 0.73333216 -0.5 0.5
		 -0.23391247 0.46428752 0.5 0.73333216 0.5 0.5 -0.23391247 0.46428752 -0.5 0.73333216 0.5 -0.5
		 -0.23391247 -0.46428752 -0.5 0.73333216 -0.5 -0.5 1.88489723 -0.48110604 -0.5 1.88489723 -0.48110604 0.5
		 1.88489723 0.66174376 -0.5 1.88489723 0.66174376 0.5 2.96872044 -0.5117166 -0.5 2.96872044 -0.5117166 0.5
		 2.96872044 0.69235438 -0.5 2.96872044 0.69235438 0.5 4.097705841 -0.5117166 -0.5
		 4.097705841 -0.5117166 0.5 4.097705841 0.69235438 -0.5 4.097705841 0.69235438 0.5
		 5.023474693 -0.47375464 -0.5 5.023474693 -0.47375464 0.5 5.023474693 0.56407368 -0.5
		 5.023474693 0.56407368 0.5 5.36217213 -0.33476686 -0.5 5.36217213 -0.33476686 0.5
		 5.36217213 0.4250859 -0.5 5.36217213 0.4250859 0.5 5.52864647 0.075933933 -0.5 5.52864647 0.075933933 0.5
		 5.52864647 0.34733486 -0.5 5.52864647 0.34733486 0.5 -0.76801586 -0.4588716 -0.5
		 -0.76801586 -0.4588716 0.5 -0.76801586 0.40338016 0.5 -0.76801586 0.40338016 -0.5
		 -1.06628418 -0.31877875 -0.5 -1.06628418 -0.31877875 0.5 -1.06628418 0.2355417 0.5
		 -1.06628418 0.2355417 -0.5 -1.21194935 -0.23667169 -0.5 -1.21194935 -0.23667169 0.5
		 -1.21194935 0.070196867 0.5 -1.21194935 0.070196867 -0.5 -1.31599522 -0.29216313 -0.5
		 -1.31599522 -0.29216313 0.5 -1.31599522 0.0147053 0.5 -1.31599522 0.0147053 -0.5;
	setAttr -s 92 ".ed[0:91]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 1 1 3 1 2 4 1
		 3 5 1 4 6 1 5 7 1 6 0 1 7 1 1 7 8 0 1 9 0 8 9 1 5 10 0 10 8 1 3 11 0 11 10 1 9 11 1
		 8 12 0 9 13 0 12 13 1 10 14 0 14 12 1 11 15 0 15 14 1 13 15 1 12 16 0 13 17 0 16 17 1
		 14 18 0 18 16 1 15 19 0 19 18 1 17 19 1 16 20 0 17 21 0 20 21 1 18 22 0 22 20 1 19 23 0
		 23 22 1 21 23 1 20 24 0 21 25 0 24 25 1 22 26 0 26 24 1 23 27 0 27 26 0 25 27 1 24 28 0
		 25 29 0 28 29 0 26 30 0 30 28 0 27 31 0 31 30 0 29 31 0 6 32 0 0 33 0 32 33 1 2 34 0
		 33 34 1 4 35 0 34 35 1 35 32 1 32 36 0 33 37 0 36 37 0 34 38 0 37 38 1 35 39 0 38 39 1
		 39 36 1 36 40 0 37 41 0 40 41 0 38 42 0 41 42 1 39 43 0 42 43 1 43 40 1 40 44 0 41 45 0
		 44 45 0 42 46 0 45 46 0 43 47 0 46 47 0 47 44 0;
	setAttr -s 46 -ch 184 ".fc[0:45]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -55 -57 -59 -60
		mu 0 4 34 35 36 37
		f 4 86 88 90 91
		mu 0 4 50 51 52 53
		f 4 -12 12 14 -14
		mu 0 4 1 10 15 14
		f 4 -10 15 16 -13
		mu 0 4 10 11 16 15
		f 4 -8 17 18 -16
		mu 0 4 11 3 17 16
		f 4 -6 13 19 -18
		mu 0 4 3 1 14 17
		f 4 -15 20 22 -22
		mu 0 4 14 15 19 18
		f 4 -17 23 24 -21
		mu 0 4 15 16 20 19
		f 4 -19 25 26 -24
		mu 0 4 16 17 21 20
		f 4 -20 21 27 -26
		mu 0 4 17 14 18 21
		f 4 -23 28 30 -30
		mu 0 4 18 19 23 22
		f 4 -25 31 32 -29
		mu 0 4 19 20 24 23
		f 4 -27 33 34 -32
		mu 0 4 20 21 25 24
		f 4 -28 29 35 -34
		mu 0 4 21 18 22 25
		f 4 -31 36 38 -38
		mu 0 4 22 23 27 26
		f 4 -33 39 40 -37
		mu 0 4 23 24 28 27
		f 4 -35 41 42 -40
		mu 0 4 24 25 29 28
		f 4 -36 37 43 -42
		mu 0 4 25 22 26 29
		f 4 -39 44 46 -46
		mu 0 4 26 27 31 30
		f 4 -41 47 48 -45
		mu 0 4 27 28 32 31
		f 4 -43 49 50 -48
		mu 0 4 28 29 33 32
		f 4 -44 45 51 -50
		mu 0 4 29 26 30 33
		f 4 -47 52 54 -54
		mu 0 4 30 31 35 34
		f 4 -49 55 56 -53
		mu 0 4 31 32 36 35
		f 4 -51 57 58 -56
		mu 0 4 32 33 37 36
		f 4 -52 53 59 -58
		mu 0 4 33 30 34 37
		f 4 10 61 -63 -61
		mu 0 4 12 0 39 38
		f 4 4 63 -65 -62
		mu 0 4 0 2 40 39
		f 4 6 65 -67 -64
		mu 0 4 2 13 41 40
		f 4 8 60 -68 -66
		mu 0 4 13 12 38 41
		f 4 62 69 -71 -69
		mu 0 4 38 39 43 42
		f 4 64 71 -73 -70
		mu 0 4 39 40 44 43
		f 4 66 73 -75 -72
		mu 0 4 40 41 45 44
		f 4 67 68 -76 -74
		mu 0 4 41 38 42 45
		f 4 70 77 -79 -77
		mu 0 4 42 43 47 46
		f 4 72 79 -81 -78
		mu 0 4 43 44 48 47
		f 4 74 81 -83 -80
		mu 0 4 44 45 49 48
		f 4 75 76 -84 -82
		mu 0 4 45 42 46 49
		f 4 78 85 -87 -85
		mu 0 4 46 47 51 50
		f 4 80 87 -89 -86
		mu 0 4 47 48 52 51
		f 4 82 89 -91 -88
		mu 0 4 48 49 53 52
		f 4 83 84 -92 -90
		mu 0 4 49 46 50 53;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "CB62F563-40B1-1361-E686-58A171529560";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "F9689658-4EE1-03A5-4566-49AEB056519F";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "718A8018-4E29-15B7-F6BE-54AB242E6EFF";
createNode displayLayerManager -n "layerManager";
	rename -uid "F3262905-4693-113D-70A0-80BB4155DBC4";
createNode displayLayer -n "defaultLayer";
	rename -uid "09032832-4A18-9A21-3C70-1DA3DD019B83";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "7014DBB8-42E2-470A-AEB8-F58F6DB77C76";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "E7149BFD-4428-5245-5B19-7BBE67BBDCDF";
	setAttr ".g" yes;
createNode polyCube -n "polyCube1";
	rename -uid "35ED078B-4934-0677-F154-E6B8BF295066";
	setAttr ".cuv" 4;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "A7386460-4924-C09F-CA8E-43A0A365F5A7";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -18.221317 -2.3081076 0 ;
	setAttr ".rs" 63089;
	setAttr ".lt" -type "double3" 1.9568380943005762e-016 -0.28428436491343445 0.88128153123164665 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -18.221318222911126 -3.1914409322890909 -0.5 ;
	setAttr ".cbx" -type "double3" -18.221318222911126 -1.4247745199355752 0.5 ;
createNode polyTweak -n "polyTweak1";
	rename -uid "F9975156-458B-BC27-3F03-588D03C2FC55";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  0 -0.38333324 0 0 -0.38333324
		 0 0 0.38333324 0 0 0.38333324 0 0 0.38333324 0 0 0.38333324 0 0 -0.38333324 0 0 -0.38333324
		 0;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "51000191-416A-BC32-28FF-73A03E1FE8D6";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -17.340034 -2.0238233 0 ;
	setAttr ".rs" 59429;
	setAttr ".lt" -type "double3" 2.6511999987298142e-016 0.056856872982687413 1.1939943326364251 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -17.340035416514642 -2.9071563406142862 -0.5 ;
	setAttr ".cbx" -type "double3" -17.340035416514642 -1.1404901666793497 0.5 ;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "6AA2D890-4DD7-97CB-09C7-8980ACBB16E8";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -16.259756 -1.9953949 0 ;
	setAttr ".rs" 62166;
	setAttr ".lt" -type "double3" 2.3987047607555505e-016 -0.085285309474030541 1.0802805866710532 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -16.259756543071038 -2.7998631520423563 -0.5 ;
	setAttr ".cbx" -type "double3" -16.259756543071038 -1.1909267826232584 0.5 ;
createNode polyTweak -n "polyTweak2";
	rename -uid "F4030EFE-406D-2726-1B08-34884892AB7D";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[12:15]" -type "float3"  -0.11371375 0.16415031 0 -0.11371375
		 0.16415031 0 -0.11371375 0.0064203031 0 -0.11371375 0.0064203031 0;
createNode polyExtrudeFace -n "polyExtrudeFace4";
	rename -uid "6CFFFB34-44FF-ACB7-D4DB-1391FBE914F5";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -15.12262 -1.9101098 0 ;
	setAttr ".rs" 57309;
	setAttr ".lt" -type "double3" 1.7043428563263086e-016 9.399994313549642e-017 0.76756778526627301 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -15.122619845301873 -2.5709302945289285 -0.5 ;
	setAttr ".cbx" -type "double3" -15.122619845301873 -1.2492891473922403 0.5 ;
createNode polyTweak -n "polyTweak3";
	rename -uid "DD53CE60-485D-B0BC-8AD6-FBB6BDFCCB9E";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[16:19]" -type "float3"  0.056856874 0.14364773 0 0.056856874
		 0.14364773 0 0.056856874 -0.14364773 0 0.056856874 -0.14364773 0;
createNode polyExtrudeFace -n "polyExtrudeFace5";
	rename -uid "55C4847A-4C61-EC39-9B34-00A8B920E49B";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -14.355051 -1.9101098 0 ;
	setAttr ".rs" 42841;
	setAttr ".lt" -type "double3" -2.0117335518960134e-017 -6.5503846275832776e-016 
		0.090600424746883235 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -14.355051018626458 -2.4647324605140359 -0.5 ;
	setAttr ".cbx" -type "double3" -14.355051018626458 -1.3554870410117776 0.5 ;
createNode polyTweak -n "polyTweak4";
	rename -uid "B163DC92-46A0-9B97-45EF-E28F9D5D65E3";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[20:23]" -type "float3"  8.8817842e-016 0.10619789
		 0 8.8817842e-016 0.10619789 0 8.8817842e-016 -0.10619789 0 8.8817842e-016 -0.10619789
		 0;
createNode polyExtrudeFace -n "polyExtrudeFace6";
	rename -uid "206E7455-4232-7C50-EB11-888542E906BD";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -18.85722 -3.1914408 0 ;
	setAttr ".rs" 50826;
	setAttr ".lt" -type "double3" 0 -1.4791141972893971e-031 0.76363215143799046 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -19.583719827563531 -3.1914409322890909 -0.5 ;
	setAttr ".cbx" -type "double3" -18.130717791945703 -3.1914409322890909 0.5 ;
createNode polyTweak -n "polyTweak5";
	rename -uid "C47A9984-42AC-E129-BC8F-8AA90E1F1BB5";
	setAttr ".uopa" yes;
	setAttr -s 17 ".tk";
	setAttr ".tk[0]" -type "float3" -0.3624016 0 0 ;
	setAttr ".tk[1]" -type "float3" 0.090600424 0 0 ;
	setAttr ".tk[2]" -type "float3" -0.3624016 0 0 ;
	setAttr ".tk[3]" -type "float3" -0.051771671 0.10354334 0 ;
	setAttr ".tk[4]" -type "float3" -0.3624016 0 0 ;
	setAttr ".tk[5]" -type "float3" -0.051771671 0.10354334 0 ;
	setAttr ".tk[6]" -type "float3" -0.3624016 0 0 ;
	setAttr ".tk[7]" -type "float3" 0.090600424 0 0 ;
	setAttr ".tk[10]" -type "float3" -0.10354334 -0.051771671 0 ;
	setAttr ".tk[11]" -type "float3" -0.10354334 -0.051771671 0 ;
	setAttr ".tk[14]" -type "float3" -0.025885835 -0.051771671 0 ;
	setAttr ".tk[15]" -type "float3" -0.025885835 -0.051771671 0 ;
	setAttr ".tk[24]" -type "float3" -1.7763568e-015 0.17826253 0 ;
	setAttr ".tk[25]" -type "float3" -1.7763568e-015 0.17826253 0 ;
	setAttr ".tk[26]" -type "float3" -1.7763568e-015 -0.17826253 0 ;
	setAttr ".tk[27]" -type "float3" -1.7763568e-015 -0.17826253 0 ;
createNode polyExtrudeFace -n "polyExtrudeFace7";
	rename -uid "41A92059-4CDB-F1EA-1256-65A4C1D663B9";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -19.025476 -3.9550731 0 ;
	setAttr ".rs" 53286;
	setAttr ".lt" -type "double3" -0.051771671283930232 -2.7117093616972281e-031 0.90600424746880304 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -19.609279729754693 -3.95507322980496 -0.5 ;
	setAttr ".cbx" -type "double3" -18.441673495204217 -3.95507322980496 0.5 ;
createNode polyTweak -n "polyTweak6";
	rename -uid "93B83FAE-461D-1976-5A11-23B0D71F5BBC";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[28:31]" -type "float3"  -0.025559574 2.220446e-016
		 0 -0.31095624 2.220446e-016 0 -0.31095624 2.220446e-016 0 -0.025559574 2.220446e-016
		 0;
createNode polyExtrudeFace -n "polyExtrudeFace8";
	rename -uid "41539707-45D1-45E1-FFCD-29820A94162A";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -19.141966 -4.8610773 0 ;
	setAttr ".rs" 48133;
	setAttr ".lt" -type "double3" 0 3.4512664603419266e-031 1.2684059464563227 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -19.642372049720056 -4.8610774202499059 -0.5 ;
	setAttr ".cbx" -type "double3" -18.64155805888868 -4.8610774202499059 0.5 ;
createNode polyTweak -n "polyTweak7";
	rename -uid "75846CDA-4477-458C-D7F4-46B212247DEF";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[32:35]" -type "float3"  0.018681608 2.6645353e-015
		 0 -0.14811078 2.6645353e-015 0 -0.14811078 2.6645353e-015 0 0.018681608 2.6645353e-015
		 0;
createNode polyExtrudeFace -n "polyExtrudeFace9";
	rename -uid "CA9363C6-4EDF-137F-534E-8CAAEDCEDCE3";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -19.219622 -6.1294832 0 ;
	setAttr ".rs" 42016;
	setAttr ".lt" -type "double3" 0 9.8607613152626476e-032 1.0354334256786295 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -19.630674936206109 -6.1294833345565465 -0.5 ;
	setAttr ".cbx" -type "double3" -18.808569826514489 -6.1294833345565465 0.5 ;
createNode polyTweak -n "polyTweak8";
	rename -uid "F57E9A5F-4123-F1C7-E7DE-31929240E5AF";
	setAttr ".uopa" yes;
	setAttr -s 5 ".tk";
	setAttr ".tk[36]" -type "float3" 0.01169619 4.4408921e-016 0 ;
	setAttr ".tk[37]" -type "float3" -0.1670112 4.4408921e-016 0 ;
	setAttr ".tk[38]" -type "float3" -0.1670112 4.4408921e-016 0 ;
	setAttr ".tk[39]" -type "float3" 0.01169619 4.4408921e-016 0 ;
createNode polyExtrudeFace -n "polyExtrudeFace10";
	rename -uid "8EDD3150-45CF-BC24-4D01-53A98D02DE3A";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -19.245508 -7.1649175 0 ;
	setAttr ".rs" 60631;
	setAttr ".lt" -type "double3" 0 -9.8607613152626476e-032 1.6217525572030702 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -19.634541549117333 -7.1649173422011998 -0.5 ;
	setAttr ".cbx" -type "double3" -18.856475271613366 -7.1649173422011998 0.5 ;
createNode polyTweak -n "polyTweak9";
	rename -uid "FD935EC7-4636-7F26-4E9E-58AFEFCC8388";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[40:43]" -type "float3"  -0.0038662776 0 0 -0.047905393
		 0 0 -0.047905393 0 0 -0.0038662776 0 0;
createNode polyExtrudeFace -n "polyExtrudeFace11";
	rename -uid "E4EB6D5D-4F94-B71F-6ABF-3E8446228C1B";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -19.245508 -8.7866697 0 ;
	setAttr ".rs" 39094;
	setAttr ".lt" -type "double3" 0 -4.6838616247497576e-031 0.9730515343218471 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -19.613701500327355 -8.7866700811538365 -0.5 ;
	setAttr ".cbx" -type "double3" -18.877314858467347 -8.7866700811538365 0.5 ;
createNode polyTweak -n "polyTweak10";
	rename -uid "B8623AA5-4D3E-23A1-8F3D-5696DFC69620";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[44:47]" -type "float3"  0.020840008 2.6645353e-015
		 0 -0.020840008 2.6645353e-015 0 -0.020840008 2.6645353e-015 0 0.020840008 2.6645353e-015
		 0;
createNode polyExtrudeFace -n "polyExtrudeFace12";
	rename -uid "FCDF7AE8-4147-2F2F-D2DD-638A19508920";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -19.126011 -9.7597208 0 ;
	setAttr ".rs" 34733;
	setAttr ".lt" -type "double3" 0 -4.9303806576313238e-032 0.80234073882678203 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -19.619120695025689 -9.7597211523208287 -0.5 ;
	setAttr ".cbx" -type "double3" -18.632900558740861 -9.7597211523208287 0.5 ;
createNode polyTweak -n "polyTweak11";
	rename -uid "7C33641D-4A24-3444-1B86-5E9F8E728ACD";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[48:51]" -type "float3"  -0.0054189079 -8.8817842e-016
		 0 0.24441399 -8.8817842e-016 0 0.24441399 -8.8817842e-016 0 -0.0054189079 -8.8817842e-016
		 0;
createNode polyExtrudeFace -n "polyExtrudeFace13";
	rename -uid "595BA946-438B-0E24-CFA0-F1BDED05B48B";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -18.852873 -10.954698 0 ;
	setAttr ".rs" 49193;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -19.694943167598016 -11.79460369779202 -0.5 ;
	setAttr ".cbx" -type "double3" -18.010804213912255 -10.114791743293729 0.5 ;
createNode polyTweak -n "polyTweak12";
	rename -uid "9B1C4D16-4353-FD89-A53A-44B3B6C40553";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[52:55]" -type "float3"  -0.075822577 -1.23254132 0
		 0.62209696 0.44727093 0 0.62209696 0.44727093 0 -0.075822577 -1.23254132 0;
createNode polyExtrudeFace -n "polyExtrudeFace14";
	rename -uid "19CC6745-477C-F575-EAB8-1BBF52CF04AD";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -18.545595 -11.94482 0 ;
	setAttr ".rs" 54787;
	setAttr ".lt" -type "double3" 0.70945304652882568 -2.1227030239597542e-016 1.2688841383003688 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -19.754184343249566 -13.45486866666409 -0.5 ;
	setAttr ".cbx" -type "double3" -17.337004877955682 -10.434772841468778 0.5 ;
createNode polyTweak -n "polyTweak13";
	rename -uid "85F0F433-4F4D-7CEF-B85D-3393FD2AFFD6";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[56:59]" -type "float3"  -0.059240945 -1.66026545 0
		 0.67379934 -0.31998017 0 0.67379934 -0.31998017 0 -0.059240945 -1.66026545 0;
createNode polyExtrudeFace -n "polyExtrudeFace15";
	rename -uid "ACCBCE0C-48EE-118A-6AC5-5BAC963360D3";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -16.975054 -12.09846 0 ;
	setAttr ".rs" 62268;
	setAttr ".lt" -type "double3" 0.16567729337858839 -5.1589759253997277e-017 2.3188289108418489 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -17.148976303965814 -13.508186690345731 -0.5 ;
	setAttr ".cbx" -type "double3" -16.801131941706902 -10.688733450904813 0.5 ;
createNode polyTweak -n "polyTweak14";
	rename -uid "23684909-4400-E180-A037-6DAE6213B5CC";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[60:63]" -type "float3"  1.17123604 0.18567637 0 -0.89809877
		 -0.01496556 0 -0.89809877 -0.01496556 0 1.17123604 0.18567637 0;
createNode polyExtrudeFace -n "polyExtrudeFace16";
	rename -uid "A2E684BA-425B-729D-87E8-76A035804036";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -14.602174 -12.337455 0 ;
	setAttr ".rs" 58419;
	setAttr ".lt" -type "double3" 0.27180605914953176 -5.5720954378943488e-016 3.3349169561360816 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -14.685227372081048 -13.424980513587919 -0.5 ;
	setAttr ".cbx" -type "double3" -14.519120194346673 -11.249928824439969 0.5 ;
createNode polyTweak -n "polyTweak15";
	rename -uid "D43128B9-45C5-ED09-748D-E4B9EF39670D";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[64:67]" -type "float3"  0.14208215 0.2027034 0 -0.039655633
		 -0.44169843 0 -0.039655633 -0.44169843 0 0.14208215 0.2027034 0;
createNode polyExtrudeFace -n "polyExtrudeFace17";
	rename -uid "1CCB9962-4DB4-F78A-65B2-AE91143FEF1B";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -11.256242 -12.320384 0 ;
	setAttr ".rs" 64444;
	setAttr ".lt" -type "double3" 0.085341706085698993 1.5756391970261993e-015 4.6898190418047712 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -11.272637345225579 -13.235648505226102 -0.5 ;
	setAttr ".cbx" -type "double3" -11.239846207530267 -11.405119292274442 0.5 ;
createNode polyTweak -n "polyTweak16";
	rename -uid "FB48345F-405D-6395-CE3D-CAB238BB5CC8";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[68:71]" -type "float3"  0.066658229 0.17226095 0 -0.066658229
		 -0.17226095 0 -0.066658229 -0.17226095 0 0.066658229 0.17226095 0;
createNode polyExtrudeFace -n "polyExtrudeFace18";
	rename -uid "1E875BC4-482F-1099-31D9-4888FC1050FD";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -6.5656462 -12.319053 0 ;
	setAttr ".rs" 62243;
	setAttr ".lt" -type "double3" 0.065881182083013939 -6.7251087220427951e-016 5.6490094362718173 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -6.5820417183945246 -13.234317175880399 -0.5 ;
	setAttr ".cbx" -type "double3" -6.5492505806992121 -11.403787962928739 0.5 ;
createNode polyExtrudeFace -n "polyExtrudeFace19";
	rename -uid "EA6B34DD-44DA-2555-DCA7-8985DB033367";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.91636372 -12.35436 0 ;
	setAttr ".rs" 43403;
	setAttr ".lt" -type "double3" 0.22852888223822843 -1.1251741764053527e-015 6.8934879199210579 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.93142602625585269 -13.195180289283719 -0.48192080855369568 ;
	setAttr ".cbx" -type "double3" -0.90130136194921207 -11.513538710609403 0.48192080855369568 ;
createNode polyTweak -n "polyTweak17";
	rename -uid "68D4F6C6-487C-E367-7A52-AC82168AD08F";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[76:79]" -type "float3"  0.0013336119 0.074443601 0.018079195
		 -0.0013336119 -0.074443601 0.018079195 -0.0013336119 -0.074443601 -0.018079195 0.0013336119
		 0.074443601 -0.018079195;
createNode polyExtrudeFace -n "polyExtrudeFace20";
	rename -uid "5B76631C-4BB8-05FE-668C-04BD2F825294";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 6.120141 -12.074299 0 ;
	setAttr ".rs" 39038;
	setAttr ".lt" -type "double3" 8.3266726846886531e-017 2.3735100074432444e-015 8.6123418678838384 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 6.1031961661269598 -13.020217291847196 -0.48192080855369568 ;
	setAttr ".cbx" -type "double3" 6.1370859366347723 -11.12838112546536 0.48192080855369568 ;
createNode polyTweak -n "polyTweak18";
	rename -uid "A287FC3E-4C77-DF01-8099-1BBB1C3FB568";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[80:83]" -type "float3"  0.13814726 0.06994018 0 0.14191264
		 0.28013474 0 0.14191264 0.28013474 0 0.13814726 0.06994018 0;
createNode polyExtrudeFace -n "polyExtrudeFace21";
	rename -uid "5107DF7D-430C-4511-7DA9-DDA149A4DB78";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 14.555783 -12.116984 0 ;
	setAttr ".rs" 39399;
	setAttr ".lt" -type "double3" 0.25364653220913264 -3.7867286039290142e-016 1.7011205382244099 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 14.537627242176764 -13.130464903846708 -0.48192080855369568 ;
	setAttr ".cbx" -type "double3" 14.573939345448249 -11.103504530921903 0.48192080855369568 ;
createNode polyTweak -n "polyTweak19";
	rename -uid "B3CBDE76-44C1-2F57-06F2-DE883BAAA108";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[84:87]" -type "float3"  -0.17653088 0.044005506 0
		 -0.17411031 0.17912978 0 -0.17411031 0.17912978 0 -0.17653088 0.044005506 0;
createNode polyExtrudeFace -n "polyExtrudeFace22";
	rename -uid "48879C57-4621-7DCD-3418-21A485C49E97";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 16.261175 -11.893847 0 ;
	setAttr ".rs" 56324;
	setAttr ".lt" -type "double3" -0.16339778567627583 2.7755949580118292e-016 1.885663031889901 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 15.875933669178718 -13.00553070737454 -0.48192080855369568 ;
	setAttr ".cbx" -type "double3" 16.646414778797858 -10.782164923683133 0.48192080855369568 ;
createNode polyTweak -n "polyTweak20";
	rename -uid "284F4FCE-41F6-8370-4063-A190113973EB";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[88:91]" -type "float3"  0.40339693 -0.098202854 0
		 -0.40339693 0.098202854 0 -0.40339693 0.098202854 0 0.40339693 -0.098202854 0;
createNode polyExtrudeFace -n "polyExtrudeFace23";
	rename -uid "100982C4-4E4B-C82C-E638-839D30105E18";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 17.771263 -11.575052 0 ;
	setAttr ".rs" 40405;
	setAttr ".lt" -type "double3" -0.081046452754276244 1.1922018611594426e-016 1.4245808724854805 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 16.673544905750983 -12.983620039955106 -0.48192080855369568 ;
	setAttr ".cbx" -type "double3" 18.868979476063483 -10.166484229103055 0.48192080855369568 ;
createNode polyTweak -n "polyTweak21";
	rename -uid "74535F22-46A0-C503-6966-7CB11767C69B";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[92:95]" -type "float3"  0.49435279 -0.74991149 0 -0.9306007
		 -0.15614119 0 -0.9306007 -0.15614119 0 0.49435279 -0.74991149 0;
createNode polyExtrudeFace -n "polyExtrudeFace24";
	rename -uid "549D4068-4DEC-4BCE-2EC7-DD8A1F6A558C";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 19.080006 -10.887124 0 ;
	setAttr ".rs" 56288;
	setAttr ".lt" -type "double3" -0.77433999249801633 -2.7737807085560542e-006 1.0070288530182216 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 17.308371565907233 -12.980484358802762 -0.48192080855369568 ;
	setAttr ".cbx" -type "double3" 20.851637862293952 -8.7937635107192662 0.48192080855369568 ;
createNode polyTweak -n "polyTweak22";
	rename -uid "9AF747E4-47F8-4C46-6984-40BAB5218341";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[96:99]" -type "float3"  0.90881783 -0.93647391 -3.7252903e-009
		 -0.43901265 0.43311131 -3.7252903e-009 -0.43901253 0.43311143 3.7252903e-009 0.90881795
		 -0.93647379 3.7252903e-009;
createNode polyExtrudeFace -n "polyExtrudeFace25";
	rename -uid "08CF3744-48B9-FE33-6D09-9782E09018F7";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 19.163898 -8.2206459 0 ;
	setAttr ".rs" 45123;
	setAttr ".lt" -type "double3" 3.5264486891833386e-015 5.6174378028957944e-017 1.9975935480543221 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 17.450213454334968 -8.2786992712173131 -0.48192080855369568 ;
	setAttr ".cbx" -type "double3" 20.877581618397468 -8.1625918073806432 0.48192080855369568 ;
createNode polyTweak -n "polyTweak23";
	rename -uid "BCC4C11E-4A19-0DF4-7861-448D593790A0";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[100:103]" -type "float3"  -0.2425162 3.46015835 0 -0.12661719
		 -0.61045784 0 -0.12661803 -0.61045593 0 -0.2425162 3.46015835 0;
createNode polyExtrudeFace -n "polyExtrudeFace26";
	rename -uid "DC541D96-43D3-F2D8-E677-D4AC5012D6BB";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 19.231529 -6.2241979 -1.1324883e-006 ;
	setAttr ".rs" 44756;
	setAttr ".lt" -type "double3" -0.043888142345518188 -2.5451588021847583e-006 2.2078088136225924 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 17.519637129872077 -6.3217033071670201 -0.48192194104194641 ;
	setAttr ".cbx" -type "double3" 20.943419478504889 -6.1266926450881627 0.48191967606544495 ;
createNode polyTweak -n "polyTweak24";
	rename -uid "90E7EB65-4D0B-3B5C-9BCD-E9B371DD24A4";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[104:107]" -type "float3"  -0.0017914885 0.15555781 0
		 0.0017916616 -0.15555781 0 0.0017914705 -0.15555745 0 -0.0017914885 0.15555781 0;
createNode polyExtrudeFace -n "polyExtrudeFace27";
	rename -uid "9AB2E530-4431-328C-A061-FF83C2310B7C";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 19.062164 -4.0224576 1.1920929e-007 ;
	setAttr ".rs" 55111;
	setAttr ".lt" -type "double3" 4.3373968536242767e-015 -1.1338404207769746e-016 1.5039293841603656 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 17.197432540028327 -4.1286681814345982 -0.48192068934440613 ;
	setAttr ".cbx" -type "double3" 20.926894209950202 -3.9162472410354288 0.48192092776298523 ;
createNode polyTweak -n "polyTweak25";
	rename -uid "02EBFB6B-4E08-833F-E8FB-7894F5FBD608";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[108:111]" -type "float3"  0.15283963 0.0087053683 0
		 -0.15283963 -0.0087053683 0 -0.1528393 -0.0087052016 0 0.15283963 0.0087053683 0;
createNode polyExtrudeFace -n "polyExtrudeFace28";
	rename -uid "3DD6BF83-4C18-E08F-7CA7-3A8D6E6109EF";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 18.976643 -2.5209618 -1.0728836e-006 ;
	setAttr ".rs" 38322;
	setAttr ".lt" -type "double3" 5.0856442399613351e-015 8.3688401023853613e-016 1.0263168175737074 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 16.912127516834968 -2.6385511977824838 -0.48192188143730164 ;
	setAttr ".cbx" -type "double3" 21.041155837147468 -2.4033723024878175 0.48191973567008972 ;
createNode polyTweak -n "polyTweak26";
	rename -uid "BF00C6C5-461A-0DBC-D91C-8DA2FD5A31E1";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[112:115]" -type "float3"  0.19978243 0.011379099 0 -0.19978243
		 -0.0113791 0 -0.19978198 -0.011378896 0 0.19978243 0.011379099 0;
createNode polyExtrudeFace -n "polyExtrudeFace29";
	rename -uid "74F9531D-48D2-4182-4AE0-7DB131219654";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 18.918282 -1.4963055 -2.0265579e-006 ;
	setAttr ".rs" 58876;
	setAttr ".lt" -type "double3" 2.990251724719891e-015 -3.6350683629214316e-017 0.2645832288932991 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 16.85086347874903 -1.5391557736549051 -0.48192283511161804 ;
	setAttr ".cbx" -type "double3" 20.985697768299811 -1.4534551425132425 0.48191878199577332 ;
createNode polyTweak -n "polyTweak27";
	rename -uid "4064ED21-461B-680A-513C-2A9AD1C1A8EE";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[116:119]" -type "float3"  0.002902061 -0.074739054 0
		 -0.002902061 0.074739054 0 -0.0029019858 0.07473892 0 0.002902061 -0.074739054 0;
createNode polyExtrudeFace -n "polyExtrudeFace30";
	rename -uid "6886B8BB-48A2-19AF-E351-DC8625C77600";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 19.178946 -1.2317791 -2.3245811e-006 ;
	setAttr ".rs" 51192;
	setAttr ".lt" -type "double3" 0.52052541788982121 -2.1876926591276765e-007 0.22452144985392378 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 17.148444197743171 -1.2738642616424234 -0.48192313313484192 ;
	setAttr ".cbx" -type "double3" 21.20944502171778 -1.1896939201507242 0.48191848397254944 ;
createNode polyTweak -n "polyTweak28";
	rename -uid "163FC52F-4342-914C-A031-A69C5FC766FE";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[120:123]" -type "float3"  0.22922826 -0.00076514907
		 0 0.30306074 0.00076514907 0 0.30306059 0.00076510676 0 0.22922826 -0.00076514907
		 0;
createNode polyExtrudeFace -n "polyExtrudeFace31";
	rename -uid "461DBC56-46B1-F89D-0551-1BBAB4F01134";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18.721318222911126 -2.3081077261123331 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 19.694708 -0.99651957 -2.3841858e-006 ;
	setAttr ".rs" 53495;
	setAttr ".lt" -type "double3" 0.42837293147699501 -1.4126366584944867e-007 0.14497796541860714 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 17.664206526844733 -1.0386047287139566 -0.48192319273948669 ;
	setAttr ".cbx" -type "double3" 21.725207350819343 -0.95443438722225737 0.48191842436790466 ;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "33CCC296-4670-5C4A-866A-2CB02FBB13CD";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n"
		+ "            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 672\n            -height 332\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n"
		+ "            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 671\n            -height 332\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 672\n            -height 332\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 671\n            -height 332\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n"
		+ "            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n"
		+ "            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n"
		+ "            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n"
		+ "            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n"
		+ "                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n"
		+ "                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n"
		+ "                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n"
		+ "                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n"
		+ "                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n"
		+ "                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"quad\\\" -ps 1 50 50 -ps 2 50 50 -ps 3 50 50 -ps 4 50 50 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 672\\n    -height 332\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 672\\n    -height 332\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 671\\n    -height 332\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 671\\n    -height 332\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Side View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 671\\n    -height 332\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 671\\n    -height 332\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Front View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 672\\n    -height 332\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 672\\n    -height 332\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "DBAD6D27-4F64-BC74-40B4-1B8406670CD4";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyCube -n "polyCube2";
	rename -uid "D557E6BB-4A67-D262-485D-B4A7CB8D5EEC";
	setAttr ".cuv" 4;
createNode polyExtrudeFace -n "polyExtrudeFace32";
	rename -uid "EA5851A1-4538-87B0-A82E-5480D19BFF0A";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1.0848556963281377 0 0 0 0 1.0848556963281377 0 0 0 0 1.0848556963281377 0
		 -8.9164455593529741 -1.788188257782328 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -8.1208849 -1.7881882 0 ;
	setAttr ".rs" 57663;
	setAttr ".lt" -type "double3" -2.6651808501797261e-016 3.6903782722932528e-016 1.2002907483744405 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -8.1208846981356899 -2.330616105946397 -0.54242784816406886 ;
	setAttr ".cbx" -type "double3" -8.1208846981356899 -1.2457604096182591 0.54242784816406886 ;
createNode polyTweak -n "polyTweak29";
	rename -uid "83EFFE81-4DB0-6491-01F2-98A00F26A83F";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  -0.23333333 0 0 0.23333333
		 0 0 -0.23333333 0 0 0.23333333 0 0 -0.23333333 0 0 0.23333333 0 0 -0.23333333 0 0
		 0.23333333 0 0;
createNode polyExtrudeFace -n "polyExtrudeFace33";
	rename -uid "7C023573-4B7D-77B9-681A-D4BC2324C2CB";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1.0848556963281377 0 0 0 0 1.0848556963281377 0 0 0 0 1.0848556963281377 0
		 -8.9164455593529741 -1.788188257782328 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -6.871603 -1.6902053 0 ;
	setAttr ".rs" 49182;
	setAttr ".lt" -type "double3" -2.6107894042576833e-016 1.4399336062461404e-016 1.1757950188157746 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -6.8716028962403559 -2.3101189212502105 -0.54242784816406886 ;
	setAttr ".cbx" -type "double3" -6.8716028962403559 -1.0702917701185353 0.54242784816406886 ;
createNode polyTweak -n "polyTweak30";
	rename -uid "96A6DBB6-4338-88EE-E1DB-FD96783BC1A3";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[8:11]" -type "float3"  0.045159426 0.018893937 0
		 0.045159426 0.018893937 0 0.045159426 0.16174378 0 0.045159426 0.16174378 0;
createNode polyExtrudeFace -n "polyExtrudeFace34";
	rename -uid "2C5D576A-4406-17E4-2D1B-82931FEF6080";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1.0848556963281377 0 0 0 0 1.0848556963281377 0 0 0 0 1.0848556963281377 0
		 -8.9164455593529741 -1.788188257782328 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -5.6958089 -1.6902053 0 ;
	setAttr ".rs" 37028;
	setAttr ".lt" -type "double3" -2.719572296101755e-016 1.4999308398397302e-016 1.2247864779330992 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -5.6958086623517161 -2.3433268661275211 -0.54242784816406886 ;
	setAttr ".cbx" -type "double3" -5.6958086623517161 -1.0370837929100056 0.54242784816406886 ;
createNode polyTweak -n "polyTweak31";
	rename -uid "FFCFAB3D-4DE0-212D-C7DC-47A0D6B943D2";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[12:15]" -type "float3"  2.6645353e-015 -0.030610487
		 0 2.6645353e-015 -0.030610487 0 2.6645353e-015 0.030610487 0 2.6645353e-015 0.030610487
		 0;
createNode polyExtrudeFace -n "polyExtrudeFace35";
	rename -uid "C255FD33-4F4D-E60A-9A05-888C4ECA9D9C";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1.0848556963281377 0 0 0 0 1.0848556963281377 0 0 0 0 1.0848556963281377 0
		 -8.9164455593529741 -1.788188257782328 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -4.4710226 -1.6902053 0 ;
	setAttr ".rs" 45689;
	setAttr ".lt" -type "double3" -2.2300492828034394e-016 -9.9050276058173238e-017 
		1.0043249119051429 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -4.4710224147005686 -2.3433269307899596 -0.54242784816406886 ;
	setAttr ".cbx" -type "double3" -4.4710224147005686 -1.0370837282475671 0.54242784816406886 ;
createNode polyExtrudeFace -n "polyExtrudeFace36";
	rename -uid "73F52D6B-4D3E-CDE5-51FD-B088CB63965C";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1.0848556963281377 0 0 0 0 1.0848556963281377 0 0 0 0 1.0848556963281377 0
		 -8.9164455593529741 -1.788188257782328 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -3.4666979 -1.7391968 0 ;
	setAttr ".rs" 50485;
	setAttr ".lt" -type "double3" -8.1587168883053005e-017 4.4997925195192009e-017 0.36743594337993057 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -3.4666978364704892 -2.3021437470774226 -0.54242784816406886 ;
	setAttr ".cbx" -type "double3" -3.4666978364704892 -1.1762497755612302 0.54242784816406886 ;
createNode polyTweak -n "polyTweak32";
	rename -uid "713B7D1B-44E6-178B-8232-01A8FE957B0A";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[16:23]" -type "float3"  2.3092639e-014 -9.3132257e-010
		 -1.6653345e-015 2.3092639e-014 -9.3132257e-010 1.6653345e-015 2.3092639e-014 9.3132257e-010
		 -1.6653345e-015 2.3092639e-014 9.3132257e-010 1.6653345e-015 1.687539e-014 0.037961915
		 -1.8873791e-015 1.687539e-014 0.037961915 1.8873791e-015 1.687539e-014 -0.12828076
		 -1.8873791e-015 1.687539e-014 -0.12828076 1.8873791e-015;
createNode polyExtrudeFace -n "polyExtrudeFace37";
	rename -uid "0B5E1120-4084-3330-6643-87BFE67D6923";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1.0848556963281377 0 0 0 0 1.0848556963281377 0 0 0 0 1.0848556963281377 0
		 -8.9164455593529741 -1.788188257782328 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -3.0992625 -1.7391967 0 ;
	setAttr ".rs" 60320;
	setAttr ".lt" -type "double3" 3.0076128885786918e-017 -0.10535065554438193 0.13545084284277698 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -3.0992625829345535 -2.1513619979771463 -0.54242784816406886 ;
	setAttr ".cbx" -type "double3" -3.0992625829345535 -1.3270313953366299 0.54242784816406886 ;
createNode polyTweak -n "polyTweak33";
	rename -uid "5D06511D-4408-5C7D-DE52-9FB36B1B3575";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[24:27]" -type "float3"  3.375078e-014 0.13898779 -1.9984014e-015
		 3.375078e-014 0.13898779 1.9984014e-015 3.375078e-014 -0.13898779 -1.9984014e-015
		 3.375078e-014 -0.13898779 1.9984014e-015;
createNode polyExtrudeFace -n "polyExtrudeFace38";
	rename -uid "7C791A34-42CB-B805-3D07-10B0DA05797B";
	setAttr ".ics" -type "componentList" 1 "f[5]";
	setAttr ".ix" -type "matrix" 1.0848556963281377 0 0 0 0 1.0848556963281377 0 0 0 0 1.0848556963281377 0
		 -8.9164455593529741 -1.788188257782328 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -9.1702032 -1.7881882 0 ;
	setAttr ".rs" 42630;
	setAttr ".lt" -type "double3" -4.9303806576313238e-032 0 0.57942860549409936 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -9.1702030822363749 -2.2918732179970984 -0.54242784816406886 ;
	setAttr ".cbx" -type "double3" -9.1702030822363749 -1.2845032975675579 0.54242784816406886 ;
createNode polyTweak -n "polyTweak34";
	rename -uid "C803B1F8-43A7-988B-BCEA-A28D591F4046";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk";
	setAttr ".tk[0]" -type "float3" 0.49942458 0.035712466 0 ;
	setAttr ".tk[2]" -type "float3" 0.49942458 -0.035712466 0 ;
	setAttr ".tk[4]" -type "float3" 0.49942458 -0.035712466 0 ;
	setAttr ".tk[6]" -type "float3" 0.49942458 0.035712466 0 ;
	setAttr ".tk[28]" -type "float3" 0.041618697 0.31359059 -2.4424907e-015 ;
	setAttr ".tk[29]" -type "float3" 0.041618697 0.31359059 2.4424907e-015 ;
	setAttr ".tk[30]" -type "float3" 0.041618697 -0.17486152 -2.4424907e-015 ;
	setAttr ".tk[31]" -type "float3" 0.041618697 -0.17486152 2.4424907e-015 ;
createNode polyExtrudeFace -n "polyExtrudeFace39";
	rename -uid "033712BA-4C3D-B2C4-6410-F8BD99ECFF3F";
	setAttr ".ics" -type "componentList" 1 "f[5]";
	setAttr ".ix" -type "matrix" 1.0848556963281377 0 0 0 0 1.0848556963281377 0 0 0 0 1.0848556963281377 0
		 -8.9164455593529741 -1.788188257782328 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -9.7496319 -1.8182884 0 ;
	setAttr ".rs" 52792;
	setAttr ".lt" -type "double3" -4.3140830754274083e-031 0 0.32357701345774714 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -9.7496319415837949 -2.2859977301932122 -0.54242784816406886 ;
	setAttr ".cbx" -type "double3" -9.7496319415837949 -1.3505791598897887 0.54242784816406886 ;
createNode polyTweak -n "polyTweak35";
	rename -uid "F02CB024-4804-421D-5599-A39D5BD9CB05";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[32:35]" -type "float3"  6.5503158e-015 0.0054159053
		 0 6.5503158e-015 0.0054159053 0 6.5503158e-015 -0.060907502 0 6.5503158e-015 -0.060907502
		 0;
createNode polyExtrudeFace -n "polyExtrudeFace40";
	rename -uid "E4682E61-498E-F946-908A-A9B188CF6F8F";
	setAttr ".ics" -type "componentList" 1 "f[5]";
	setAttr ".ix" -type "matrix" 1.0848556963281377 0 0 0 0 1.0848556963281377 0 0 0 0 1.0848556963281377 0
		 -8.9164455593529741 -1.788188257782328 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -10.07321 -1.8333385 0 ;
	setAttr ".rs" 41185;
	setAttr ".lt" -type "double3" -3.508881703341812e-017 -0.045150280947592325 0.15802598331657336 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -10.073210025591534 -2.1340173011408696 -0.54242784816406886 ;
	setAttr ".cbx" -type "double3" -10.073210025591534 -1.5326597277044749 0.54242784816406886 ;
createNode polyTweak -n "polyTweak36";
	rename -uid "BF5C508F-4449-28FF-74CC-7CA379F0BE87";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[36:39]" -type "float3"  3.5971226e-014 0.14009276
		 0 3.5971226e-014 0.14009276 0 3.5971226e-014 -0.16783854 0 3.5971226e-014 -0.16783854
		 0;
createNode polyExtrudeFace -n "polyExtrudeFace41";
	rename -uid "CE9C2DB7-43EC-F5F0-78B2-78ADC6FDE388";
	setAttr ".ics" -type "componentList" 1 "f[5]";
	setAttr ".ix" -type "matrix" 1.0848556963281377 0 0 0 0 1.0848556963281377 0 0 0 0 1.0848556963281377 0
		 -8.9164455593529741 -1.788188257782328 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -10.231236 -1.8784888 0 ;
	setAttr ".rs" 50994;
	setAttr ".lt" -type "double3" -2.5063440738155519e-017 -0.060200374596789619 0.1128757023689797 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -10.231235713679814 -2.0449428361893052 -0.54242784816406886 ;
	setAttr ".cbx" -type "double3" -10.231235713679814 -1.7120347059367276 0.54242784816406886 ;
createNode polyTweak -n "polyTweak37";
	rename -uid "77F538AF-412B-BC7E-EB1A-7490A0AEF5D8";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[40:43]" -type "float3"  3.0642155e-014 0.12372594
		 0 3.0642155e-014 0.12372594 0 3.0642155e-014 -0.12372594 0 3.0642155e-014 -0.12372594
		 0;
createNode polyExtrudeFace -n "polyExtrudeFace42";
	rename -uid "4A76703A-4DEF-07B2-C3AF-8E9D95D6D57B";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1.0848556963281377 0 0 0 0 1.0848556963281377 0 0 0 0 1.0848556963281377 0
		 2.7950496675498684 -1.7881882577823278 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 13.607809 -1.2562519 0 ;
	setAttr ".rs" 32849;
	setAttr ".lt" -type "double3" -4.0357134644920076e-017 -1.9978635830726459e-016 
		0.18175237654859977 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 13.607809504926253 -1.6571749153661131 -0.54242784816406886 ;
	setAttr ".cbx" -type "double3" 13.607809504926253 -0.85532886513516293 0.54242784816406886 ;
createNode polyExtrudeFace -n "polyExtrudeFace43";
	rename -uid "8A3F3DDA-4B41-0026-801C-27A0DDB63F42";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1.0848556963281377 0 0 0 0 1.0848556963281377 0 0 0 0 1.0848556963281377 0
		 2.7950496675498684 -1.7881882577823278 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 13.721405 -1.2562519 0 ;
	setAttr ".rs" 41726;
	setAttr ".lt" -type "double3" -1.1770830938101176e-017 -2.155526163281824e-016 0.053011109826677227 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 13.721405372931757 -1.5354798021341169 -0.54242784816406886 ;
	setAttr ".cbx" -type "double3" 13.721405372931757 -0.97702396220154952 0.54242784816406886 ;
createNode polyTweak -n "polyTweak38";
	rename -uid "BD0CC123-4FF0-67CD-60A7-A3A10046CE4A";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[48:51]" -type "float3"  -0.062825993 0.11217631 0
		 -0.062825993 0.11217631 0 -0.062825993 -0.11217631 0 -0.062825993 -0.11217631 0;
createNode polySplitRing -n "polySplitRing1";
	rename -uid "6E250E46-4200-9649-5391-9AAF24C478A6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[12:13]" "e[15]" "e[17]";
	setAttr ".ix" -type "matrix" 1.0848556963281377 0 0 0 0 1.0848556963281377 0 0 0 0 1.0848556963281377 0
		 2.7950496675498684 -1.7881882577823278 0 1;
	setAttr ".wt" 0.40405663847923279;
	setAttr ".re" 17;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak39";
	rename -uid "4A82B2BD-4014-A1A9-F241-3390BB2B1A18";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[52:55]" -type "float3"  -3.1974423e-014 0.12868702
		 0 -3.1974423e-014 0.12868702 0 -3.1974423e-014 -0.12868702 0 -3.1974423e-014 -0.12868702
		 0;
createNode polySplitRing -n "polySplitRing2";
	rename -uid "856EA3B6-46EA-83F1-3BE5-5C8E782AB9BA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[12:13]" "e[15]" "e[17]";
	setAttr ".ix" -type "matrix" 1.0848556963281377 0 0 0 0 1.0848556963281377 0 0 0 0 1.0848556963281377 0
		 2.7950496675498684 -1.7881882577823278 0 1;
	setAttr ".wt" 0.81781476736068726;
	setAttr ".dr" no;
	setAttr ".re" 17;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak40";
	rename -uid "5822F5C6-4436-C771-1B04-07867257D9EF";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[56:59]" -type "float3"  0.011101088 0.12899455 0 0.011101088
		 0.12899455 0 0.011101088 -0.017983664 0 0.011101088 -0.017983664 0;
createNode objectSet -n "set1";
	rename -uid "0A03BEC6-4329-AB52-6E9D-4886EE2F3C70";
	setAttr ".ihi" 0;
createNode groupId -n "groupId1";
	rename -uid "A2211326-43C3-561A-3573-3A885CF3AD1F";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "0093AF7D-449D-5537-6AA7-37B6FDBE156F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 36 "e[2:3]" "e[8:9]" "e[12]" "e[15:16]" "e[20]" "e[23:24]" "e[28]" "e[31:32]" "e[36]" "e[39:40]" "e[52:54]" "e[60:62]" "e[68:70]" "e[76:78]" "e[84:86]" "e[92:94]" "e[100:102]" "e[108:110]" "e[116:118]" "e[124:126]" "e[132:134]" "e[140:142]" "e[148:150]" "e[156:158]" "e[164:166]" "e[172:174]" "e[180:182]" "e[188:190]" "e[196:198]" "e[204:206]" "e[212:214]" "e[220:222]" "e[228:230]" "e[236:238]" "e[244:246]" "e[252:254]";
createNode polyTweak -n "polyTweak41";
	rename -uid "2419F946-48A4-7E38-D296-CF833AAA1345";
	setAttr ".uopa" yes;
	setAttr -s 38 ".tk";
	setAttr ".tk[0]" -type "float3" -3.5862856 0 0 ;
	setAttr ".tk[2]" -type "float3" -3.5862856 0 0 ;
	setAttr ".tk[4]" -type "float3" -3.5862856 0 0 ;
	setAttr ".tk[6]" -type "float3" -3.5862856 0 0 ;
	setAttr ".tk[28]" -type "float3" -3.5862856 0 0 ;
	setAttr ".tk[31]" -type "float3" -3.5862856 0 0 ;
	setAttr ".tk[32]" -type "float3" -3.5862856 0 0 ;
	setAttr ".tk[35]" -type "float3" -3.5862856 0 0 ;
	setAttr ".tk[36]" -type "float3" -3.5862856 0 0 ;
	setAttr ".tk[39]" -type "float3" -3.5862856 0 0 ;
	setAttr ".tk[40]" -type "float3" -3.5862856 0 0 ;
	setAttr ".tk[43]" -type "float3" -3.5862856 0 0 ;
	setAttr ".tk[44]" -type "float3" -3.5862856 0 0 ;
	setAttr ".tk[47]" -type "float3" -3.5862856 0 0 ;
	setAttr ".tk[48]" -type "float3" -3.5862856 0 0 ;
	setAttr ".tk[51]" -type "float3" -3.5862856 0 0 ;
	setAttr ".tk[52]" -type "float3" -3.5862856 0 0 ;
	setAttr ".tk[55]" -type "float3" -3.5862856 0 0 ;
	setAttr ".tk[56]" -type "float3" -3.5862856 0 0 ;
	setAttr ".tk[59]" -type "float3" -3.5862856 0 0 ;
	setAttr ".tk[96]" -type "float3" 0.24349865 0 0 ;
	setAttr ".tk[99]" -type "float3" 0.24349865 0 0 ;
	setAttr ".tk[100]" -type "float3" 0.21755627 -3.9968029e-014 0 ;
	setAttr ".tk[103]" -type "float3" 0.21755627 -3.9968029e-014 0 ;
	setAttr ".tk[104]" -type "float3" 0.15171823 -2.5757174e-014 0 ;
	setAttr ".tk[107]" -type "float3" 0.15171823 -2.5757174e-014 0 ;
	setAttr ".tk[108]" -type "float3" 0.16824356 -2.553513e-014 0 ;
	setAttr ".tk[111]" -type "float3" 0.16824356 -2.553513e-014 0 ;
	setAttr ".tk[112]" -type "float3" 0.053982854 -2.7672309e-014 0 ;
	setAttr ".tk[115]" -type "float3" 0.053982854 -2.7672309e-014 0 ;
	setAttr ".tk[116]" -type "float3" 0.10943839 -2.8532732e-014 0 ;
	setAttr ".tk[119]" -type "float3" 0.10943839 -2.8532732e-014 0 ;
	setAttr ".tk[120]" -type "float3" -0.11430481 -2.553513e-014 0 ;
	setAttr ".tk[123]" -type "float3" -0.11430481 -2.553513e-014 0 ;
	setAttr ".tk[124]" -type "float3" -0.63006413 -2.553513e-014 0 ;
	setAttr ".tk[127]" -type "float3" -0.63006413 -2.553513e-014 0 ;
	setAttr ".tk[128]" -type "float3" -1.0553358 -2.5979219e-014 0 ;
	setAttr ".tk[131]" -type "float3" -1.0553358 -2.5979219e-014 0 ;
createNode deleteComponent -n "deleteComponent1";
	rename -uid "8D0538D9-4AB0-883E-C80D-73A081DAB57F";
	setAttr ".dc" -type "componentList" 31 "f[2]" "f[7]" "f[11]" "f[15]" "f[19]" "f[26]" "f[30]" "f[34]" "f[38]" "f[42]" "f[46]" "f[50]" "f[54]" "f[58]" "f[62]" "f[66]" "f[70]" "f[74]" "f[78]" "f[82]" "f[86]" "f[90]" "f[94]" "f[98]" "f[102]" "f[106]" "f[110]" "f[114]" "f[118]" "f[122]" "f[126]";
createNode objectSet -n "set2";
	rename -uid "1C26433A-4621-403E-D406-4C870147FC97";
	setAttr ".ihi" 0;
createNode groupId -n "groupId2";
	rename -uid "1B7E0279-44A4-F6A4-F5DB-618D0DE5BB15";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "1CCE6634-4E36-42DC-C2FC-2CB80A3405D3";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 30 "e[2:3]" "e[8:9]" "e[12]" "e[15:16]" "e[20]" "e[23:24]" "e[28]" "e[31:32]" "e[36]" "e[39:40]" "e[44]" "e[47:48]" "e[52]" "e[55:56]" "e[60]" "e[65]" "e[67:68]" "e[73]" "e[75:76]" "e[81]" "e[83:84]" "e[89]" "e[91:92]" "e[95:96]" "e[100]" "e[103:104]" "e[109]" "e[111:112]" "e[117]" "e[119:120]";
createNode polyTweak -n "polyTweak42";
	rename -uid "FCF300E9-4A72-E6B8-7C16-D1A21652D626";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[60:63]" -type "float3"  2.220446e-016 0.019524772
		 0 2.220446e-016 0.019524772 0 2.220446e-016 -0.0064879255 0 2.220446e-016 -0.0064879255
		 0;
createNode deleteComponent -n "deleteComponent2";
	rename -uid "B1B99AD2-48D9-ED19-BDC1-E28DEFE7B02B";
	setAttr ".dc" -type "componentList" 15 "f[2]" "f[7]" "f[11]" "f[15]" "f[19]" "f[23]" "f[27]" "f[33]" "f[37]" "f[41]" "f[45]" "f[47]" "f[51]" "f[55]" "f[59]";
createNode objectSet -n "set3";
	rename -uid "EEE76FAB-4D29-2F04-DD80-AF84E13F5B1C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId3";
	rename -uid "C3240FCC-41FD-A673-D792-2AA203F72520";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "29929B68-4EF7-0098-7C0E-E4B0749EDCB5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 23 "e[2:3]" "e[8:9]" "e[12]" "e[15:16]" "e[20]" "e[23:24]" "e[28]" "e[31:32]" "e[36]" "e[39:40]" "e[44]" "e[47:48]" "e[52]" "e[55:56]" "e[60]" "e[65]" "e[67:68]" "e[73]" "e[75:76]" "e[81]" "e[83:84]" "e[89]" "e[91]";
createNode polyTweak -n "polyTweak43";
	rename -uid "4F8B2624-41CC-7AF7-910B-9287432E294F";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[44:47]" -type "float3"  0.020809349 0.11670084 0 0.020809349
		 0.11670084 0 0.020809349 -0.075082153 0 0.020809349 -0.075082153 0;
createNode deleteComponent -n "deleteComponent3";
	rename -uid "C7B4F8A6-4971-A200-8EAC-5CB9C622FFD4";
	setAttr ".dc" -type "componentList" 11 "f[2]" "f[7]" "f[11]" "f[15]" "f[19]" "f[23]" "f[27]" "f[33]" "f[37]" "f[41]" "f[45]";
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 3 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr ":defaultColorMgtGlobals.cme" "imagePlaneShape1.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "imagePlaneShape1.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "imagePlaneShape1.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "imagePlaneShape1.ws";
connectAttr ":frontShape.msg" "imagePlaneShape1.ltc";
connectAttr "groupId1.id" "pCubeShape1.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape1.iog.og[0].gco";
connectAttr "deleteComponent1.og" "pCubeShape1.i";
connectAttr "groupId3.id" "pCubeShape2.iog.og[0].gid";
connectAttr "set3.mwc" "pCubeShape2.iog.og[0].gco";
connectAttr "deleteComponent3.og" "pCubeShape2.i";
connectAttr "groupId2.id" "pCubeShape3.iog.og[0].gid";
connectAttr "set2.mwc" "pCubeShape3.iog.og[0].gco";
connectAttr "deleteComponent2.og" "pCubeShape3.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyTweak1.out" "polyExtrudeFace1.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polyCube1.out" "polyTweak1.ip";
connectAttr "polyExtrudeFace1.out" "polyExtrudeFace2.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyTweak2.out" "polyExtrudeFace3.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace3.mp";
connectAttr "polyExtrudeFace2.out" "polyTweak2.ip";
connectAttr "polyTweak3.out" "polyExtrudeFace4.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace4.mp";
connectAttr "polyExtrudeFace3.out" "polyTweak3.ip";
connectAttr "polyTweak4.out" "polyExtrudeFace5.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace5.mp";
connectAttr "polyExtrudeFace4.out" "polyTweak4.ip";
connectAttr "polyTweak5.out" "polyExtrudeFace6.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace6.mp";
connectAttr "polyExtrudeFace5.out" "polyTweak5.ip";
connectAttr "polyTweak6.out" "polyExtrudeFace7.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace7.mp";
connectAttr "polyExtrudeFace6.out" "polyTweak6.ip";
connectAttr "polyTweak7.out" "polyExtrudeFace8.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace8.mp";
connectAttr "polyExtrudeFace7.out" "polyTweak7.ip";
connectAttr "polyTweak8.out" "polyExtrudeFace9.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace9.mp";
connectAttr "polyExtrudeFace8.out" "polyTweak8.ip";
connectAttr "polyTweak9.out" "polyExtrudeFace10.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace10.mp";
connectAttr "polyExtrudeFace9.out" "polyTweak9.ip";
connectAttr "polyTweak10.out" "polyExtrudeFace11.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace11.mp";
connectAttr "polyExtrudeFace10.out" "polyTweak10.ip";
connectAttr "polyTweak11.out" "polyExtrudeFace12.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace12.mp";
connectAttr "polyExtrudeFace11.out" "polyTweak11.ip";
connectAttr "polyTweak12.out" "polyExtrudeFace13.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace13.mp";
connectAttr "polyExtrudeFace12.out" "polyTweak12.ip";
connectAttr "polyTweak13.out" "polyExtrudeFace14.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace14.mp";
connectAttr "polyExtrudeFace13.out" "polyTweak13.ip";
connectAttr "polyTweak14.out" "polyExtrudeFace15.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace15.mp";
connectAttr "polyExtrudeFace14.out" "polyTweak14.ip";
connectAttr "polyTweak15.out" "polyExtrudeFace16.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace16.mp";
connectAttr "polyExtrudeFace15.out" "polyTweak15.ip";
connectAttr "polyTweak16.out" "polyExtrudeFace17.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace17.mp";
connectAttr "polyExtrudeFace16.out" "polyTweak16.ip";
connectAttr "polyExtrudeFace17.out" "polyExtrudeFace18.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace18.mp";
connectAttr "polyTweak17.out" "polyExtrudeFace19.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace19.mp";
connectAttr "polyExtrudeFace18.out" "polyTweak17.ip";
connectAttr "polyTweak18.out" "polyExtrudeFace20.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace20.mp";
connectAttr "polyExtrudeFace19.out" "polyTweak18.ip";
connectAttr "polyTweak19.out" "polyExtrudeFace21.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace21.mp";
connectAttr "polyExtrudeFace20.out" "polyTweak19.ip";
connectAttr "polyTweak20.out" "polyExtrudeFace22.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace22.mp";
connectAttr "polyExtrudeFace21.out" "polyTweak20.ip";
connectAttr "polyTweak21.out" "polyExtrudeFace23.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace23.mp";
connectAttr "polyExtrudeFace22.out" "polyTweak21.ip";
connectAttr "polyTweak22.out" "polyExtrudeFace24.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace24.mp";
connectAttr "polyExtrudeFace23.out" "polyTweak22.ip";
connectAttr "polyTweak23.out" "polyExtrudeFace25.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace25.mp";
connectAttr "polyExtrudeFace24.out" "polyTweak23.ip";
connectAttr "polyTweak24.out" "polyExtrudeFace26.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace26.mp";
connectAttr "polyExtrudeFace25.out" "polyTweak24.ip";
connectAttr "polyTweak25.out" "polyExtrudeFace27.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace27.mp";
connectAttr "polyExtrudeFace26.out" "polyTweak25.ip";
connectAttr "polyTweak26.out" "polyExtrudeFace28.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace28.mp";
connectAttr "polyExtrudeFace27.out" "polyTweak26.ip";
connectAttr "polyTweak27.out" "polyExtrudeFace29.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace29.mp";
connectAttr "polyExtrudeFace28.out" "polyTweak27.ip";
connectAttr "polyTweak28.out" "polyExtrudeFace30.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace30.mp";
connectAttr "polyExtrudeFace29.out" "polyTweak28.ip";
connectAttr "polyExtrudeFace30.out" "polyExtrudeFace31.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace31.mp";
connectAttr "polyTweak29.out" "polyExtrudeFace32.ip";
connectAttr "pCubeShape2.wm" "polyExtrudeFace32.mp";
connectAttr "polyCube2.out" "polyTweak29.ip";
connectAttr "polyTweak30.out" "polyExtrudeFace33.ip";
connectAttr "pCubeShape2.wm" "polyExtrudeFace33.mp";
connectAttr "polyExtrudeFace32.out" "polyTweak30.ip";
connectAttr "polyTweak31.out" "polyExtrudeFace34.ip";
connectAttr "pCubeShape2.wm" "polyExtrudeFace34.mp";
connectAttr "polyExtrudeFace33.out" "polyTweak31.ip";
connectAttr "polyExtrudeFace34.out" "polyExtrudeFace35.ip";
connectAttr "pCubeShape2.wm" "polyExtrudeFace35.mp";
connectAttr "polyTweak32.out" "polyExtrudeFace36.ip";
connectAttr "pCubeShape2.wm" "polyExtrudeFace36.mp";
connectAttr "polyExtrudeFace35.out" "polyTweak32.ip";
connectAttr "polyTweak33.out" "polyExtrudeFace37.ip";
connectAttr "pCubeShape2.wm" "polyExtrudeFace37.mp";
connectAttr "polyExtrudeFace36.out" "polyTweak33.ip";
connectAttr "polyTweak34.out" "polyExtrudeFace38.ip";
connectAttr "pCubeShape2.wm" "polyExtrudeFace38.mp";
connectAttr "polyExtrudeFace37.out" "polyTweak34.ip";
connectAttr "polyTweak35.out" "polyExtrudeFace39.ip";
connectAttr "pCubeShape2.wm" "polyExtrudeFace39.mp";
connectAttr "polyExtrudeFace38.out" "polyTweak35.ip";
connectAttr "polyTweak36.out" "polyExtrudeFace40.ip";
connectAttr "pCubeShape2.wm" "polyExtrudeFace40.mp";
connectAttr "polyExtrudeFace39.out" "polyTweak36.ip";
connectAttr "polyTweak37.out" "polyExtrudeFace41.ip";
connectAttr "pCubeShape2.wm" "polyExtrudeFace41.mp";
connectAttr "polyExtrudeFace40.out" "polyTweak37.ip";
connectAttr "polySurfaceShape1.o" "polyExtrudeFace42.ip";
connectAttr "pCubeShape3.wm" "polyExtrudeFace42.mp";
connectAttr "polyTweak38.out" "polyExtrudeFace43.ip";
connectAttr "pCubeShape3.wm" "polyExtrudeFace43.mp";
connectAttr "polyExtrudeFace42.out" "polyTweak38.ip";
connectAttr "polyTweak39.out" "polySplitRing1.ip";
connectAttr "pCubeShape3.wm" "polySplitRing1.mp";
connectAttr "polyExtrudeFace43.out" "polyTweak39.ip";
connectAttr "polyTweak40.out" "polySplitRing2.ip";
connectAttr "pCubeShape3.wm" "polySplitRing2.mp";
connectAttr "polySplitRing1.out" "polyTweak40.ip";
connectAttr "groupId1.msg" "set1.gn" -na;
connectAttr "pCubeShape1.iog.og[0]" "set1.dsm" -na;
connectAttr "polyExtrudeFace31.out" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "groupParts1.og" "polyTweak41.ip";
connectAttr "polyTweak41.out" "deleteComponent1.ig";
connectAttr "groupId2.msg" "set2.gn" -na;
connectAttr "pCubeShape3.iog.og[0]" "set2.dsm" -na;
connectAttr "polySplitRing2.out" "groupParts2.ig";
connectAttr "groupId2.id" "groupParts2.gi";
connectAttr "groupParts2.og" "polyTweak42.ip";
connectAttr "polyTweak42.out" "deleteComponent2.ig";
connectAttr "groupId3.msg" "set3.gn" -na;
connectAttr "pCubeShape2.iog.og[0]" "set3.dsm" -na;
connectAttr "polyExtrudeFace41.out" "groupParts3.ig";
connectAttr "groupId3.id" "groupParts3.gi";
connectAttr "groupParts3.og" "polyTweak43.ip";
connectAttr "polyTweak43.out" "deleteComponent3.ig";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCubeShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape3.iog" ":initialShadingGroup.dsm" -na;
// End of WetlandSection_model_001.ma
