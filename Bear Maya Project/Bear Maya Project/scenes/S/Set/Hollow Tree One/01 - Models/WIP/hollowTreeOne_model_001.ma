//Maya ASCII 2017ff05 scene
//Name: hollowTreeOne_model_001.ma
//Last modified: Thu, Sep 21, 2017 03:40:58 PM
//Codeset: 1252
requires maya "2017ff05";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Enterprise Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "education";
createNode transform -s -n "persp";
	rename -uid "F40FE648-4E83-0A83-0B69-339714E8E6E0";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 6.3579085249653771 12.633556349478816 -16.066341915374643 ;
	setAttr ".r" -type "double3" -29.138352729611018 161.39999999992312 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "A92542E0-4A4F-C895-974A-CE9143378074";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 17.971831735963317;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 1.8252369951867213 12.752561274180982 -2.1861573381430408 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "5E3B3BEB-4792-451E-222C-36BDA1BDB657";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "31B11BC2-4777-89D0-0F46-2EAEF2407E5E";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "2D8976D7-4885-2BDD-5658-D8B080741D5C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -2.2666692133785427 6.3645329724491519 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "5CA9F0BC-43C5-5EA7-ABA7-7BAC6745F0C3";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 37.613866347950704;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "A4EAA2D4-4476-B68E-9965-52BEA596EC68";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 5.6827957068068544 0.68925837015314628 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "2A83B06E-42C2-690B-64B2-E498ADCC1CF6";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 53.248602395829302;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "pCylinder1";
	rename -uid "6756173B-4DEA-9BCB-E130-0A821C17B1C1";
	setAttr ".t" -type "double3" 0 4.5274275034449296 0 ;
	setAttr ".s" -type "double3" 2.7022374593329266 4.6776033309485667 2.7022374593329266 ;
createNode transform -n "transform1" -p "pCylinder1";
	rename -uid "E9FA9AB3-4CF7-3EBE-B792-7EA46D4A219B";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape1" -p "transform1";
	rename -uid "5ED2F9FD-44A2-0B57-C743-9E811C06959F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 3 ".ciog[0].cog";
	setAttr ".pv" -type "double2" 0.49999988079071045 0.3125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder2";
	rename -uid "0986929A-4BED-CE18-6F50-64A72FEE2B33";
	setAttr ".t" -type "double3" 0 4.5274275034449296 0 ;
	setAttr ".s" -type "double3" 2.3688729489507097 4.6776033309485667 2.3688729489507097 ;
createNode mesh -n "polySurfaceShape1" -p "pCylinder2";
	rename -uid "AC147937-4D6A-1D05-02AB-4D8B6290E376";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 2 ".ciog[0].cog";
	setAttr ".pv" -type "double2" 0.49999988079071045 0.3125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 108 ".uvst[0].uvsp[0:107]" -type "float2" 0.375 0.3125 0.38749999
		 0.3125 0.39999998 0.3125 0.41249996 0.3125 0.42499995 0.3125 0.43749994 0.3125 0.44999993
		 0.3125 0.46249992 0.3125 0.4749999 0.3125 0.48749989 0.3125 0.61249977 0.3125 0.62499976
		 0.3125 0.375 0.68843985 0.38749999 0.68843985 0.39999998 0.68843985 0.41249996 0.68843985
		 0.42499995 0.68843985 0.43749994 0.68843985 0.44999993 0.68843985 0.46249992 0.68843985
		 0.4749999 0.68843985 0.48749989 0.68843985 0.61249977 0.68843985 0.62499976 0.68843985
		 0.48749989 0.66268158 0.4749999 0.66268158 0.46249992 0.66268158 0.44999993 0.66268158
		 0.43749994 0.66268158 0.42499995 0.66268158 0.41249996 0.66268158 0.39999998 0.66268158
		 0.38749999 0.66268158 0.62499976 0.66268158 0.375 0.66268158 0.61249977 0.66268158
		 0.48749989 0.63639599 0.4749999 0.63639599 0.46249992 0.63639599 0.44999993 0.63639599
		 0.43749994 0.63639599 0.42499995 0.63639599 0.41249996 0.63639599 0.39999998 0.63639599
		 0.38749999 0.63639599 0.62499976 0.63639599 0.375 0.63639599 0.61249977 0.63639599
		 0.61249977 0.62336034 0.62499976 0.62336034 0.375 0.62336034 0.38749999 0.62336034
		 0.39999998 0.62336034 0.41249996 0.62336034 0.42499995 0.62336034 0.43749994 0.62336034
		 0.44999993 0.62336034 0.46249992 0.62336034 0.4749999 0.62336034 0.48749989 0.62336034
		 0.61249977 0.57855874 0.62499976 0.57855874 0.375 0.57855874 0.38749999 0.57855874
		 0.39999998 0.57855874 0.41249996 0.57855874 0.42499995 0.57855874 0.43749994 0.57855874
		 0.44999993 0.57855874 0.46249992 0.57855874 0.4749999 0.57855874 0.48749989 0.57855874
		 0.48749989 0.34440276 0.4749999 0.34440276 0.46249992 0.34440276 0.44999993 0.34440276
		 0.43749994 0.34440276 0.42499995 0.34440276 0.41249999 0.34440276 0.39999998 0.34440276
		 0.38749999 0.34440276 0.62499976 0.34440276 0.37500003 0.34440276 0.61249977 0.34440276
		 0.61249983 0.35333258 0.62499976 0.35333258 0.37500003 0.35333258 0.38749999 0.35333258
		 0.40000001 0.35333258 0.41249999 0.35333258 0.42499998 0.35333258 0.43749994 0.35333258
		 0.44999993 0.35333258 0.46249992 0.35333258 0.4749999 0.35333258 0.48749992 0.35333258
		 0.61249983 0.39844829 0.62499976 0.39844829 0.37500006 0.39844829 0.38750002 0.39844829
		 0.40000001 0.39844829 0.41249999 0.39844829 0.42499998 0.39844829 0.43749994 0.39844829
		 0.44999996 0.39844829 0.46249992 0.39844829 0.4749999 0.39844829 0.48749992 0.39844829;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 102 ".vt[0:101]"  1.66936922 -1 -0.35362118 1.42005026 -1 -0.84293705
		 1.031726837 -1 -1.23126042 0.54241097 -1 -1.48057926 9.0036096e-008 -1 -1.5664885
		 -0.54241079 -1 -1.48057914 -1.03172648 -1 -1.23126006 -1.42004967 -1 -0.84293675
		 -1.66936839 -1 -0.35362095 -1.75527787 -1 0.18878971 1.75527763 -1 0.18878971 0.95105714 1.75840771 -0.30901718
		 0.80901754 1.75840771 -0.5877856 0.67545396 1.75840771 -0.7760157 0.30901715 1.75840771 -0.95105702
		 0.077210471 1.75840771 -1.000000476837 -0.30901715 1.75840771 -0.95105696 -0.58778548 1.75840771 -0.8090173
		 -0.80901724 1.75840771 -0.58778542 -0.95105678 1.75840771 -0.30901706 -1.000000238419 1.75840771 0
		 1 1.75840771 0 -1.025879025 1.56940985 4.6274837e-009 -0.97566891 1.56940985 -0.31701404
		 -0.82995355 1.56940985 -0.60299659 -0.60299665 1.56940985 -0.82995367 -0.31701413 1.56940985 -0.97566915
		 1.7986151e-008 1.56940985 -1.025879264 0.31701413 1.56940985 -0.97566921 0.65593016 1.56940985 -0.80994833
		 0.82995391 1.56940985 -0.60299677 0.97566932 1.56940985 -0.31701416 1.025878787 1.56940985 4.6274837e-009
		 -0.04479406 1.75840771 -1.000000476837 -1.052287579 1.37654257 9.3497068e-009 -1.00078499317 1.37654257 -0.32517472
		 -0.85131854 1.37654257 -0.61851919 -0.61851925 1.37654257 -0.85131866 -0.32517484 1.37654257 -1.00078523159
		 2.0015776e-008 1.37654257 -1.052287817 0.32517484 1.37654257 -1.00078523159 0.61851931 1.37654257 -0.85131884
		 0.85131896 1.37654257 -0.61851931 1.00078547001 1.37654257 -0.32517487 1.05228734 1.37654257 9.3497068e-009
		 0.60299677 1.56940985 -0.82995385 0.5877856 1.75840771 -0.80901748 1.06538403 1.28089499 1.1691573e-008
		 1.013241172 1.28089499 -0.32922196 0.8619144 1.28089499 -0.62621731 0.62621731 1.28089499 -0.86191428
		 0.32922193 1.28089499 -1.013240933 2.1022316e-008 1.28089499 -1.065384388 -0.32922193 1.28089499 -1.013240933
		 -0.62621725 1.28089499 -0.86191404 -0.86191392 1.28089499 -0.62621719 -1.013240695 1.28089499 -0.32922181
		 -1.065384269 1.28089499 1.1691573e-008 1.11039519 0.95216942 1.9740192e-008 1.056049347 0.95216942 -0.34313118
		 0.8983292 0.95216942 -0.6526742 0.6526742 0.95216942 -0.89832908 0.34313115 0.95216942 -1.056049109
		 2.4481636e-008 0.95216942 -1.11039555 -0.34313115 0.95216942 -1.056049109 -0.65267414 0.95216942 -0.89832884
		 -0.89832872 0.95216942 -0.65267408 -1.05604887 0.95216942 -0.34313104 -1.11039543 0.95216942 1.9740192e-008
		 -1.34564662 -0.7659179 6.1806382e-008 -1.27978599 -0.7659179 -0.4158276 -1.088650942 -0.7659179 -0.79095125
		 -0.79095131 -0.7659179 -1.088651061 -0.41582775 -0.7659179 -1.27978623 4.2561805e-008 -0.7659179 -1.34564686
		 0.41582784 -0.7659179 -1.27978635 0.79095155 -0.7659179 -1.088651419 1.088651419 -0.7659179 -0.79095149
		 1.27978659 -0.7659179 -0.41582775 1.3456465 -0.7659179 6.1806382e-008 1.33667505 -0.70039666 6.020214e-008
		 1.27125418 -0.70039666 -0.41305539 1.081393361 -0.70039666 -0.78567815 0.78567821 -0.70039666 -1.081393361
		 0.41305548 -0.70039666 -1.27125394 4.1872298e-008 -0.70039666 -1.33667529 -0.41305539 -0.70039666 -1.27125382
		 -0.78567797 -0.70039666 -1.081393003 -1.081392884 -0.70039666 -0.78567791 -1.27125359 -0.70039666 -0.41305524
		 -1.33667517 -0.70039666 6.020214e-008 1.29134834 -0.3693665 5.2097093e-008 1.22814596 -0.3693665 -0.39904869
		 1.044723272 -0.3693665 -0.75903583 0.75903583 -0.3693665 -1.044723272 0.39904875 -0.3693665 -1.2281456
		 3.8388727e-008 -0.3693665 -1.29134858 -0.39904869 -0.3693665 -1.2281456 -0.75903559 -0.3693665 -1.044722915
		 -1.044722795 -0.3693665 -0.75903553 -1.22814536 -0.3693665 -0.39904854 -1.29134846 -0.3693665 5.2097093e-008;
	setAttr -s 178 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 10 0 0 11 12 0 12 13 0 46 14 0 14 15 0 33 16 0 16 17 0 17 18 0 18 19 0 19 20 0 21 11 0
		 0 78 1 1 77 1 2 76 1 3 75 1 4 74 1 5 73 1 6 72 1 7 71 1 8 70 1 9 69 0 10 79 0 22 20 0
		 23 19 1 22 23 1 24 18 1 23 24 1 25 17 1 24 25 1 26 16 1 25 26 1 27 15 0 26 27 1 28 14 1
		 27 28 1 29 13 0 28 45 1 30 12 1 29 30 1 31 11 1 30 31 1 32 21 0 31 32 1 27 33 0 34 22 0
		 35 23 1 34 35 1 36 24 1 35 36 1 37 25 1 36 37 1 38 26 1 37 38 1 39 27 1 38 39 1 40 28 1
		 39 40 1 41 29 0 40 41 1 42 30 1 41 42 1 43 31 1 42 43 1 44 32 0 43 44 1 45 46 0 41 45 0
		 47 44 0 48 43 1 47 48 0 49 42 1 48 49 1 50 41 1 49 50 1 51 40 1 50 51 1 52 39 1 51 52 1
		 53 38 1 52 53 1 54 37 1 53 54 1 55 36 1 54 55 1 56 35 1 55 56 1 57 34 0 56 57 1 59 48 0
		 58 59 0 60 49 1 59 60 1 61 50 1 60 61 1 62 51 1 61 62 1 63 52 1 62 63 1 64 53 1 63 64 1
		 65 54 1 64 65 1 66 55 1 65 66 1 67 56 1 66 67 1 68 57 0 67 68 1 69 90 0 70 89 1 69 70 1
		 71 88 1 70 71 1 72 87 1 71 72 1 73 86 1 72 73 1 74 85 1 73 74 1 75 84 1 74 75 1 76 83 1
		 75 76 1 77 82 1 76 77 1 78 81 1 77 78 1 79 80 0 78 79 1 81 92 1 80 81 1 82 93 1 81 82 1
		 83 94 1 82 83 1 84 95 1 83 84 1 85 96 1 84 85 1 86 97 1 85 86 1 87 98 1 86 87 1 88 99 1
		 87 88 1 89 100 1 88 89 1 89 90 1 91 58 0 92 59 1 91 92 1 93 60 1 92 93 1 94 61 1
		 93 94 1 95 62 1 94 95 1;
	setAttr ".ed[166:177]" 96 63 1 95 96 1 97 64 1 96 97 1 98 65 1 97 98 1 99 66 1
		 98 99 1 100 67 1 99 100 1 101 68 0 100 101 1;
	setAttr -s 77 -ch 308 ".fc[0:76]" -type "polyFaces" 
		f 4 80 79 71 -78
		mu 0 4 50 51 44 46
		f 4 82 81 69 -80
		mu 0 4 51 52 43 44
		f 4 84 83 67 -82
		mu 0 4 52 53 42 43
		f 4 86 85 65 -84
		mu 0 4 53 54 41 42
		f 4 88 87 63 -86
		mu 0 4 54 55 40 41
		f 4 90 89 61 -88
		mu 0 4 55 56 39 40
		f 4 92 91 59 -90
		mu 0 4 56 57 38 39
		f 4 94 93 57 -92
		mu 0 4 57 58 37 38
		f 4 96 95 55 -94
		mu 0 4 58 59 36 37
		f 4 78 77 73 -77
		mu 0 4 48 49 45 47
		f 4 -34 31 -19 -33
		mu 0 4 25 24 21 20
		f 4 -36 32 -18 -35
		mu 0 4 26 25 20 19
		f 4 -38 34 -17 -37
		mu 0 4 27 26 19 18
		f 4 -40 36 -16 -39
		mu 0 4 28 27 18 17
		f 4 -42 38 -15 -53
		mu 0 4 29 28 17 16
		f 4 -44 40 -14 -43
		mu 0 4 30 29 16 15
		f 4 -46 42 -13 -75
		mu 0 4 31 30 15 14
		f 4 -48 44 -12 -47
		mu 0 4 32 31 14 13
		f 4 -50 46 -11 -49
		mu 0 4 34 32 13 12
		f 4 -52 48 -20 -51
		mu 0 4 35 33 23 22
		f 4 -56 53 33 -55
		mu 0 4 37 36 24 25
		f 4 -58 54 35 -57
		mu 0 4 38 37 25 26
		f 4 -60 56 37 -59
		mu 0 4 39 38 26 27
		f 4 -62 58 39 -61
		mu 0 4 40 39 27 28
		f 4 -64 60 41 -63
		mu 0 4 41 40 28 29
		f 4 -66 62 43 -65
		mu 0 4 42 41 29 30
		f 4 -68 64 45 -76
		mu 0 4 43 42 30 31
		f 4 -70 66 47 -69
		mu 0 4 44 43 31 32
		f 4 -72 68 49 -71
		mu 0 4 46 44 32 34
		f 4 -74 70 51 -73
		mu 0 4 47 45 33 35
		f 4 100 99 -81 -98
		mu 0 4 62 63 51 50
		f 4 102 101 -83 -100
		mu 0 4 63 64 52 51
		f 4 104 103 -85 -102
		mu 0 4 64 65 53 52
		f 4 106 105 -87 -104
		mu 0 4 65 66 54 53
		f 4 108 107 -89 -106
		mu 0 4 66 67 55 54
		f 4 110 109 -91 -108
		mu 0 4 67 68 56 55
		f 4 112 111 -93 -110
		mu 0 4 68 69 57 56
		f 4 114 113 -95 -112
		mu 0 4 69 70 58 57
		f 4 116 115 -97 -114
		mu 0 4 70 71 59 58
		f 4 9 20 137 -31
		mu 0 4 10 11 81 83
		f 4 0 21 135 -21
		mu 0 4 0 1 80 82
		f 4 1 22 133 -22
		mu 0 4 1 2 79 80
		f 4 2 23 131 -23
		mu 0 4 2 3 78 79
		f 4 3 24 129 -24
		mu 0 4 3 4 77 78
		f 4 4 25 127 -25
		mu 0 4 4 5 76 77
		f 4 5 26 125 -26
		mu 0 4 5 6 75 76
		f 4 6 27 123 -27
		mu 0 4 6 7 74 75
		f 4 7 28 121 -28
		mu 0 4 7 8 73 74
		f 4 8 29 119 -29
		mu 0 4 8 9 72 73
		f 4 177 176 -117 -175
		mu 0 4 106 107 71 70
		f 4 175 174 -115 -173
		mu 0 4 105 106 70 69
		f 4 173 172 -113 -171
		mu 0 4 104 105 69 68
		f 4 171 170 -111 -169
		mu 0 4 103 104 68 67
		f 4 169 168 -109 -167
		mu 0 4 102 103 67 66
		f 4 167 166 -107 -165
		mu 0 4 101 102 66 65
		f 4 165 164 -105 -163
		mu 0 4 100 101 65 64
		f 4 163 162 -103 -161
		mu 0 4 99 100 64 63
		f 4 161 160 -101 -159
		mu 0 4 98 99 63 62
		f 4 159 158 -99 -158
		mu 0 4 96 97 61 60
		f 4 -138 134 -140 -137
		mu 0 4 83 81 85 84
		f 4 -136 132 -142 -135
		mu 0 4 82 80 87 86
		f 4 -134 130 -144 -133
		mu 0 4 80 79 88 87
		f 4 -132 128 -146 -131
		mu 0 4 79 78 89 88
		f 4 -130 126 -148 -129
		mu 0 4 78 77 90 89
		f 4 -128 124 -150 -127
		mu 0 4 77 76 91 90
		f 4 -126 122 -152 -125
		mu 0 4 76 75 92 91
		f 4 -124 120 -154 -123
		mu 0 4 75 74 93 92
		f 4 -122 118 -156 -121
		mu 0 4 74 73 94 93
		f 4 -120 117 -157 -119
		mu 0 4 73 72 95 94
		f 4 141 140 -162 -139
		mu 0 4 86 87 99 98
		f 4 143 142 -164 -141
		mu 0 4 87 88 100 99
		f 4 145 144 -166 -143
		mu 0 4 88 89 101 100
		f 4 147 146 -168 -145
		mu 0 4 89 90 102 101
		f 4 149 148 -170 -147
		mu 0 4 90 91 103 102
		f 4 151 150 -172 -149
		mu 0 4 91 92 104 103
		f 4 153 152 -174 -151
		mu 0 4 92 93 105 104
		f 4 155 154 -176 -153
		mu 0 4 93 94 106 105;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "transform2" -p "pCylinder2";
	rename -uid "01166DCF-4991-CA31-A788-38816DE28E5A";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape2" -p "transform2";
	rename -uid "1B3A32DB-4EBE-219E-387B-C28B3C4C133B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 3 ".ciog[0].cog";
	setAttr ".pv" -type "double2" 0.49999988079071045 0.50046992301940918 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder3";
	rename -uid "96938659-4184-7526-A661-D5B53330D580";
	setAttr ".rp" -type "double3" -3.2213180789497642e-007 6.3011927233386729 -1.8614346444221632 ;
	setAttr ".sp" -type "double3" -3.2213180789497642e-007 6.3011927233386729 -1.8614346444221632 ;
createNode mesh -n "pCylinder3Shape" -p "pCylinder3";
	rename -uid "43E6F47C-423E-7904-6E69-EB987793B9A6";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.42499996721744537 0.39844828844070435 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 230 ".pt";
	setAttr ".pt[0]" -type "float3" 0.18486428 0 0 ;
	setAttr ".pt[1]" -type "float3" 0.18486428 0 0 ;
	setAttr ".pt[2]" -type "float3" 0.18486428 0 0 ;
	setAttr ".pt[3]" -type "float3" 0.18486428 0 0 ;
	setAttr ".pt[4]" -type "float3" 0.33002025 0 -0.15110137 ;
	setAttr ".pt[5]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[6]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[7]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[8]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[9]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[10]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[11]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[12]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[13]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[14]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[15]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[16]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[17]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[18]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[19]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[20]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[21]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[22]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[23]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[24]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[25]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[26]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[27]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[28]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[29]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[30]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[47]" -type "float3" 0.036288992 0 0 ;
	setAttr ".pt[48]" -type "float3" 0.036288992 0 0 ;
	setAttr ".pt[49]" -type "float3" 0.036288992 0 0 ;
	setAttr ".pt[50]" -type "float3" 0.036288992 0 0 ;
	setAttr ".pt[51]" -type "float3" 0.036288992 0 0 ;
	setAttr ".pt[52]" -type "float3" 0.036288992 0 0 ;
	setAttr ".pt[53]" -type "float3" 0.036288992 0 0 ;
	setAttr ".pt[54]" -type "float3" 0.18486428 0 0 ;
	setAttr ".pt[55]" -type "float3" 0.18486428 0 0 ;
	setAttr ".pt[56]" -type "float3" 0.18486428 0 0 ;
	setAttr ".pt[57]" -type "float3" 0.18486428 0 0 ;
	setAttr ".pt[58]" -type "float3" 0.33002025 0 -0.15110137 ;
	setAttr ".pt[59]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[60]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[61]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[62]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[63]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[64]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[65]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[66]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[67]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[68]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[69]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[70]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[71]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[72]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[73]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[74]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[75]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[76]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[77]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[78]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[79]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[80]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[81]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[82]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[83]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[84]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[101]" -type "float3" 0.065272108 0 0 ;
	setAttr ".pt[102]" -type "float3" 0.065272108 0 0 ;
	setAttr ".pt[103]" -type "float3" 0.065272108 0 0 ;
	setAttr ".pt[104]" -type "float3" 0.065272108 0 0 ;
	setAttr ".pt[105]" -type "float3" 0.065272108 0 0 ;
	setAttr ".pt[106]" -type "float3" 0.065272108 0 0 ;
	setAttr ".pt[107]" -type "float3" 0.065272108 0 0 ;
	setAttr ".pt[130]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[131]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[132]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[133]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[134]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[135]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[136]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[137]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[138]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[139]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[140]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[141]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[142]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[143]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[144]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[145]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[146]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[147]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[148]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[149]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[150]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[151]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[152]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[153]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[154]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[155]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[160]" -type "float3" 0.18486428 0 0 ;
	setAttr ".pt[161]" -type "float3" 0.18486428 0 0 ;
	setAttr ".pt[162]" -type "float3" 0.25744227 0 -0.15110137 ;
	setAttr ".pt[163]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[164]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[165]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[166]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[167]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[168]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[169]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[170]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[171]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[172]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[173]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[174]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[175]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[176]" -type "float3" 0.33002025 0 -0.15110137 ;
	setAttr ".pt[177]" -type "float3" 0.33002025 0 -0.15110137 ;
	setAttr ".pt[178]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[179]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[180]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[181]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[182]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[183]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[184]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[185]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[186]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[187]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[188]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[189]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[190]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[191]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[199]" -type "float3" 0.036288992 0 0 ;
	setAttr ".pt[201]" -type "float3" 0.036288992 0 0 ;
	setAttr ".pt[205]" -type "float3" 0.036288992 0 0 ;
	setAttr ".pt[206]" -type "float3" 0.036288992 0 0 ;
	setAttr ".pt[234]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[235]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[236]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[237]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[238]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[239]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[240]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[241]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[242]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[243]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[244]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[245]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[246]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[247]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[248]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[249]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[250]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[251]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[252]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[253]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[254]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[255]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[256]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[257]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[258]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[259]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[264]" -type "float3" 0.18486428 0 0 ;
	setAttr ".pt[265]" -type "float3" 0.18486428 0 0 ;
	setAttr ".pt[266]" -type "float3" 0.25744227 0 -0.15110137 ;
	setAttr ".pt[267]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[268]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[269]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[270]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[271]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[272]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[273]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[274]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[275]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[276]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[277]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[278]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[279]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[280]" -type "float3" 0.33002025 0 -0.15110137 ;
	setAttr ".pt[281]" -type "float3" 0.33002025 0 -0.15110137 ;
	setAttr ".pt[282]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[283]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[284]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[285]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[286]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[287]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[288]" -type "float3" 0.36630923 0 0 ;
	setAttr ".pt[289]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[290]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[291]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[292]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[293]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[294]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[295]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[303]" -type "float3" 0.065272108 0 0 ;
	setAttr ".pt[304]" -type "float3" 0.065272108 0 0 ;
	setAttr ".pt[309]" -type "float3" 0.065272108 0 0 ;
	setAttr ".pt[311]" -type "float3" 0.065272108 0 0 ;
	setAttr ".pt[316]" -type "float3" 0.26004022 -1.7763568e-015 -0.13202864 ;
	setAttr ".pt[317]" -type "float3" 0.30895919 -1.7763568e-015 -0.20495863 ;
	setAttr ".pt[318]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[319]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[320]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[321]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[322]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[323]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[324]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[325]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[326]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[327]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[328]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[329]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[330]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[331]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[332]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[333]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[334]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[335]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[336]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[337]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[338]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[339]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[340]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[341]" -type "float3" 0.34325635 0 0 ;
	setAttr ".pt[342]" -type "float3" -0.14837651 0 0 ;
	setAttr ".pt[343]" -type "float3" -0.14837651 0 0 ;
	setAttr ".pt[344]" -type "float3" 0.25744227 0 0 ;
	setAttr ".pt[345]" -type "float3" 0.25744227 0 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "611C2D04-4245-D4B2-9268-B4ACAFB7E023";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "A70C5842-4E48-E897-2474-59AC5F2078A7";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "CFC424A5-4CD8-38FB-6F64-99B941CAEE7E";
createNode displayLayerManager -n "layerManager";
	rename -uid "1970A607-43DB-EA5D-AC55-209C85C151F6";
createNode displayLayer -n "defaultLayer";
	rename -uid "4A155B2D-437F-7064-8FAE-CEAD11EA04A1";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "CDB34A99-4412-20A7-5FA3-6F9330A78850";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "8619DAFE-481F-08E6-F093-1ABAB2312F0B";
	setAttr ".g" yes;
createNode polyCylinder -n "polyCylinder1";
	rename -uid "8B97D7FD-461E-A4B7-C31F-7F9EAC686477";
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polyTweak -n "polyTweak1";
	rename -uid "9AEF3BEC-483E-1425-0201-FC98F62EF213";
	setAttr ".uopa" yes;
	setAttr -s 42 ".tk[0:41]" -type "float3"  0.35921264 0 -0.11671519 0.30556458
		 0 -0.22200556 0.22200568 0 -0.30556443 0.1167153 0 -0.35921255 4.502515e-008 0 -0.37769842
		 -0.11671521 0 -0.35921255 -0.22200552 0 -0.30556437 -0.30556437 0 -0.2220055 -0.35921246
		 0 -0.11671515 -0.37769836 0 6.7537719e-008 -0.35921246 0 0.11671527 -0.30556434 0
		 0.22200559 -0.2220055 0 0.30556443 -0.11671516 0 0.35921255 3.376886e-008 0 0.37769839
		 0.11671522 0 0.35921255 0.22200552 0 0.3055644 0.30556437 0 0.22200558 0.35921246
		 0 0.11671525 0.37769836 0 6.7537719e-008 0 0.75840771 0 0 0.75840771 0 0 0.75840771
		 0 0 0.75840771 0 0 0.75840771 0 0 0.75840771 0 0 0.75840771 0 0 0.75840771 0 0 0.75840771
		 0 0 0.75840771 0 0 0.75840771 0 0 0.75840771 0 0 0.75840771 0 0 0.75840771 0 0 0.75840771
		 0 0 0.75840771 0 0 0.75840771 0 0 0.75840771 0 0 0.75840771 0 0 0.75840771 0 4.502515e-008
		 0 6.7537719e-008 0 0.75840771 0;
createNode deleteComponent -n "deleteComponent1";
	rename -uid "5A9DF21B-4D67-42AF-F0D7-F3BB1202A3B2";
	setAttr ".dc" -type "componentList" 3 "f[9:18]" "f[29:38]" "f[49:58]";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "431B830B-400C-899A-326C-1994FD688AF6";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n"
		+ "            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 778\n            -height 333\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 778\n            -height 333\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n"
		+ "            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n"
		+ "            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 778\n            -height 333\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n"
		+ "        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 778\n            -height 333\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n"
		+ "            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n"
		+ "            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n"
		+ "            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n"
		+ "            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n"
		+ "                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n"
		+ "                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n"
		+ "                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n"
		+ "                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n"
		+ "                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n"
		+ "                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n"
		+ "                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"\"\n"
		+ "\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"quad\\\" -ps 1 50 50 -ps 2 50 50 -ps 3 50 50 -ps 4 50 50 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 778\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 778\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 778\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 778\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Side View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 778\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 778\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Front View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 778\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 778\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "C9F86598-4BA6-6469-7733-ACA0F867D100";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode deleteComponent -n "deleteComponent2";
	rename -uid "67274E09-414A-E4C6-0F11-A888DCF88456";
	setAttr ".dc" -type "componentList" 1 "f[20:29]";
createNode polySplitRing -n "polySplitRing1";
	rename -uid "0B13B400-4A41-B6D5-F2BE-E79F3A5754A6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[20:30]";
	setAttr ".ix" -type "matrix" 2.7022374593329266 0 0 0 0 4.6776033309485667 0 0 0 0 2.7022374593329266 0
		 0 4.5274275034449296 0 1;
	setAttr ".wt" 0.93148297071456909;
	setAttr ".dr" no;
	setAttr ".re" 24;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitEdge -n "polySplitEdge1";
	rename -uid "A4BC34DB-4BB8-BBB1-81CF-0BAC7D9517CE";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[51]";
createNode polySplitRing -n "polySplitRing2";
	rename -uid "473B9B4A-416E-D3A5-8B28-BD812A8A5E04";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[20:30]";
	setAttr ".ix" -type "matrix" 2.7022374593329266 0 0 0 0 4.6776033309485667 0 0 0 0 2.7022374593329266 0
		 0 4.5274275034449296 0 1;
	setAttr ".wt" 0.92493712902069092;
	setAttr ".dr" no;
	setAttr ".re" 25;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak2";
	rename -uid "97404CE6-44DD-9C10-D88D-318777653A47";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[15]" -type "float3" 0.077210471 0 0 ;
	setAttr ".tk[28]" -type "float3" 1.4901161e-008 0 0 ;
	setAttr ".tk[34]" -type "float3" -0.04479406 0 0 ;
createNode polySplitEdge -n "polySplitEdge2";
	rename -uid "97239C2F-49F4-858C-946B-2287AB2450EE";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[55]" "e[77]";
createNode polyTweak -n "polyTweak3";
	rename -uid "0335EDCF-4667-DAF9-2C48-7091DAC02B17";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[13]" -type "float3" 0.087668367 0 0.033001781 ;
	setAttr ".tk[30]" -type "float3" 0.052933421 0 0.020005513 ;
createNode deleteComponent -n "deleteComponent3";
	rename -uid "FFB8FEEA-4634-4F16-401F-A8A1DBA01C6E";
	setAttr ".dc" -type "componentList" 1 "f[10:19]";
createNode polySplitRing -n "polySplitRing3";
	rename -uid "F71A63EB-4001-74F7-A15C-B39D9A206C8B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[20:30]";
	setAttr ".ix" -type "matrix" 2.7022374593329266 0 0 0 0 4.6776033309485667 0 0 0 0 2.7022374593329266 0
		 0 4.5274275034449296 0 1;
	setAttr ".wt" 0.95975345373153687;
	setAttr ".dr" no;
	setAttr ".re" 30;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing4";
	rename -uid "7535C6C5-45EA-01F0-F52A-FEA10A511710";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[20:30]";
	setAttr ".ix" -type "matrix" 2.7022374593329266 0 0 0 0 4.6776033309485667 0 0 0 0 2.7022374593329266 0
		 0 4.5274275034449296 0 1;
	setAttr ".wt" 0.85587871074676514;
	setAttr ".dr" no;
	setAttr ".re" 30;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode deleteComponent -n "deleteComponent4";
	rename -uid "3B8D903A-4FC0-E279-096B-759CCC7D6F2C";
	setAttr ".dc" -type "componentList" 1 "f[30]";
createNode polySplitRing -n "polySplitRing5";
	rename -uid "AD61C2D8-41D8-3EEB-59D5-EC99BC03816A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[20:30]";
	setAttr ".ix" -type "matrix" 2.7022374593329266 0 0 0 0 4.6776033309485667 0 0 0 0 2.7022374593329266 0
		 0 4.5274275034449296 0 1;
	setAttr ".wt" 0.11990870535373688;
	setAttr ".re" 28;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing6";
	rename -uid "B510DE31-4F9E-B619-74FD-1A8A916A3AD4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 10 "e[117:118]" "e[120]" "e[122]" "e[124]" "e[126]" "e[128]" "e[130]" "e[132]" "e[134]" "e[136]";
	setAttr ".ix" -type "matrix" 2.7022374593329266 0 0 0 0 4.6776033309485667 0 0 0 0 2.7022374593329266 0
		 0 4.5274275034449296 0 1;
	setAttr ".wt" 0.03813614696264267;
	setAttr ".re" 136;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak4";
	rename -uid "FB5F0C02-4044-96FE-5949-7C8E5842B1FC";
	setAttr ".uopa" yes;
	setAttr -s 11 ".tk[0:10]" -type "float3"  0.35909936 0 0.072111204 0.30546823
		 0 -0.033145919 0.2219356 0 -0.11667844 0.11667848 0 -0.17030965 4.5010943e-008 0
		 -0.18878965 -0.11667839 0 -0.1703096 -0.22193551 0 -0.11667839 -0.30546802 0 -0.033145856
		 -0.35909915 0 0.072111264 -0.37757918 0 0.18878964 0.37757918 0 0.18878964;
createNode polySplitRing -n "polySplitRing7";
	rename -uid "601A2338-4122-E7A1-72F5-3CB70F134646";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 10 "e[138:139]" "e[141]" "e[143]" "e[145]" "e[147]" "e[149]" "e[151]" "e[153]" "e[155]" "e[157]";
	setAttr ".ix" -type "matrix" 2.7022374593329266 0 0 0 0 4.6776033309485667 0 0 0 0 2.7022374593329266 0
		 0 4.5274275034449296 0 1;
	setAttr ".wt" 0.20031280815601349;
	setAttr ".re" 138;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode deleteComponent -n "deleteComponent5";
	rename -uid "A6BFB829-49AC-08DF-1465-6FB9FCF74B0C";
	setAttr ".dc" -type "componentList" 1 "f[69]";
createNode deleteComponent -n "deleteComponent6";
	rename -uid "FEE35BFC-4E30-FFFE-D54D-938A04AE27E0";
	setAttr ".dc" -type "componentList" 1 "f[77]";
createNode polyNormal -n "polyNormal1";
	rename -uid "53202F28-41A9-4840-05AD-D5817A52EBBC";
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".unm" no;
createNode polyUnite -n "polyUnite1";
	rename -uid "DB28FCAF-4623-4A90-199C-1297831214FE";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId1";
	rename -uid "95401C1F-4094-046E-4F16-C4899BFEE49E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "413A3CC2-451F-9A6D-7695-3E813B829B06";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:76]";
createNode groupId -n "groupId2";
	rename -uid "7B1F2F66-4949-0CAD-3CC1-77BB18302363";
	setAttr ".ihi" 0;
createNode groupId -n "groupId3";
	rename -uid "CE4A7094-4210-62B7-02C4-3DA42FE02486";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "60592624-4CFD-24A4-13F0-149DDDEE4C25";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:76]";
createNode groupId -n "groupId4";
	rename -uid "C09F4207-4FC1-4719-F364-1EB360617884";
	setAttr ".ihi" 0;
createNode groupId -n "groupId5";
	rename -uid "8B6D2761-4125-B3AC-5D1B-BAA525FC0909";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "7D19F366-4271-E650-9857-269ED4689137";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:153]";
createNode polyBridgeEdge -n "polyBridgeEdge1";
	rename -uid "8569221A-45B0-71BB-B627-6F9B6F66653C";
	setAttr ".ics" -type "componentList" 2 "e[29]" "e[207]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 9;
	setAttr ".sv2" 171;
	setAttr ".d" 1;
	setAttr ".sd" 1;
createNode polyBridgeEdge -n "polyBridgeEdge2";
	rename -uid "D57354FA-446D-4AD5-E7D6-32BA0B8F06F8";
	setAttr ".ics" -type "componentList" 39 "e[0:19]" "e[30:31]" "e[40]" "e[44]" "e[50]" "e[52:53]" "e[66]" "e[72]" "e[74:76]" "e[78]" "e[95]" "e[97:98]" "e[115]" "e[117]" "e[136]" "e[138:139]" "e[154]" "e[156:157]" "e[159]" "e[176:197]" "e[208:209]" "e[218]" "e[222]" "e[228]" "e[230:231]" "e[244]" "e[250]" "e[252:254]" "e[256]" "e[273]" "e[275:276]" "e[293]" "e[295]" "e[314]" "e[316:317]" "e[332]" "e[334:335]" "e[337]" "e[354:355]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 69;
	setAttr ".sv2" 111;
	setAttr ".d" 1;
	setAttr ".sd" 1;
createNode polyBevel3 -n "polyBevel1";
	rename -uid "6A858377-4D80-E2F8-5415-63B7563072EC";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 39 "e[0:19]" "e[29:31]" "e[40]" "e[44]" "e[50]" "e[52:53]" "e[66]" "e[72]" "e[74:76]" "e[78]" "e[95]" "e[97:98]" "e[115]" "e[117]" "e[136]" "e[138:139]" "e[154]" "e[156:157]" "e[159]" "e[176:197]" "e[207:209]" "e[218]" "e[222]" "e[228]" "e[230:231]" "e[244]" "e[250]" "e[252:254]" "e[256]" "e[273]" "e[275:276]" "e[293]" "e[295]" "e[314]" "e[316:317]" "e[332]" "e[334:335]" "e[337]" "e[354:355]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polySplitRing -n "polySplitRing8";
	rename -uid "809D5C7A-4132-C602-C6EB-8EAC7C925618";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 6 "e[201]" "e[220]" "e[360]" "e[362]" "e[505]" "e[507]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.90931606292724609;
	setAttr ".dr" no;
	setAttr ".re" 220;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplit -n "polySplit1";
	rename -uid "82473E33-410B-756B-CBC9-C3AE75520529";
	setAttr -s 22 ".e[0:21]"  0 0.89353001 0.89014697 0.883735 0.88886499
		 0.89735299 0.89701498 0.882976 0.87379599 0.121904 0.87580401 0.13623101 0.858486
		 0.86521798 0.86620402 0.87114501 0.87333697 0.86991799 0.86575699 0.872132 0.87259799
		 1;
	setAttr -s 22 ".d[0:21]"  -2147483022 -2147483476 -2147483475 -2147483473 -2147483471 -2147483469 
		-2147483467 -2147483465 -2147483425 -2147483140 -2147483138 -2147483285 -2147483283 -2147483441 -2147483558 -2147483560 -2147483562 -2147483564 
		-2147483566 -2147483568 -2147483569 -2147483447;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "F45EA6F9-4A10-5F3E-1DFD-7788231B0189";
	setAttr ".ics" -type "componentList" 1 "f[312]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 2.9480026 8.6373653 -0.51024806 ;
	setAttr ".rs" 33778;
	setAttr ".lt" -type "double3" -3.8857805861880479e-016 -9.4368957093138306e-016 
		2.3317891580402197 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 2.8589777946472168 8.3636388778686523 -0.94231593608856201 ;
	setAttr ".cbx" -type "double3" 3.03702712059021 8.9110918045043945 -0.078180201351642609 ;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 5 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 5 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "groupId3.id" "pCylinderShape1.iog.og[2].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinderShape1.iog.og[2].gco";
connectAttr "groupParts2.og" "pCylinderShape1.i";
connectAttr "groupId4.id" "pCylinderShape1.ciog.cog[2].cgid";
connectAttr "groupId1.id" "pCylinderShape2.iog.og[2].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinderShape2.iog.og[2].gco";
connectAttr "groupParts1.og" "pCylinderShape2.i";
connectAttr "groupId2.id" "pCylinderShape2.ciog.cog[2].cgid";
connectAttr "polyExtrudeFace1.out" "pCylinder3Shape.i";
connectAttr "groupId5.id" "pCylinder3Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinder3Shape.iog.og[0].gco";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCylinder1.out" "polyTweak1.ip";
connectAttr "polyTweak1.out" "deleteComponent1.ig";
connectAttr "deleteComponent1.og" "deleteComponent2.ig";
connectAttr "deleteComponent2.og" "polySplitRing1.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing1.mp";
connectAttr "polySplitRing1.out" "polySplitEdge1.ip";
connectAttr "polyTweak2.out" "polySplitRing2.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing2.mp";
connectAttr "polySplitEdge1.out" "polyTweak2.ip";
connectAttr "polySplitRing2.out" "polySplitEdge2.ip";
connectAttr "polySplitEdge2.out" "polyTweak3.ip";
connectAttr "polyTweak3.out" "deleteComponent3.ig";
connectAttr "deleteComponent3.og" "polySplitRing3.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing3.mp";
connectAttr "polySplitRing3.out" "polySplitRing4.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing4.mp";
connectAttr "polySplitRing4.out" "deleteComponent4.ig";
connectAttr "deleteComponent4.og" "polySplitRing5.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing5.mp";
connectAttr "polyTweak4.out" "polySplitRing6.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing6.mp";
connectAttr "polySplitRing5.out" "polyTweak4.ip";
connectAttr "polySplitRing6.out" "polySplitRing7.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing7.mp";
connectAttr "polySplitRing7.out" "deleteComponent5.ig";
connectAttr "deleteComponent5.og" "deleteComponent6.ig";
connectAttr "polySurfaceShape1.o" "polyNormal1.ip";
connectAttr "pCylinderShape2.o" "polyUnite1.ip[0]";
connectAttr "pCylinderShape1.o" "polyUnite1.ip[1]";
connectAttr "pCylinderShape2.wm" "polyUnite1.im[0]";
connectAttr "pCylinderShape1.wm" "polyUnite1.im[1]";
connectAttr "polyNormal1.out" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "deleteComponent6.og" "groupParts2.ig";
connectAttr "groupId3.id" "groupParts2.gi";
connectAttr "polyUnite1.out" "groupParts3.ig";
connectAttr "groupId5.id" "groupParts3.gi";
connectAttr "groupParts3.og" "polyBridgeEdge1.ip";
connectAttr "pCylinder3Shape.wm" "polyBridgeEdge1.mp";
connectAttr "polyBridgeEdge1.out" "polyBridgeEdge2.ip";
connectAttr "pCylinder3Shape.wm" "polyBridgeEdge2.mp";
connectAttr "polyBridgeEdge2.out" "polyBevel1.ip";
connectAttr "pCylinder3Shape.wm" "polyBevel1.mp";
connectAttr "polyBevel1.out" "polySplitRing8.ip";
connectAttr "pCylinder3Shape.wm" "polySplitRing8.mp";
connectAttr "polySplitRing8.out" "polySplit1.ip";
connectAttr "polySplit1.out" "polyExtrudeFace1.ip";
connectAttr "pCylinder3Shape.wm" "polyExtrudeFace1.mp";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCylinderShape2.iog.og[2]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape2.ciog.cog[2]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape1.iog.og[2]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape1.ciog.cog[2]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinder3Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId1.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId2.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId3.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId4.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId5.msg" ":initialShadingGroup.gn" -na;
// End of hollowTreeOne_model_001.ma
